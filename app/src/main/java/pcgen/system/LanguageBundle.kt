/*
 * Copyright 2002 (C) Thomas Behr <ravenlock@gmx.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.system

import android.content.Context
import net.redlightning.pcgen.MainActivity
import net.redlightning.pcgen.R
import java.text.MessageFormat

/**
 * `LanguageBundle` manages the localisation of the PCGen interface.
 * It provides a set of features to translate il8n keys into text in the
 * language chosen in preferences.
 */
object LanguageBundle {
    const val KEY_PREFIX = "in_"
    private const val BUNDLE_NAME = "pcgen.lang"
    private const val UNDEFINED = " not defined."

    /**
     * Allow pretty formatting of multiplier. For example, if d is 0.5d, it
     * returns x 1/2 (
     *
     * @param d a double value
     * @return a formatted String
     */
    fun getPrettyMultiplier(context: Context, d: Double): String {
        return if (d == 0.25) {
            context.getString(R.string.in_multQuarter)
        } else if (d == 0.5) {
            context.getString(R.string.in_multHalf)
        } else if (d == 0.75) {
            context.getString(R.string.in_multThreeQuarter)
        } else {
            MessageFormat.format(context.getString(R.string.in_multiply), d)
        }
    }

    /**
     * Returns a localized string from the language bundle for the key passed.
     * This method accepts a variable number of parameters and will replace
     * {argno} in the string with each passed paracter in turn.
     * @param aKey The key of the string to retrieve
     * @param varargs A variable number of parameters to substitute into the
     * returned string.
     * @return A formatted localized string
     */
    @JvmStatic
    fun getFormattedString(aKey: Int, vararg varargs: Any?): String {
        val string = getString(aKey)
        return if (varargs != null && varargs.isNotEmpty()) {
            MessageFormat.format(string, *varargs)
        } else string
    }

    /**
     * Get a string from the app context
     *
     * @param aKey The key of the string to retrieve
     * @return localized string
     */
    @JvmStatic
    fun getString(aKey: Int): String {
        return MainActivity.getAppContext()!!.getString(aKey)
    }
}