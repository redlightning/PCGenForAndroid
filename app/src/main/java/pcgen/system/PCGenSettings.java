/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.system;

import android.graphics.Color;

import java.io.File;

import pcgen.facade.core.CharacterFacade;
import pcgen.output.publish.OutputDB;

import org.apache.commons.lang3.SystemUtils;

/**
 * This stores some of the properties that pcgen uses.
 * This class is mainly intended to be used to store non-ui related
 * properties
 */
public final class PCGenSettings extends PropertyContext
{

	private static final PCGenSettings instance = new PCGenSettings();
	/**
	 * This is the PropertyContext for the pcgen options, all keys that are used with
	 * this context have a key name starting with 'OPTION'
	 */
	public static final PropertyContext OPTIONS_CONTEXT = instance.createChildContext("pcgen.options");
	public static final String OPTION_SAVE_CUSTOM_EQUIPMENT = "saveCustomInLst";
	public static final String OPTION_ALLOWED_IN_SOURCES = "optionAllowedInSources";
	public static final String OPTION_SOURCES_ALLOW_MULTI_LINE = "optionSourcesAllowMultiLine";
	public static final String OPTION_SHOW_LICENSE = "showLicense";
	public static final String OPTION_SHOW_MATURE_ON_LOAD = "showMatureOnLoad";
	public static final String OPTION_SHOW_SPONSORS_ON_LOAD = "showSponsorsOnLoad";
	public static final String OPTION_CREATE_PCG_BACKUP = "createPcgBackup";
	public static final String OPTION_SHOW_HP_DIALOG_AT_LEVELUP = "showHPDialogAtLevelUp";
	public static final String OPTION_SHOW_STAT_DIALOG_AT_LEVELUP = "showStatDialogAtLevelUp";
	public static final String OPTION_SHOW_WARNING_AT_FIRST_LEVEL_UP = "showWarningAtFirstLevelUp";
	public static final String OPTION_AUTO_RESIZE_EQUIP = "autoResizeEquip";
	public static final String OPTION_SHOW_SKILL_MOD_BREAKDOWN = "showSkillModBreakdown";
	public static final String OPTION_SHOW_SKILL_RANK_BREAKDOWN = "showSkillRankBreakdown";
	public static final String OPTION_SHOW_OUTPUT_NAME_FOR_OTHER_ITEMS = "showOutputNameForOtherItems";
	public static final String OPTION_AUTOLOAD_SOURCES_AT_START = "autoloadSourcesAtStart";
	public static final String OPTION_AUTOLOAD_SOURCES_WITH_PC = "autoloadSourcesWithPC";
	public static final String OPTION_AUTOCREATE_MW_MAGIC_EQUIP = "autoCreateMagicMwEquip";
	public static final String OPTION_ALLOW_OVERRIDE_DUPLICATES = "allowOverrideDuplicates";
	public static final String OPTION_SKILL_FILTER = "skillsOutputFilter";
	public static final String OPTION_GENERATE_TEMP_FILE_WITH_PDF = "generateTempFileWithPdf";
	public static final String BROWSER_PATH = "browserPath";
	/**
	 * The key for the path to the character files.
	 */
	public static final String PCG_SAVE_PATH = "pcgen.files.characters";
	public static final String PCP_SAVE_PATH = "pcgen.files.parties";
	public static final String CHAR_PORTRAITS_PATH = "pcgen.files.portaits";
	public static final String BACKUP_PCG_PATH = "pcgen.files.characters.backup";
	public static final String SELECTED_SPELL_SHEET_PATH = "pcgen.files.selectedSpellOutputSheet";
	public static final String RECENT_CHARACTERS = "recentCharacters";
	public static final String RECENT_PARTIES = "recentParties";
	public static final String LAST_LOADED_SOURCES = "lastLoadedSources";
	public static final String LAST_LOADED_GAME = "lastLoadedGame";
	public static final String PAPERSIZE = "papersize";

	/* Data converter saved choices. */
	public static final String CONVERT_OUTPUT_SAVE_PATH = "pcgen.convert.outputPath";
	public static final String CONVERT_INPUT_PATH = "pcgen.convert.inputPath";
	public static final String CONVERT_GAMEMODE = "pcgen.convert.gamemode";
	public static final String CONVERT_SOURCES = "pcgen.convert.sources";
	public static final String CONVERT_DATA_LOG_FILE = "pcgen.convert.dataLogFile";
	
	public static final PropertyContext GMGEN_OPTIONS_CONTEXT = instance.createChildContext("gmgen.options");
    private static final String CUSTOM_ITEM_COLOR = "customItemColor";
    private static final String NOT_QUALIFIED_COLOR = "notQualifiedColor";
    private static final String AUTOMATIC_COLOR = "automaticColor";
    private static final String VIRTUAL_COLOR = "virtualColor";
    private static final String QUALIFIED_COLOR = "qualifiedColor";
    private static final String SOURCE_STATUS_RELEASE_COLOR = "sourceStatusReleaseColor";
    private static final String SOURCE_STATUS_ALPHA_COLOR = "sourceStatusAlphaColor";
    private static final String SOURCE_STATUS_BETA_COLOR = "sourceStatusBetaColor";
    private static final String SOURCE_STATUS_TEST_COLOR = "sourceStatusTestColor";
    public static final String ALWAYS_OPEN_EXPORT_FILE = "alwaysOpenExportFile";
    public static final String DEFAULT_OS_TYPE = "defaultOSType";
    public static final String DEFAULT_PDF_OUTPUT_SHEET = "defaultPdfOutputSheet";
    public static final String DEFAULT_HTML_OUTPUT_SHEET = "defaultHtmlOutputSheet";
    public static final String SAVE_OUTPUT_SHEET_WITH_PC = "saveOutputSheetWithPC";
    /** Should we delete all temp files on exit that were generated during outputting character. */
    public static final String CLEANUP_TEMP_FILES = "cleanupTempFiles";
    /** Settings key for showing the source selection dialog. */
    public static final String SKIP_SOURCE_SELECTION = "SourceSelectionDialog.skipOnStart";
    /** Settings key for basic/advanced sources. */
    public static final String SOURCE_USE_BASIC_KEY = "SourceSelectionDialog.useBasic";
    /** What should the chooser do with a single choice? */
    private static final String SINGLE_CHOICE_ACTION = "singleChoiceAction";

    /* Configuration Settings */
    public static final String SETTINGS_FILES_PATH = "settingsPath";
    public static final String SYSTEMS_DIR = "systemsPath";
    public static final String THEME_PACK_DIR = "themesPath";
    public static final String OUTPUT_SHEETS_DIR = "osPath";
    public static final String PLUGINS_DIR = "pluginsPath";
    public static final String PREVIEW_DIR = "previewPath";
    public static final String VENDOR_DATA_DIR = "vendordataPath";
    public static final String HOMEBREW_DATA_DIR = "homebrewdataPath";
    public static final String DOCS_DIR = "docsPath";
    public static final String PCC_FILES_DIR = "pccFilesPath";
    public static final String CUSTOM_DATA_DIR = "customPath";

    /**
     * The character property for the initial tab to open
     * this property corresponds to an integer value
     */
    public static final String C_PROP_INITIAL_TAB = "initialTab";

    public Color getColor(String key)
    {
        String prop = getProperty(key);
        if (prop == null)
        {
            return null;
        }
        return  Color.valueOf(Integer.parseInt(prop, 16));
    }

    public Color getColor(String key, Color defaultValue)
    {
        String prop = getProperty(key, Integer.toString(defaultValue.toArgb(), 16));
        return  Color.valueOf(Integer.parseInt(prop, 16));
    }

    public void setColor(String key, Color color)
    {
        setProperty(key, Integer.toString(color.toArgb(), 16));
    }

    public Color initColor(String key, Color defaultValue)
    {
        String prop = initProperty(key, Integer.toString(defaultValue.toArgb(), 16));
        return Color.valueOf(Integer.parseInt(prop, 16));
    }

    public static Color getCustomItemColor()
    {
        return getInstance().getColor(CUSTOM_ITEM_COLOR);
    }

    public static void setCustomItemColor(Color color)
    {
        getInstance().setColor(CUSTOM_ITEM_COLOR, color);
    }

    public static void setQualifiedColor(Color color)
    {
        getInstance().setColor(QUALIFIED_COLOR, color);
    }

    public static Color getQualifiedColor()
    {
        return getInstance().getColor(QUALIFIED_COLOR);
    }

    public static void setNotQualifiedColor(Color color)
    {
        getInstance().setColor(NOT_QUALIFIED_COLOR, color);
    }

    public static Color getNotQualifiedColor()
    {
        return getInstance().getColor(NOT_QUALIFIED_COLOR);
    }

    public static void setAutomaticColor(Color color)
    {
        getInstance().setColor(AUTOMATIC_COLOR, color);
    }

    public static Color getAutomaticColor()
    {
        return getInstance().getColor(AUTOMATIC_COLOR);
    }

    public static void setVirtualColor(Color color)
    {
        getInstance().setColor(VIRTUAL_COLOR, color);
    }

    public static Color getVirtualColor()
    {
        return getInstance().getColor(VIRTUAL_COLOR);
    }

    public static void setSourceStatusReleaseColor(Color color)
    {
        getInstance().setColor(SOURCE_STATUS_RELEASE_COLOR, color);
    }

    public static Color getSourceStatusReleaseColor()
    {
        return getInstance().initColor(SOURCE_STATUS_RELEASE_COLOR, Color.valueOf(Color.BLACK));
    }

    public static void setSourceStatusAlphaColor(Color color)
    {
        getInstance().setColor(SOURCE_STATUS_ALPHA_COLOR, color);
    }

    public static Color getSourceStatusAlphaColor()
    {
        return getInstance().initColor(SOURCE_STATUS_ALPHA_COLOR, Color.valueOf(Color.RED));
    }

    public static void setSourceStatusBetaColor(Color color)
    {
        getInstance().setColor(SOURCE_STATUS_BETA_COLOR, color);
    }

    public static Color getSourceStatusBetaColor()
    {
        return getInstance().initColor(SOURCE_STATUS_BETA_COLOR, Color.valueOf(Color.rgb(128, 0, 0)));
    }

    public static void setSourceStatusTestColor(Color color)
    {
        getInstance().setColor(SOURCE_STATUS_TEST_COLOR, color);
    }

    public static Color getSourceStatusTestColor()
    {
        return getInstance().initColor(SOURCE_STATUS_TEST_COLOR, Color.valueOf(Color.MAGENTA));
    }

    public static int getSingleChoiceAction()
    {
        return getInstance().initInt(SINGLE_CHOICE_ACTION, 0);
    }

    public static void setSingleChoiceAction(int action)
    {
        getInstance().setInt(SINGLE_CHOICE_ACTION, action);
    }

    /**
     * Attempts to create the property key for this character for the given property.
     * This allows for character specific properties such that the key created with this method
     * can be used as the key for any of the other PropertyContext methods.
     * The following is a typical example of its usage:
     * <code>
     * String charKey = UIPropertyContext.createCharacterPropertyKey(aCharacter, "allowNegativeMoney");<br>
     * if(charKey != null){<br>
     * boolean bool = UIPropertyContext.getInstance().getBoolean(charKey);<br>
     * }<br>
     * </code>
     * @param character a CharacterFacade
     * @param key a String property key
     * @return the character property key or null if it could not be created
     */
    public static String createCharacterPropertyKey(CharacterFacade character, String key)
    {
        return createFilePropertyKey(character.getFileRef().get(), key);
    }

    static String createFilePropertyKey(File file, String key)
    {
        if (file == null)
        {
            return null;
        }
        String path = file.getAbsolutePath();
        return path + '.' + key;
    }
	private PCGenSettings()
	{
		super("options.ini");
		setProperty(PCG_SAVE_PATH, (ConfigurationSettings.getUserDir() + "/characters").replace('/', File.separatorChar));
		setProperty(PCP_SAVE_PATH, (ConfigurationSettings.getUserDir() + "/characters").replace('/', File.separatorChar));
		setProperty(CHAR_PORTRAITS_PATH, (ConfigurationSettings.getUserDir() + "/characters").replace('/', File.separatorChar));
		setProperty(BACKUP_PCG_PATH, (ConfigurationSettings.getUserDir() + "/characters").replace('/', File.separatorChar));
		setProperty(VENDOR_DATA_DIR, "@vendordata");
		setProperty(HOMEBREW_DATA_DIR, "@homebrewdata");
		setProperty(CUSTOM_DATA_DIR, "@data/customsources".replace('/', File.separatorChar));
		OutputDB.registerBooleanPreference(OPTION_SHOW_OUTPUT_NAME_FOR_OTHER_ITEMS, false);
        setColor(CUSTOM_ITEM_COLOR, Color.valueOf(Color.BLUE));
        setColor(NOT_QUALIFIED_COLOR, Color.valueOf(Color.RED));
        setColor(AUTOMATIC_COLOR, Color.valueOf(Color.rgb(178, 178, 0)));
        setColor(VIRTUAL_COLOR, Color.valueOf(Color.MAGENTA));
        setColor(QUALIFIED_COLOR, Color.valueOf(Color.BLACK));

    }

	@Override
	protected void beforePropertiesSaved()
	{
		relativize(VENDOR_DATA_DIR);
		relativize(HOMEBREW_DATA_DIR);
		relativize(CUSTOM_DATA_DIR);
	}

	public static PCGenSettings getInstance()
	{
		return instance;
	}

	public static String getSelectedSpellSheet() { return getInstance().getProperty(SELECTED_SPELL_SHEET_PATH);	}

	public static String getPcgDir()
	{
		return getInstance().getProperty(PCG_SAVE_PATH);
	}

	public static String getPortraitsDir()
	{
		return getInstance().getProperty(CHAR_PORTRAITS_PATH);
	}

	public static String getBackupPcgDir()
	{
		return getInstance().getProperty(BACKUP_PCG_PATH);
	}

	public static String getBrowserPath()
	{
		return OPTIONS_CONTEXT.getProperty(BROWSER_PATH);
	}

	public static boolean getCreatePcgBackup()
	{
		return OPTIONS_CONTEXT.initBoolean(
				PCGenSettings.OPTION_CREATE_PCG_BACKUP, true);
	}

	public static String getVendorDataDir()
	{
		return getDirectory(VENDOR_DATA_DIR);
	}

	public static String getHomebrewDataDir()
	{
		return getDirectory(HOMEBREW_DATA_DIR);
	}

	public static String getCustomDir()
	{
		return getDirectory(CUSTOM_DATA_DIR);
	}

	public static String getSystemProperty(String key)
	{
		return getInstance().getProperty(key);
	}

	public static Object setSystemProperty(String key, String value)
	{
		return getInstance().setProperty(key, value);
	}

	private static String getDirectory(String key)
	{
		return expandRelativePath(getSystemProperty(key));
	}

	private static String expandRelativePath(String path)
	{
		if (path.startsWith("@"))
		{
			path = SystemUtils.USER_DIR + File.separator + path.substring(1);
		}
		return path;
	}

	private static String unexpandRelativePath(String path)
	{
		if (path.startsWith(SystemUtils.USER_DIR + File.separator))
		{
			path = '@' + path.substring(SystemUtils.USER_DIR.length() + 1);
		}
		return path;
	}

	private static void relativize(String property)
	{
		setSystemProperty(property, unexpandRelativePath(getSystemProperty(property)));
	}

}
