/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.system;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PropertyContextFactory
{
    private static final String TAG = PropertyContextFactory.class.getCanonicalName();
	private static PropertyContextFactory DEFAULT_FACTORY;
	private final Map<String, PropertyContext> contextMap = new HashMap<>();
    private final File dir;

    public PropertyContextFactory(Context context) {
        this.dir = context.getFilesDir();
	}

	public static PropertyContextFactory getDefaultFactory()
	{
		return DEFAULT_FACTORY;
	}

    public static void setDefaultFactory(Context context) {
        DEFAULT_FACTORY = new PropertyContextFactory(context);
    }

    public void registerAndLoadPropertyContext(PropertyContext context) {
		registerPropertyContext(context);
        loadPropertyContext(new File(dir.getPath(), context.getName()));
    }

    private void loadPropertyContext(File file) {
		String name = file.getName();
        if (!file.exists()) {
            Log.d(TAG, "No " + name + " file found, will create one when exiting.");
			return;
        } else if (!file.canWrite()) {
			 Log.e(TAG, "WARNING: The file you specified is not updatable. "
					+ "Settings changes will not be saved. File is " + file.getAbsolutePath());
		}

		PropertyContext context = contextMap.get(name);
        if (context == null) {
			context = new PropertyContext(name);
			contextMap.put(name, context);
		}
		FileInputStream in = null;
		boolean loaded = false;
        try {
			in = new FileInputStream(file);
			context.properties.load(in);
			loaded = true;
			context.afterPropertiesLoaded();
        } catch (Exception ex) {
			 Log.e(TAG, "Error occurred while reading properties", ex);
        } finally {
            try {
                if (in != null) {
					in.close();
				}
            } catch (IOException ex) {
				//Not much to do about it...
				 Log.e(TAG, "Failed to close input stream for file: " + context.getName(), ex);
			}
		}
        if (!loaded) {
			 Log.e(TAG,
					"Failed to load " + name + ", either the file is unreadable or it "
					+ "is corrupt. Possible solution is to delete the " + name
					+ " file and restart PCGen");
		}
	}

    public void loadPropertyContexts() {
        File[] files = dir.listFiles();
        if (files == null) {
			return;
		}
        for (final File file : files) {
            if (!file.isDirectory() && file.getName().endsWith(".ini")) {
				loadPropertyContext(file);
			}
		}
	}

    private void savePropertyContext(File settingsDir, PropertyContext context) {
		File file = new File(settingsDir, context.getName());
        if (file.exists() && !file.canWrite()) {
			 Log.e(TAG, "WARNING: Could not update settings file: " + file.getAbsolutePath());
			return;
		}
		FileOutputStream out = null;
        try {
			context.beforePropertiesSaved();
			out = new FileOutputStream(file);
			context.properties.store(out, null);
        } catch (Exception ex) {
			 Log.e(TAG, "Error occurred while storing properties", ex);
        } finally {
            try {
                if (out != null) {
					out.close();
				}
            } catch (IOException ex) {
				//Not much to do about it...
				 Log.e(TAG, "Failed to close output stream for file: " + context.getName(), ex);
			}
		}
	}

    public void savePropertyContexts() {
        contextMap.values().forEach(context -> {
            savePropertyContext(dir, context);
			});
	}

    public void registerPropertyContext(PropertyContext context) {
		contextMap.put(context.getName(), context);
	}
}
