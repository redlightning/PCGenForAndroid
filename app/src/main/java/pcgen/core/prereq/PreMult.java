/*
 * Copyright 2001 (C) Bryan McRoberts <merton_monk@yahoo.com>
 * Copyright 2003 (C) Chris Ward <frugal@purplewombat.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	   See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.core.prereq;

import org.apache.commons.lang3.StringUtils;

import pcgen.cdom.base.CDOMObject;
import pcgen.core.Equipment;
import pcgen.core.PlayerCharacter;
import android.util.Log;
import pcgen.system.LanguageBundle;


public class PreMult  extends AbstractPrerequisiteTest implements PrerequisiteTest {

    private static final String TAG = PreMult.class.getCanonicalName();
    /* (non-Javadoc)
	 * @see pcgen.core.prereq.PrerequisiteTest#passes(pcgen.core.PlayerCharacter)
	 */
    @Override
	public int passes(final Prerequisite prereq, final PlayerCharacter character, CDOMObject source) throws PrerequisiteException {
		int runningTotal=0;
		final int targetNumber = Integer.parseInt( prereq.getOperand() );

		for ( Prerequisite element : prereq.getPrerequisites() )
		{
			final PrerequisiteTestFactory factory = PrerequisiteTestFactory.getInstance();
			final PrerequisiteTest test = factory.getTest(element.getKind());
			if (test != null) {
				runningTotal += test.passes(element, character, source);
			}
			else {
				Log.e(TAG,"PreMult.cannot_find_subtest: " + element.getKind());
			}
		}

		runningTotal = prereq.getOperator().compare(runningTotal, targetNumber);

		return countedTotal(prereq, runningTotal);
	}



	/* (non-Javadoc)
	 * @see pcgen.core.prereq.PrerequisiteTest#kindsHandled()
	 */
    @Override
	public String kindHandled() {
		return "MULT";
	}

	/* (non-Javadoc)
	 * @see pcgen.core.prereq.PrerequisiteTest#passes(pcgen.core.prereq.Prerequisite, pcgen.core.Equipment)
	 */
    @Override
	public int passes(final Prerequisite prereq, final Equipment equipment, PlayerCharacter aPC) throws PrerequisiteException {
		int runningTotal=0;
		final int targetNumber = Integer.parseInt( prereq.getOperand() );

		for ( Prerequisite element : prereq.getPrerequisites() )
		{
			final PrerequisiteTestFactory factory = PrerequisiteTestFactory.getInstance();
			final PrerequisiteTest test = factory.getTest(element.getKind());
			runningTotal += test.passes(element, equipment, aPC);
		}

		runningTotal = prereq.getOperator().compare(runningTotal, targetNumber);
		return countedTotal(prereq, runningTotal);
	}


	/* (non-Javadoc)
	 * @see pcgen.core.prereq.PrerequisiteTest#toHtmlString(pcgen.core.prereq.Prerequisite)
	 */
    @Override
	public String toHtmlString(final Prerequisite prereq) {
		final PrerequisiteTestFactory factory = PrerequisiteTestFactory.getInstance();

		StringBuilder str = new StringBuilder(250);
		String delimiter = "";
		for ( Prerequisite element : prereq.getPrerequisites() )
		{
			final PrerequisiteTest test = factory.getTest(element.getKind());
			if (test==null)
			{
				Log.e(TAG,"PreMult.cannot_find_subformatter: " + element.getKind() );
			}
			else {
				str.append(delimiter);
				if (test instanceof PreMult && !delimiter.equals(""))
				{
					str.append("##BR##");
				}
				str.append(test.toHtmlString(element));
				delimiter = LanguageBundle.getString("PreMult.html_delimiter");
			}
		}

		// Handle some special cases - all required, one required or none required
		int numRequired = -1;
		if (StringUtils.isNumeric(prereq.getOperand()))
		{
			numRequired = Integer.parseInt(prereq.getOperand());
		}
		if ((prereq.getOperator() == PrerequisiteOperator.GTEQ
			|| prereq.getOperator() == PrerequisiteOperator.GT || prereq
			.getOperator() == PrerequisiteOperator.EQ)
			&& numRequired == prereq.getPrerequisites().size())
		{
			return LanguageBundle.getFormattedString("PreMult.toHtmlAllOf",
				str.toString());
		}
		if ((prereq.getOperator() == PrerequisiteOperator.GTEQ || prereq
				.getOperator() == PrerequisiteOperator.EQ) && numRequired == 1)
		{
			return LanguageBundle.getFormattedString("PreMult.toHtmlEither",
				str.toString());
		}
		if ((prereq.getOperator() == PrerequisiteOperator.LT && numRequired == 1)
			|| ((prereq.getOperator() == PrerequisiteOperator.EQ || prereq
				.getOperator() == PrerequisiteOperator.LTEQ) && numRequired == 0))
		{
			return LanguageBundle.getFormattedString("PreMult.toHtmlNone",
				str.toString());
		}
		
		return LanguageBundle.getFormattedString("PreMult.toHtml",
				prereq.getOperator().toDisplayString(),
				prereq.getOperand(),
				str.toString());

	}

}
