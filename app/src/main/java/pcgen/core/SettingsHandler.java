/*
 * Copyright 2001 (C) Jonas Karlsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import pcgen.base.lang.StringUtil;
import pcgen.cdom.base.Constants;
import pcgen.core.utils.CoreUtility;
import pcgen.core.utils.MessageType;
import pcgen.core.utils.ShowMessageDelegate;
import pcgen.core.utils.SortedProperties;
import pcgen.persistence.PersistenceManager;
import pcgen.system.ConfigurationSettings;
import pcgen.system.LanguageBundle;

import android.util.Log;

import net.redlightning.pcgen.R;

import org.apache.commons.lang3.SystemUtils;

/**
 * Should be cleaned up more.
 * <p>
 * <b>NB: This class is being gradually replaced with use of
 * {@link pcgen.system.PropertyContext} and its children.</b>
 **/
public final class SettingsHandler {
    private static final String TAG = SettingsHandler.class.getCanonicalName();

    private static boolean autoFeatsRefundable = false;
    private static boolean autogenExoticMaterial = false;
    private static boolean autogenMagic = false;
    private static boolean autogenMasterwork = false;
    private static boolean autogenRacial = false;
    public static boolean validateBonuses = false;

    //
    // For EqBuilder
    //
    private static int maxPotionSpellLevel = Constants.DEFAULT_MAX_POTION_SPELL_LEVEL;
    private static int maxWandSpellLevel = Constants.DEFAULT_MAX_WAND_SPELL_LEVEL;
    private static boolean allowMetamagicInCustomizer = false;
    private static boolean spellMarketPriceAdjusted = false;

    // Map of RuleCheck keys and their settings
    private static Map<String, String> ruleCheckMap = new HashMap<>();

    /**
     * That browserPath is set to null is intentional.
     */
    private static String browserPath = null; //Intentional null

    /**
     * See @javax.swing.SwingConstants.
     */
    private static int customizerSplit1 = -1;
    private static int customizerSplit2 = -1;
    private static boolean enforceSpendingBeforeLevelUp = false;
    private static final Properties FILTERSETTINGS = new Properties();
    public static GameMode game = new GameMode("default");
    private static boolean grimHPMode = false;
    private static boolean grittyACMode = false;
    private static boolean useWaitCursor = true;
    private static boolean loadURLs = false;
    private static boolean hpMaxAtFirstLevel = true;
    private static boolean hpMaxAtFirstClassLevel = true;
    private static boolean hpMaxAtFirstPCClassLevelOnly = true;
    private static int hpRollMethod = Constants.HP_STANDARD;
    private static int hpPercent = Constants.DEFAULT_HP_PERCENT;
    private static boolean ignoreMonsterHDCap = false;

    private static String invalidDmgText;
    private static String invalidToHitText;
    private static boolean gearTab_IgnoreCost = false;
    private static boolean gearTab_AutoResize = false;
    private static boolean gearTab_AllowDebt = false;
    private static int gearTab_SellRate = Constants.DEFAULT_GEAR_TAB_SELL_RATE;
    private static int gearTab_BuyRate = Constants.DEFAULT_GEAR_TAB_BUY_RATE;
    private static boolean isROG = false;
    private static final SortedProperties options = new SortedProperties();
    private static final Properties filepaths = new Properties();
    private static final String fileLocation = Globals.getFilepathsPath();
    private static File pccFilesLocation = null;
    private static File pcgPath = new File(Globals.getDefaultPath());
    private static File lastUsedPcgPath = null; // NB: This is not saved to preferences
    private static File backupPcgPath = null;
    private static boolean createPcgBackup = true;
    private static File portraitsPath = new File(Globals.getDefaultPath());
    private static File pcgenSponsorDir =
            new File(Globals.getDefaultPath()
                    + File.separator + "system"
                    + File.separator + "sponsors");

    /**
     * Where to load the system lst files from.
     */
    private static File pcgenOutputSheetDir =
            new File(Globals.getDefaultPath()
                    + File.separator + "outputsheets");
    private static File gmgenPluginDir =
            new File(Globals.getDefaultPath()
                    + File.separator + "plugins");
    private static int prereqQualifyColor = Constants.DEFAULT_PREREQ_QUALIFY_COLOUR;
    private static int prereqFailColor = Constants.DEFAULT_PREREQ_FAIL_COLOUR;
    private static boolean previewTabShown = false;
    private static File pcgenPreviewDir =
            new File(Globals.getDefaultPath()
                    + File.separator + "preview");//$NON-NLS-1$

    /////////////////////////////////////////////////
    private static boolean saveCustomInLst = false;
    private static String selectedCharacterHTMLOutputSheet = "";
    private static String selectedCharacterPDFOutputSheet = "";
    private static boolean saveOutputSheetWithPC = false;
    private static boolean printSpellsWithPC = true;
    private static String selectedPartyHTMLOutputSheet = "";
    private static String selectedPartyPDFOutputSheet = "";
    private static String selectedEqSetTemplate = "";
    private static String selectedSpellSheet = "";
    private static boolean showFeatDialogAtLevelUp = true;
    private static boolean showHPDialogAtLevelUp = true;
    private static boolean showStatDialogAtLevelUp = true;
    private static boolean showToolBar = true;
    private static boolean showSkillModifier = false;
    private static boolean showSkillRanks = false;
    private static boolean showWarningAtFirstLevelUp = true;
    private static String skinLFThemePack = null;
    private static boolean alwaysOverwrite = false;
    private static String defaultOSType = "";

    /**
     * See @javax.swing.SwingConstants
     */
    private static final String tmpPath = System.getProperty("java.io.tmpdir");
    private static final File tempPath = new File(getTmpPath());
    private static boolean useHigherLevelSlotsDefault = false;
    private static boolean wantToLoadMasterworkAndMagic = false;
    private static int nameDisplayStyle = Constants.DISPLAY_STYLE_NAME;
    private static boolean weaponProfPrintout = Constants.DEFAULT_PRINTOUT_WEAPONPROF;
    private static String postExportCommandStandard = "";
    private static String postExportCommandPDF = "";
    private static boolean hideMonsterClasses = false;
    private static boolean guiUsesOutputNameEquipment = false;
    private static boolean guiUsesOutputNameSpells = false;
    private static int lastTipShown = -1;
    private static boolean showMemoryArea = false;
    private static boolean showImagePreview = true;
    private static boolean showTipOfTheDay = true;
    private static boolean isGMGen = false;
    private static boolean showSingleBoxPerBundle = false;

    private SettingsHandler() {
    }


    public static String getSelectedGenerators(String string) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static void setSelectedGenerators(String prop, String generators) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static void setAlwaysOverwrite(final boolean argAlwaysOverwrite) {
        alwaysOverwrite = argAlwaysOverwrite;
    }

    public static boolean getAlwaysOverwrite() {
        return alwaysOverwrite;
    }

    public static void setDefaultOSType(final String argDefaultOSType) {
        defaultOSType = argDefaultOSType;
    }

    public static String getDefaultOSType() {
        return defaultOSType;
    }

    public static void setAutogen(final int idx, final boolean bFlag) {
        switch (idx) {
            case Constants.AUTOGEN_RACIAL:
                setAutogenRacial(bFlag);

                break;

            case Constants.AUTOGEN_MASTERWORK:
                setAutogenMasterwork(bFlag);

                break;

            case Constants.AUTOGEN_MAGIC:
                setAutogenMagic(bFlag);

                break;

            case Constants.AUTOGEN_EXOTIC_MATERIAL:
                setAutogenExoticMaterial(bFlag);

                break;

            default:
                break;
        }
    }

    public static boolean getAutogen(final int idx) {
        if (!wantToLoadMasterworkAndMagic()) {
            switch (idx) {
                case Constants.AUTOGEN_RACIAL:
                    return isAutogenRacial();

                case Constants.AUTOGEN_MASTERWORK:
                    return isAutogenMasterwork();

                case Constants.AUTOGEN_MAGIC:
                    return isAutogenMagic();

                case Constants.AUTOGEN_EXOTIC_MATERIAL:
                    return isAutogenExoticMaterial();

                default:
                    break;
            }
        }

        return false;
    }

    /**
     * Sets the path to the backup directory for character files.
     *
     * @param path the {@code File} representing the path
     */
    public static void setBackupPcgPath(final File path) {
        backupPcgPath = path;
    }

    /**
     * Returns the path to the backup directory for character files.
     *
     * @return the {@code backupPcgPath} property
     */
    public static File getBackupPcgPath() {
        return backupPcgPath;
    }

    /**
     * Sets the external browser path to use.
     *
     * @param path the {@code String} representing the path
     **/
    public static void setBrowserPath(final String path) {
        browserPath = path;
    }

    /**
     * Returns the external browser path to use.
     *
     * @return the {@code browserPath} property
     */
    public static String getBrowserPath() {
        return browserPath;
    }

    /**
     * Sets the flag to determine whether PCGen should backup pcg files before saving
     *
     * @param argCreatePcgBackup the {@code flag}
     */
    public static void setCreatePcgBackup(final boolean argCreatePcgBackup) {
        createPcgBackup = argCreatePcgBackup;
    }

    /**
     * Returns the flag to determine whether PCGen should backup pcg files before saving
     *
     * @return the {@code createPcgBackup} property
     */
    public static boolean getCreatePcgBackup() {
        return createPcgBackup;
    }

    public static void setCustomizerSplit1(final int split) {
        customizerSplit1 = split;
    }

    public static int getCustomizerSplit1() {
        return customizerSplit1;
    }

    public static void setCustomizerSplit2(final int split) {
        customizerSplit2 = split;
    }

    public static int getCustomizerSplit2() {
        return customizerSplit2;
    }

    /**
     * Sets whether PCgen will enforce the spending of all unallocated feats and skill points
     * before allowing the character to level up.
     *
     * @param argEnforceSpendingBeforeLevelUp Should spending be enforced?
     */
    public static void setEnforceSpendingBeforeLevelUp(final boolean argEnforceSpendingBeforeLevelUp) {
        enforceSpendingBeforeLevelUp = argEnforceSpendingBeforeLevelUp;
    }

    public static boolean getEnforceSpendingBeforeLevelUp() {
        return enforceSpendingBeforeLevelUp;
    }

    public static void setFilePaths(final String aString) {
        getFilepathProp().setProperty("pcgen.filepaths", aString);
    }

    public static String getFilePaths() {
        String def_type = "user";
        if (SystemUtils.IS_OS_MAC) {
            def_type = "mac_user";
        }
        return getFilepathProp().getProperty("pcgen.filepaths", def_type); //$NON-NLS-2$
    }

    public static Properties getFilepathProp() {
        return filepaths;
    }

    public static boolean getFirstRun() {
        // if filepaths.ini doesn't exist that means this is
        // the first time PCGen has been run
        final File aFile = new File(fileLocation);

        return !aFile.exists();

    }

    public static boolean isGMGen() {
        return isGMGen;
    }

    /**
     * Puts all properties into the {@code Properties} object,
     * ({@code options}). This is called by
     * {@code writeOptionsProperties}, which then saves the
     * {@code options} into a file.
     * <p>
     * I am guessing that named object properties are faster to access
     * than using the {@code getProperty} method, and that this is
     * why settings are stored as static properties of {@code Global},
     * but converted into a {@code Properties} object for
     * storage and retrieval.
     *
     * @param optionName
     * @param optionValue
     */
    public static void setGMGenOption(final String optionName, final boolean optionValue) {
        setGMGenOption(optionName, optionValue ? "true" : "false"); //$NON-NLS-2$
    }

    public static void setGMGenOption(final String optionName, final int optionValue) {
        setGMGenOption(optionName, String.valueOf(optionValue));
    }

    public static void setGMGenOption(final String optionName, final double optionValue) {
        setGMGenOption(optionName, String.valueOf(optionValue));
    }

    public static void setGMGenOption(final String optionName, final String optionValue) {
        getOptions().setProperty("gmgen.options." + optionName, optionValue);
    }

    /**
     * Set most of this objects static properties from the loaded {@code options}.
     * Called by readOptionsProperties. Most of the static properties are
     * set as a side effect, with the main screen size being returned.
     * <p>
     * I am guessing that named object properties are faster to access
     * than using the {@code getProperty} method, and that this is
     * why settings are stored as static properties of {@code Global},
     * but converted into a {@code Properties} object for
     * storage and retrieval.
     *
     * @param optionName
     * @param defaultValue
     * @return the default {@code Dimension} to set the screen size to
     */
    public static boolean getGMGenOption(final String optionName, final boolean defaultValue) {
        final String option = getGMGenOption(optionName, defaultValue ? "true" : "false"); //$NON-NLS-2$

        return "true".equalsIgnoreCase(option);
    }

    public static int getGMGenOption(final String optionName, final int defaultValue) {
        return Integer.decode(getGMGenOption(optionName, String.valueOf(defaultValue))).intValue();
    }

    public static Double getGMGenOption(final String optionName, final double defaultValue) {
        return new Double(getGMGenOption(optionName, Double.toString(defaultValue)));
    }

    public static String getGMGenOption(final String optionName, final String defaultValue) {
        return getOptions().getProperty("gmgen.options." + optionName, defaultValue);
    }

    public static void setGUIUsesOutputNameEquipment(final boolean argUseOutputNameEquipment) {
        guiUsesOutputNameEquipment = argUseOutputNameEquipment;
    }

    public static void setGUIUsesOutputNameSpells(final boolean argUseOutputNameSpells) {
        guiUsesOutputNameSpells = argUseOutputNameSpells;
    }

    public static void setGame(final String g) {
        final GameMode newMode = SystemCollections.getGameModeNamed(g);

        if (newMode != null) {
            game = newMode;
        }
        String key = g;
        // new key for game mode specific options are pcgen.options.gameMode.X.optionName
        // but offer downward compatible support to read in old version for unitSet from 5.8.0
        String unitSetName = getOptions().getProperty("pcgen.options.gameMode." + key + ".unitSetName",
                getOptions().getProperty("pcgen.options.unitSetName." + key, game.getDefaultUnitSet()));
        if (!game.selectUnitSet(unitSetName)) {
            if (!game.selectDefaultUnitSet()) {
                game.selectUnitSet(Constants.STANDARD_UNITSET_NAME);
            }
        }
        game.setDefaultXPTableName(getPCGenOption("gameMode." + key + ".xpTableName", "")); //$NON-NLS-2$
        game.setDefaultCharacterType(getPCGenOption("gameMode." + key + ".characterType", "")); //$NON-NLS-2$

        AbilityCategory featTemplate = game.getFeatTemplate();
        if (featTemplate != null) {
            AbilityCategory.FEAT.copyFields(featTemplate);
        }
        getChosenCampaignFiles(game);
    }

    public static GameMode getGame() {
        return game;
    }

    public static void setGearTab_AllowDebt(final boolean allowDebt) {
        gearTab_AllowDebt = allowDebt;
    }

    public static boolean getGearTab_AllowDebt() {
        return gearTab_AllowDebt;
    }

    public static void setGearTab_AutoResize(final boolean autoResize) {
        gearTab_AutoResize = autoResize;
    }

    public static boolean getGearTab_AutoResize() {
        return gearTab_AutoResize;
    }

    public static void setGearTab_BuyRate(final int argBuyRate) {
        gearTab_BuyRate = argBuyRate;
    }

    public static int getGearTab_BuyRate() {
        return gearTab_BuyRate;
    }

    public static void setGearTab_IgnoreCost(final boolean ignoreCost) {
        gearTab_IgnoreCost = ignoreCost;
    }

    public static boolean getGearTab_IgnoreCost() {
        return gearTab_IgnoreCost;
    }

    public static void setGearTab_SellRate(final int argSellRate) {
        gearTab_SellRate = argSellRate;
    }

    public static int getGearTab_SellRate() {
        return gearTab_SellRate;
    }

    public static void setGmgenPluginDir(final File aFile) {
        gmgenPluginDir = aFile;
    }

    public static File getGmgenPluginDir() {
        return gmgenPluginDir;
    }

    public static void setHPMaxAtFirstLevel(final boolean aBool) {
        hpMaxAtFirstLevel = aBool;
    }

    public static boolean isHPMaxAtFirstLevel() {
        return hpMaxAtFirstLevel;
    }

    public static void setHPMaxAtFirstClassLevel(final boolean aBool) {
        hpMaxAtFirstClassLevel = aBool;
    }

    public static boolean isHPMaxAtFirstClassLevel() {
        return hpMaxAtFirstClassLevel;
    }

    public static void setHPMaxAtFirstPCClassLevelOnly(final boolean aBool) {
        hpMaxAtFirstPCClassLevelOnly = aBool;
    }

    public static boolean isHPMaxAtFirstPCClassLevelOnly() {
        return hpMaxAtFirstPCClassLevelOnly;
    }

    public static void setHPPercent(final int argHPPct) {
        hpPercent = argHPPct;
    }

    public static int getHPPercent() {
        return hpPercent;
    }

    public static void setHPRollMethod(final int aBool) {
        hpRollMethod = aBool;
    }

    public static int getHPRollMethod() {
        return hpRollMethod;
    }

    public static String getHTMLOutputSheetPath() {
        if ("".equals(selectedCharacterHTMLOutputSheet))
        {
            return ConfigurationSettings.getOutputSheetsDir();
        }

        return new File(selectedCharacterHTMLOutputSheet).getParentFile().getAbsolutePath();
    }

    public static void setHideMonsterClasses(final boolean argHideMonsterClasses) {
        hideMonsterClasses = argHideMonsterClasses;
    }

    public static void setIgnoreMonsterHDCap(final boolean argIgoreCap) {
        ignoreMonsterHDCap = argIgoreCap;
    }

    public static boolean isIgnoreMonsterHDCap() {
        return ignoreMonsterHDCap;
    }

    /**
     * @param string The invalidDmgText to set.
     */
    public static void setInvalidDmgText(final String string) {
        SettingsHandler.invalidDmgText = string;
    }

    public static String getInvalidDmgText() {
        return invalidDmgText;
    }

    /**
     * @param string The invalidToHitText to set.
     */
    public static void setInvalidToHitText(final String string) {
        SettingsHandler.invalidToHitText = string;
    }

    public static String getInvalidToHitText() {
        return invalidToHitText;
    }

    public static void setLastTipShown(final int argLastTipShown) {
        lastTipShown = argLastTipShown;
    }

    public static int getLastTipShown() {
        return lastTipShown;
    }

    public static void setLoadURLs(final boolean aBool) {
        loadURLs = aBool;
    }

    public static boolean isLoadURLs() {
        return loadURLs;
    }

    public static void setMaxPotionSpellLevel(final int anInt) {
        maxPotionSpellLevel = anInt;
    }

    public static int getMaxPotionSpellLevel() {
        return maxPotionSpellLevel;
    }

    public static void setMaxWandSpellLevel(final int anInt) {
        maxWandSpellLevel = anInt;
    }

    public static int getMaxWandSpellLevel() {
        return maxWandSpellLevel;
    }

    public static void setMetamagicAllowedInEqBuilder(final boolean aBool) {
        allowMetamagicInCustomizer = aBool;
    }

    public static boolean isMetamagicAllowedInEqBuilder() {
        return allowMetamagicInCustomizer;
    }

    public static void setNameDisplayStyle(final int style) {
        nameDisplayStyle = style;
    }

    public static int getNameDisplayStyle() {
        return nameDisplayStyle;
    }

    public static SortedProperties getOptions() {
        return options;
    }



    /**
     * Retreive the chosen campaign files from properties for
     * use by the rest of PCGen.
     *
     * @param gameMode The GameMode to reteieve the files for.
     */
    private static void getChosenCampaignFiles(GameMode gameMode) {
        List<String> uriStringList =
                CoreUtility.split(getOptions().getProperty(
                        "pcgen.files.chosenCampaignSourcefiles." + gameMode.getName(),
                        ""), ',');
        List<URI> uriList = new ArrayList<>(uriStringList.size());
        for (String str : uriStringList) {
            try {
                uriList.add(new URI(str));
            } catch (URISyntaxException e) {
                Log.e(TAG, "Settings error: Unable to convert " + str
                        + " to a URI: " + e.getLocalizedMessage());
            }
        }
        PersistenceManager.getInstance().setChosenCampaignSourcefiles(uriList, gameMode);
    }

    public static void setOptionsProperties(final PlayerCharacter aPC) {
        if (getBackupPcgPath() != null && !getBackupPcgPath().getPath().equals("")) {
            getOptions().setProperty("pcgen.files.characters.backup", retractRelativePath(getBackupPcgPath().getAbsolutePath()));
        } else {
            getOptions().setProperty("pcgen.files.characters.backup", "");
        }

        getOptions().setProperty("pcgen.files.portraits", retractRelativePath(getPortraitsPath().getAbsolutePath()));
        getOptions().setProperty("pcgen.files.selectedSpellOutputSheet", retractRelativePath(getSelectedSpellSheet()));
        getOptions().setProperty("pcgen.files.selectedCharacterHTMLOutputSheet",
                retractRelativePath(getSelectedCharacterHTMLOutputSheet(aPC)));
        getOptions().setProperty("pcgen.files.selectedCharacterPDFOutputSheet",
                retractRelativePath(getSelectedCharacterPDFOutputSheet(aPC)));
        getOptions().setProperty("pcgen.files.selectedPartyHTMLOutputSheet",
                retractRelativePath(getSelectedPartyHTMLOutputSheet()));
        getOptions().setProperty("pcgen.files.selectedPartyPDFOutputSheet",
                retractRelativePath(getSelectedPartyPDFOutputSheet()));
        getOptions().setProperty("pcgen.files.selectedEqSetTemplate", retractRelativePath(getSelectedEqSetTemplate()));
        getOptions().setProperty("pcgen.files.chosenCampaignSourcefiles",
                StringUtil.join(PersistenceManager.getInstance().getChosenCampaignSourcefiles(), ", "));

        getOptions().setProperty("pcgen.options.custColumnWidth", StringUtil.join(Globals.getCustColumnWidth(), ", "));

        if (getGmgenPluginDir() != null) {
            getOptions().setProperty("gmgen.files.gmgenPluginDir",
                    retractRelativePath(getGmgenPluginDir().getAbsolutePath()));
        } else {
            getOptions().setProperty("gmgen.files.gmgenPluginDir", ""); //$NON-NLS-2$
        }

        if (getBrowserPath() != null) {
            setPCGenOption("browserPath", getBrowserPath());
        } else {
            setPCGenOption("browserPath", ""); //$NON-NLS-2$
        }

        if (getGame() != null) {
            setPCGenOption("game", getGame().getName());
        } else {
            setPCGenOption("game", ""); //$NON-NLS-2$
        }

        try {
            setPCGenOption("skinLFThemePack", getSkinLFThemePack());
        } catch (NullPointerException e) {
            //TODO: Should this really be ignored???  XXX
        }

        if (Globals.getRootFrame() != null) {
            setPCGenOption("windowWidth", Globals.getRootFrame().getDisplay().getWidth());
            setPCGenOption("windowHeight", Globals.getRootFrame().getDisplay().getHeight());
        }

        //
        // Remove old-style option values
        //
        setPCGenOption("allStatsValue", null);
        setPCGenOption("purchaseMethodName", null);
        setPCGenOption("rollMethod", null);
        setPCGenOption("rollMethodExpression", null);

        for (int idx = 0; idx < SystemCollections.getUnmodifiableGameModeList().size(); idx++) {
            final GameMode gameMode = SystemCollections.getUnmodifiableGameModeList().get(idx);
            String gameModeKey = gameMode.getName();
            if (gameMode.getUnitSet() != null && gameMode.getUnitSet().getDisplayName() != null) {
                setPCGenOption("gameMode." + gameModeKey + ".unitSetName", gameMode.getUnitSet().getDisplayName());
            }
            setPCGenOption("gameMode." + gameModeKey + ".purchaseMethodName", gameMode.getPurchaseModeMethodName());
            setPCGenOption("gameMode." + gameModeKey + ".rollMethod", gameMode.getRollMethod());
            setPCGenOption("gameMode." + gameModeKey + ".rollMethodExpression", gameMode.getRollMethodExpressionName());
            setPCGenOption("gameMode." + gameModeKey + ".allStatsValue", gameMode.getAllStatsValue());
            setPCGenOption("gameMode." + gameModeKey + ".xpTableName", gameMode.getDefaultXPTableName());
            setPCGenOption("gameMode." + gameModeKey + ".characterType", gameMode.getDefaultCharacterType());
        }

        setRuleChecksInOptions("ruleChecks");

        setPCGenOption("allowMetamagicInCustomizer", isMetamagicAllowedInEqBuilder());
        setPCGenOption("alwaysOverwrite", getAlwaysOverwrite());
        setPCGenOption("autoFeatsRefundable", isAutoFeatsRefundable());
        setPCGenOption("autoGenerateExoticMaterial", isAutogenExoticMaterial());
        setPCGenOption("autoGenerateMagic", isAutogenMagic());
        setPCGenOption("autoGenerateMasterwork", isAutogenMasterwork());
        setPCGenOption("autoGenerateRacial", isAutogenRacial());
        setPCGenOption("createPcgBackup", getCreatePcgBackup());
        setPCGenOption("customizer.split1", getCustomizerSplit1());
        setPCGenOption("customizer.split2", getCustomizerSplit2());
        setPCGenOption("defaultOSType", getDefaultOSType());
        setPCGenOption("GearTab.allowDebt", getGearTab_AllowDebt());
        setPCGenOption("GearTab.autoResize", getGearTab_AutoResize());
        setPCGenOption("GearTab.buyRate", getGearTab_BuyRate());
        setPCGenOption("GearTab.ignoreCost", getGearTab_IgnoreCost());
        setPCGenOption("GearTab.sellRate", getGearTab_SellRate());
        setPCGenOption("grimHPMode", isGrimHPMode());
        setPCGenOption("grittyACMode", isGrittyACMode());
        setPCGenOption("GUIUsesOutputNameEquipment", guiUsesOutputNameEquipment());
        setPCGenOption("GUIUsesOutputNameSpells", guiUsesOutputNameSpells());
        setPCGenOption("hideMonsterClasses", hideMonsterClasses());
        setPCGenOption("hpMaxAtFirstLevel", isHPMaxAtFirstLevel());
        setPCGenOption("hpMaxAtFirstClassLevel", isHPMaxAtFirstClassLevel());
        setPCGenOption("hpMaxAtFirstPCClassLevelOnly", isHPMaxAtFirstPCClassLevelOnly());
        setPCGenOption("hpPercent", getHPPercent());
        setPCGenOption("hpRollMethod", getHPRollMethod());
        setPCGenOption("ignoreMonsterHDCap", isIgnoreMonsterHDCap());
        setPCGenOption("invalidDmgText", getInvalidDmgText());
        setPCGenOption("invalidToHitText", getInvalidToHitText());
        setPCGenOption("lastTipOfTheDayTipShown", getLastTipShown());
        setPCGenOption("loadMasterworkAndMagicFromLst", wantToLoadMasterworkAndMagic());
        setPCGenOption("loadURLs", loadURLs);
        setPCGenOption("maxPotionSpellLevel", getMaxPotionSpellLevel());
        setPCGenOption("maxWandSpellLevel", getMaxWandSpellLevel());
        setPCGenOption("nameDisplayStyle", getNameDisplayStyle());
        setPCGenOption("postExportCommandStandard", SettingsHandler.getPostExportCommandStandard());
        setPCGenOption("postExportCommandPDF", SettingsHandler.getPostExportCommandPDF());
        setPCGenOption("prereqFailColor", "0x" + Integer.toHexString(getPrereqFailColor())); //$NON-NLS-2$
        setPCGenOption("prereqQualifyColor", "0x" + Integer.toHexString(getPrereqQualifyColor())); //$NON-NLS-2$
        setPCGenOption("previewTabShown", isPreviewTabShown());
        setPCGenOption("saveCustomInLst", isSaveCustomInLst());
        setPCGenOption("saveOutputSheetWithPC", getSaveOutputSheetWithPC());
        setPCGenOption("printSpellsWithPC", getPrintSpellsWithPC());
        setPCGenOption("showFeatDialogAtLevelUp", getShowFeatDialogAtLevelUp());
        setPCGenOption("enforceSpendingBeforeLevelUp", getEnforceSpendingBeforeLevelUp());
        setPCGenOption("showHPDialogAtLevelUp", getShowHPDialogAtLevelUp());
        setPCGenOption("showMemoryArea", isShowMemoryArea());
        setPCGenOption("showImagePreview", isShowImagePreview());
        setPCGenOption("showStatDialogAtLevelUp", getShowStatDialogAtLevelUp());
        setPCGenOption("showTipOfTheDay", getShowTipOfTheDay());
        setPCGenOption("showToolBar", isShowToolBar());
        setPCGenOption("showSkillModifier", getShowSkillModifier());
        setPCGenOption("showSkillRanks", getShowSkillRanks());
        setPCGenOption("showSingleBoxPerBundle", getShowSingleBoxPerBundle());
        setPCGenOption("showWarningAtFirstLevelUp", isShowWarningAtFirstLevelUp());
        setPCGenOption("sourceDisplay", Globals.getSourceDisplay().ordinal());
        setPCGenOption("spellMarketPriceAdjusted", isSpellMarketPriceAdjusted());
        setPCGenOption("useHigherLevelSlotsDefault", isUseHigherLevelSlotsDefault());
        setPCGenOption("useWaitCursor", getUseWaitCursor());
        setPCGenOption("validateBonuses", validateBonuses);
        setPCGenOption("weaponProfPrintout", SettingsHandler.getWeaponProfPrintout());
        setPCGenOption("outputDeprecationMessages", outputDeprecationMessages());
        setPCGenOption("inputUnconstructedMessages", inputUnconstructedMessages());
    }

    public static void setPCGenOption(final String optionName, final int optionValue) {
        setPCGenOption(optionName, String.valueOf(optionValue));
    }

    public static void setPCGenOption(final String optionName, final String optionValue) {
        if (optionValue == null) {
            getOptions().remove("pcgen.options." + optionName);
        } else {
            getOptions().setProperty("pcgen.options." + optionName, optionValue);
        }
    }

    public static int getPCGenOption(final String optionName, final int defaultValue) {
        return Integer.decode(getPCGenOption(optionName, String.valueOf(defaultValue))).intValue();
    }

    public static String getPCGenOption(final String optionName, final String defaultValue) {
        return getOptions().getProperty("pcgen.options." + optionName, defaultValue);
    }

    public static boolean hasPCGenOption(final String optionName) {
        return getOptions().containsKey("pcgen.options." + optionName);
    }

    public static String getPDFOutputSheetPath() {
        if ("".equals(selectedCharacterPDFOutputSheet))
        {
            return ConfigurationSettings.getOutputSheetsDir();
        }

        return new File(selectedCharacterPDFOutputSheet).getParentFile().getAbsolutePath();
    }

    /**
     * Ensures that the path specified exists.
     *
     * @param path the {@code File} representing the path
     */
    public static void ensurePathExists(final File path) {
        if (path != null && !path.exists()) {
            path.mkdirs();
        }
    }

    /**
     * Sets the path that was last used in a character or output file chooser.
     *
     * @param path the {@code File} representing the path
     */
    public static void setLastUsedPcgPath(final File path) {
        if (path != null && !path.exists()) {
            path.mkdirs();
        }
        lastUsedPcgPath = path;
    }

    /**
     * @return The path that was last used in a character or output file chooser.
     */
    public static File getLastUsedPcgPath() {
        if (lastUsedPcgPath == null) {
            return pcgPath;
        }
        return lastUsedPcgPath;
    }

    public static void setPcgenSponsorDir(final File aFile) {
        pcgenSponsorDir = aFile;
    }

    /**
     * @return the sponsor directory
     * @deprecated
     */
    @Deprecated
    public static File getPcgenSponsorDir() {
        return pcgenSponsorDir;
    }

    public static void setPcgenOutputSheetDir(final File aFile) {
        pcgenOutputSheetDir = aFile;
    }

    public static void setPcgenPreviewDir(final File aFile) {
        pcgenPreviewDir = aFile;
    }

    /**
     * Sets the path to the portrait files.
     *
     * @param path the {@code File} representing the path
     */
    public static void setPortraitsPath(final File path) {
        portraitsPath = path;
    }

    /**
     * @return the portraits directory
     * @deprecated Use PCGenSettings.getPortraitsDir()
     */
    @Deprecated
    public static File getPortraitsPath() {
        return portraitsPath;
    }

    public static void setPostExportCommandStandard(final String argPreference) {
        postExportCommandStandard = argPreference;
    }

    public static void setPostExportCommandPDF(final String argPreference) {
        postExportCommandPDF = argPreference;
    }

    public static String getPostExportCommandStandard() {
        return postExportCommandStandard;
    }

    public static String getPostExportCommandPDF() {
        return postExportCommandPDF;
    }

    public static void setPrereqFailColor(final int newColor) {
        prereqFailColor = newColor & 0x00FFFFFF;
    }

    public static int getPrereqFailColor() {
        return prereqFailColor;
    }

    public static String getPrereqFailColorAsHtmlStart() {
        final StringBuilder rString = new StringBuilder("<font color=");

        if (getPrereqFailColor() != 0) {
            rString.append("\"#").append(Integer.toHexString(getPrereqFailColor())).append("\""); //$NON-NLS-2$
        } else {
            rString.append("red");
        }

        rString.append('>');

        return rString.toString();
    }

    public static String getPrereqFailColorAsHtmlEnd() {
        return "</font>";
    }

    public static void setPrereqQualifyColor(final int newColor) {
        prereqQualifyColor = newColor & 0x00FFFFFF;
    }

    public static int getPrereqQualifyColor() {
        return prereqQualifyColor;
    }

    /**
     * Output spells on standard PC output sheet?
     *
     * @param arg
     **/
    public static void setPrintSpellsWithPC(final boolean arg) {
        printSpellsWithPC = arg;
    }

    public static boolean getPrintSpellsWithPC() {
        return printSpellsWithPC;
    }

    /**
     * I guess only ROG can document this?
     *
     * @return TRUR if ROG, else FALSE
     */
    public static boolean isROG() {
        return isROG;
    }

    /**
     * Set's the ruleCheckMap key to 'Y' or 'N'
     *
     * @param aKey
     * @param aBool
     **/
    public static void setRuleCheck(final String aKey, final boolean aBool) {
        String aVal = "N";

        if (aBool) {
            aVal = "Y";
        }

        ruleCheckMap.put(aKey, aVal);
    }

    /**
     * Gets this PC's choice on a Rule
     *
     * @param aKey
     * @return true or false
     **/
    public static boolean getRuleCheck(final String aKey) {
        if (ruleCheckMap.containsKey(aKey)) {
            final String aVal = ruleCheckMap.get(aKey);

            if (aVal.equals("Y"))
            {
                return true;
            }
        }

        return false;
    }

    public static void setSaveCustomEquipment(final boolean aBool) {
        setSaveCustomInLst(aBool);
    }

    /**
     * save the outputsheet location with the PC?
     *
     * @param arg
     **/
    public static void setSaveOutputSheetWithPC(final boolean arg) {
        saveOutputSheetWithPC = arg;
    }

    public static boolean getSaveOutputSheetWithPC() {
        return saveOutputSheetWithPC;
    }

    /**
     * Sets the current HTML output sheet for a single character.
     *
     * @param path a string containing the path to the HTML output sheet
     * @param aPC
     */
    public static void setSelectedCharacterHTMLOutputSheet(final String path, final PlayerCharacter aPC) {
        if (getSaveOutputSheetWithPC() && (aPC != null)) {
            aPC.setSelectedCharacterHTMLOutputSheet(path);
        }

        selectedCharacterHTMLOutputSheet = path;
    }

    /**
     * Returns the current HTML output sheet for a single character.
     *
     * @param aPC
     * @return the {@code selectedCharacterHTMLOutputSheet} property
     **/
    public static String getSelectedCharacterHTMLOutputSheet(final PlayerCharacter aPC) {
        if (getSaveOutputSheetWithPC() && (aPC != null)) {
            if (!aPC.getSelectedCharacterHTMLOutputSheet().isEmpty()) {
                return aPC.getSelectedCharacterHTMLOutputSheet();
            }
        }

        return selectedCharacterHTMLOutputSheet;
    }

    /**
     * Sets the current PDF output sheet for a single character.
     *
     * @param path a string containing the path to the PDF output sheet
     * @param aPC
     */
    public static void setSelectedCharacterPDFOutputSheet(final String path, final PlayerCharacter aPC) {
        if (getSaveOutputSheetWithPC() && (aPC != null)) {
            aPC.setSelectedCharacterPDFOutputSheet(path);
        }

        selectedCharacterPDFOutputSheet = path;
    }

    /**
     * Returns the current PDF output sheet for a single character.
     *
     * @param aPC
     * @return the {@code selectedCharacterPDFOutputSheet} property
     */
    public static String getSelectedCharacterPDFOutputSheet(final PlayerCharacter aPC) {
        if (getSaveOutputSheetWithPC() && (aPC != null)) {
            if (!aPC.getSelectedCharacterPDFOutputSheet().isEmpty()) {
                return aPC.getSelectedCharacterPDFOutputSheet();
            }
        }

        return selectedCharacterPDFOutputSheet;
    }

    /**
     * Sets the current EquipSet template.
     *
     * @param path a string containing the path to the template
     **/
    public static void setSelectedEqSetTemplate(final String path) {
        selectedEqSetTemplate = path;
    }

    /**
     * Returns the current EquipSet template.
     *
     * @return the {@code selectedEqSetTemplate} property
     **/
    public static String getSelectedEqSetTemplate() {
        return selectedEqSetTemplate;
    }

    public static String getSelectedEqSetTemplateName() {
        if (!selectedEqSetTemplate.isEmpty()) {
            final int i = selectedEqSetTemplate.lastIndexOf('\\');

            return selectedEqSetTemplate.substring(i + 1);
        }

        return selectedEqSetTemplate;
    }

    /**
     * Sets the current party HTML template.
     *
     * @param path a string containing the path to the template
     */
    public static void setSelectedPartyHTMLOutputSheet(final String path) {
        selectedPartyHTMLOutputSheet = path;
    }

    /**
     * Returns the current party HTML template.
     *
     * @return the {@code selectedPartyHTMLOutputSheet} property
     **/
    public static String getSelectedPartyHTMLOutputSheet() {
        return selectedPartyHTMLOutputSheet;
    }

    /**
     * Sets the current party PDF template.
     *
     * @param path a string containing the path to the template
     **/
    public static void setSelectedPartyPDFOutputSheet(final String path) {
        selectedPartyPDFOutputSheet = path;
    }

    /**
     * Returns the current party PDF template.
     *
     * @return the {@code selectedPartyPDFOutputSheet} property
     **/
    public static String getSelectedPartyPDFOutputSheet() {
        return selectedPartyPDFOutputSheet;
    }

    /**
     * Sets the current Spell output sheet
     *
     * @param path a string containing the path to the template
     **/
    public static void setSelectedSpellSheet(final String path) {
        selectedSpellSheet = path;
    }

    /**
     * Returns the current spell output sheet
     *
     * @return the {@code selectedSpellSheet} property
     **/
    public static String getSelectedSpellSheet() {
        return selectedSpellSheet;
    }

    public static String getSelectedSpellSheetName() {
        if (!selectedSpellSheet.isEmpty()) {
            final int i = selectedSpellSheet.lastIndexOf('\\');

            return selectedSpellSheet.substring(i + 1);
        }

        return selectedSpellSheet;
    }

    /**
     * Sets whether the feats dialog should be shown at level up.
     * NOTE: This function has been disabled as it interferes with class builds.
     *
     * @param argShowFeatDialogAtLevelUp Should the feats dialog be shown at level up?
     * @see <a href="https://sourceforge.net/tracker/index.php?func=detail&aid=1502512&group_id=25576&atid=384719">#1502512</a>
     */
    public static void setShowFeatDialogAtLevelUp(final boolean argShowFeatDialogAtLevelUp) {
        showFeatDialogAtLevelUp = true; //argShowFeatDialogAtLevelUp;
    }

    /**
     * Returns whether the feats dialog should be shown at level up.
     *
     * @return true if the feats dialog should be shown at level up.
     */
    public static boolean getShowFeatDialogAtLevelUp() {
        return showFeatDialogAtLevelUp;
    }

    /**
     * Sets whether the hit point dialog should be shown at level up.
     *
     * @param argShowHPDialogAtLevelUp Should the hit point dialog be shown at level up?
     */
    public static void setShowHPDialogAtLevelUp(final boolean argShowHPDialogAtLevelUp) {
        showHPDialogAtLevelUp = argShowHPDialogAtLevelUp;
    }

    /**
     * Returns whether the hit point dialog should be shown at level up.
     *
     * @return true if the hit point dialog should be shown at level up.
     */
    public static boolean getShowHPDialogAtLevelUp() {
        return showHPDialogAtLevelUp;
    }

    /**
     * Sets whether the Stat dialog should be shown at level up.
     *
     * @param argShowStatDialogAtLevelUp Should the Stat dialog should be shown at level up?
     */
    public static void setShowStatDialogAtLevelUp(final boolean argShowStatDialogAtLevelUp) {
        showStatDialogAtLevelUp = argShowStatDialogAtLevelUp;
    }

    /**
     * Returns whether the Stat dialog should be shown at level up.
     *
     * @return true if the Stat dialog should be shown at level up.
     */
    public static boolean getShowStatDialogAtLevelUp() {
        return showStatDialogAtLevelUp;
    }

    public static void setShowTipOfTheDay(final boolean argShowTipOfTheDay) {
        showTipOfTheDay = argShowTipOfTheDay;
    }

    public static boolean getShowTipOfTheDay() {
        return showTipOfTheDay;
    }

    /**
     * Sets the argShowWarningAtFirstLevelUp.
     *
     * @param argShowWarningAtFirstLevelUp The argShowWarningAtFirstLevelUp to set
     */
    public static void setShowWarningAtFirstLevelUp(final boolean argShowWarningAtFirstLevelUp) {
        SettingsHandler.showWarningAtFirstLevelUp = argShowWarningAtFirstLevelUp;
    }

    /**
     * Returns the showWarningAtFirstLevelUp.
     *
     * @return boolean
     */
    public static boolean isShowWarningAtFirstLevelUp() {
        return showWarningAtFirstLevelUp;
    }

    public static void setSkinLFThemePack(final String argSkinLFThemePack) {
        skinLFThemePack = argSkinLFThemePack;
    }

    public static String getSkinLFThemePack() {
        return skinLFThemePack;
    }

    /**
     * Returns the path to the temporary output location (for previews).
     *
     * @return the {@code tempPath} property
     */
    public static File getTempPath() {
        return tempPath;
    }

    public static void setToolBarShown(final boolean argShowToolBar) {
        setShowToolBar(argShowToolBar);
    }

    public static boolean isToolBarShown() {
        return isShowToolBar();
    }

    /**
     * @return Returns the useHigherLevelSlotsDefault.
     */
    public static boolean isUseHigherLevelSlotsDefault() {
        return useHigherLevelSlotsDefault;
    }

    /**
     * @param useHigherLevelSlotsDefault The useHigherLevelSlotsDefault to set.
     */
    public static void setUseHigherLevelSlotsDefault(
            boolean useHigherLevelSlotsDefault) {
        SettingsHandler.useHigherLevelSlotsDefault = useHigherLevelSlotsDefault;
    }

    public static void setUseWaitCursor(final boolean b) {
        useWaitCursor = b;
    }

    public static boolean getUseWaitCursor() {
        return useWaitCursor;
    }

    public static void setWantToLoadMasterworkAndMagic(final boolean bFlag) {
        wantToLoadMasterworkAndMagic = bFlag;
    }

    public static void setWeaponProfPrintout(final boolean argPreference) {
        weaponProfPrintout = argPreference;
    }

    public static boolean getWeaponProfPrintout() {
        return weaponProfPrintout;
    }

    public static boolean guiUsesOutputNameEquipment() {
        return guiUsesOutputNameEquipment;
    }

    public static boolean guiUsesOutputNameSpells() {
        return guiUsesOutputNameSpells;
    }

    /**
     * Checks to see if the user has set a value for this key
     *
     * @param aKey
     * @return true or false
     **/
    public static boolean hasRuleCheck(final String aKey) {
        return ruleCheckMap.containsKey(aKey);
    }

    public static boolean hideMonsterClasses() {
        return hideMonsterClasses;
    }

    public static void readGUIOptionsProperties() {
        setNameDisplayStyle(getPCGenOption("nameDisplayStyle", Constants.DISPLAY_STYLE_NAME));
    }

    /**
     * Opens the options.ini
     */
    public static void readOptionsProperties() {
        // read in the filepath.ini settings before anything else
        readFilePaths();

        // now get the Filter settings
        readFilterSettings();

        // Globals.getOptionsPath() will _always_ return a string
        final String optionsLocation = Globals.getOptionsPath();

        FileInputStream in = null;

        try {
            in = new FileInputStream(optionsLocation);
            getOptions().load(in);
        } catch (IOException e) {
            Log.d(TAG, "SettingsHandler - no options file");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                //Not much to do about it...
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_close_options_file), ex);
            }
        }
    }

    /**
     * retrieve filter settings
     * <p>
     * <br>author: Thomas Behr 19-02-02
     *
     * @param optionName the name of the property to retrieve
     * @return filter settings
     */
    public static String retrieveFilterSettings(final String optionName) {
        return getFilterSettings().getProperty("pcgen.filters." + optionName,
                getOptions().getProperty("pcgen.filters." + optionName, "")); //$NON-NLS-2$
    }

    public static boolean wantToLoadMasterworkAndMagic() {
        return wantToLoadMasterworkAndMagic;
    }

    private static String getPropertiesFileHeader(final String description) {
        return "# Emacs, this is -*- java-properties-generic -*- mode."//$NON-NLS-1$
                + Constants.LINE_SEPARATOR + "#" //$NON-NLS-2$
                + Constants.LINE_SEPARATOR + description
                + Constants.LINE_SEPARATOR + "# Do not edit this file manually."
                + Constants.LINE_SEPARATOR;
    }

    /**
     * Writes out filepaths.ini
     **/
    public static void writeFilePaths() {
        final String fType = getFilePaths();
        final String header = getPropertiesFileHeader(
                "# filepaths.ini -- location of other .ini files set in pcgen");

        if (!fType.equals("pcgen") && !fType.equals("user") && !fType.equals("mac_user")) //$NON-NLS-2$
        {
            if (fType != null) {
                setFilePaths(fType);
            }
        }

        // if it's the users home directory, we need to make sure
        // that the $HOME/.pcgen directory exists
        if (fType.equals("user"))
        {
            final String aLoc = System.getProperty("user.home") + File.separator + ".pcgen"; //$NON-NLS-2$
            final File aFile = new File(aLoc);

            if (!aFile.exists()) {
                // Directory doesn't exist, so create it
                aFile.mkdir();
                Log.e(TAG, LanguageBundle.getFormattedString(R.string.SettingsHandler_dir_does_not_exist, aLoc));
            } else if (!aFile.isDirectory()) {
                String notDir = LanguageBundle.getFormattedString(R.string.SettingsHandler_is_not_a_directory, aLoc);
                ShowMessageDelegate.showMessageDialog(
                        notDir,
                        Constants.APPLICATION_NAME,
                        MessageType.ERROR);
            }
        }

        // if it's the standard Mac user directory, we need to make sure
        // that the $HOME/Library/Preferences/pcgen directory exists
        if (fType.equals("mac_user"))
        {
            final String aLoc = Globals.defaultMacOptionsPath;
            final File aFile = new File(aLoc);

            if (!aFile.exists()) {
                // Directory doesn't exist, so create it
                aFile.mkdir();
                Log.e(TAG, LanguageBundle.getFormattedString(R.string.SettingsHandler_dir_does_not_exist, aLoc));
            } else if (!aFile.isDirectory()) {
                String notDir = LanguageBundle.getFormattedString(R.string.SettingsHandler_is_not_a_directory, aLoc);
                ShowMessageDelegate.showMessageDialog(
                        notDir,
                        Constants.APPLICATION_NAME,
                        MessageType.ERROR);
            }
        }

        FileOutputStream out = null;

        try {
            out = new FileOutputStream(fileLocation);
            getFilepathProp().store(out, header);
        } catch (FileNotFoundException fnfe) {
            final File f = new File(fileLocation);
            if (!f.canWrite()) {
                Log.e(TAG, LanguageBundle.getFormattedString(R.string.SettingsHandler_filepaths_readonly, fileLocation));
            } else {
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_filepaths_write), fnfe);
            }
        } catch (IOException e) {
            Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_filepaths_write), e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // Not much to do about it...
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_close_filepaths_ini_write), ex);
            }
        }
    }

    /**
     * Opens (options.ini) for writing and calls {@link SettingsHandler#setOptionsProperties(PlayerCharacter)}.
     *
     * @param aPC
     */
    public static void writeOptionsProperties(final PlayerCharacter aPC) {
        writeFilePaths();
        writeFilterSettings();

        // Globals.getOptionsPath() will _always_ return a string
        final String optionsLocation = Globals.getOptionsPath();
        final String header = getPropertiesFileHeader(
                "# options.ini -- options set in pcgen");

        // Make sure all the Properties are set
        setOptionsProperties(aPC);

        FileOutputStream out = null;

        try {
            out = new FileOutputStream(optionsLocation);
            getOptions().mystore(out, header);
        } catch (FileNotFoundException fnfe) {
            final File f = new File(fileLocation);
            if (!f.canWrite()) {
                Log.e(TAG, LanguageBundle.getFormattedString(R.string.SettingsHandler_options_ini_read_only, optionsLocation));
            } else {
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_write_options_ini), fnfe);
            }
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // Not much to do about it...
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_close_options_ini_write), ex);
            }
        }
    }

    static boolean isAutogenExoticMaterial() {
        return autogenExoticMaterial;
    }

    static boolean isAutogenMagic() {
        return autogenMagic;
    }

    static boolean isAutogenMasterwork() {
        return autogenMasterwork;
    }

    static boolean isAutogenRacial() {
        return autogenRacial;
    }

    static void setPreviewTabShown(final boolean showPreviewTab) {
        previewTabShown = showPreviewTab;
    }

    static boolean isPreviewTabShown() {
        return previewTabShown;
    }

    /**
     * Sets whether 'automatic' class-granted feats can be turned in for other feats
     *
     * @param argAutoFeatsRefundable
     */
    private static void setAutoFeatsRefundable(final boolean argAutoFeatsRefundable) {
        autoFeatsRefundable = argAutoFeatsRefundable;
    }

    /**
     * Returns whether 'automatic' class-granted feats can be turned in for other feats
     *
     * @return true if 'automatic' class-granted feats can be turned in for other feats
     */
    private static boolean isAutoFeatsRefundable() {
        return autoFeatsRefundable;
    }

    private static void setAutogenExoticMaterial(final boolean aBool) {
        autogenExoticMaterial = aBool;
    }

    private static void setAutogenMagic(final boolean aBool) {
        autogenMagic = aBool;
    }

    private static void setAutogenMasterwork(final boolean aBool) {
        autogenMasterwork = aBool;
    }

    private static void setAutogenRacial(final boolean aBool) {
        autogenRacial = aBool;
    }

    private static Properties getFilterSettings() {
        return FILTERSETTINGS;
    }

    private static void setGrimHPMode(final boolean argGrimHPMode) {
        grimHPMode = argGrimHPMode;
    }

    private static boolean isGrimHPMode() {
        return grimHPMode;
    }

    private static void setGrittyACMode(final boolean aBool) {
        grittyACMode = aBool;
    }

    private static boolean isGrittyACMode() {
        return grittyACMode;
    }

    /**
     * Puts all properties into the {@code Properties} object,
     * ({@code options}). This is called by
     * {@code writeOptionsProperties}, which then saves the
     * {@code options} into a file.
     * <p>
     * I am guessing that named object properties are faster to access
     * than using the {@code getProperty} method, and that this is
     * why settings are stored as static properties of {@code Global},
     * but converted into a {@code Properties} object for
     * storage and retrieval.
     *
     * @param optionName
     * @param optionValue
     */
    public static void setPCGenOption(final String optionName, final boolean optionValue) {
        setPCGenOption(optionName, optionValue ? "true" : "false"); //$NON-NLS-2$
    }

    public static void setPCGenOption(final String optionName, final double optionValue) {
        setPCGenOption(optionName, String.valueOf(optionValue));
    }

    /**
     * Set most of this objects static properties from the loaded {@code options}.
     * Called by readOptionsProperties. Most of the static properties are
     * set as a side effect, with the main screen size being returned.
     * <p>
     * I am guessing that named object properties are faster to access
     * than using the {@code getProperty} method, and that this is
     * why settings are stored as static properties of {@code Global},
     * but converted into a {@code Properties} object for
     * storage and retrieval.
     *
     * @param optionName
     * @param defaultValue
     * @return the default {@code Dimension} to set the screen size to
     */
    public static boolean getPCGenOption(final String optionName, final boolean defaultValue) {
        final String option = getPCGenOption(optionName, defaultValue ? "true" : "false"); //$NON-NLS-2$

        return "true".equalsIgnoreCase(option);
    }

    private static Double getPCGenOption(final String optionName, final double defaultValue) {
        return new Double(getPCGenOption(optionName, Double.toString(defaultValue)));
    }

    /**
     * What does this do???
     *
     * @param ROG
     */
    private static void setROG(final boolean ROG) {
        isROG = ROG;
    }

    /**
     * Set's the RuleChecks in the options.ini file
     *
     * @param optionName
     **/
    private static void setRuleChecksInOptions(final String optionName) {
        String value = "";

        for (Iterator<String> i = ruleCheckMap.keySet().iterator(); i.hasNext(); ) {
            final String aKey = i.next();
            final String aVal = ruleCheckMap.get(aKey);

            if (value.isEmpty()) {
                value = aKey + "|" + aVal;
            } else {
                value += ("," + aKey + "|" + aVal); //$NON-NLS-2$
            }
        }

        //setPCGenOption(optionName, value);
        getOptions().setProperty("pcgen.options." + optionName, value);
    }

    private static void setSaveCustomInLst(final boolean aBool) {
        saveCustomInLst = aBool;
    }

    private static boolean isSaveCustomInLst() {
        return saveCustomInLst;
    }

    private static void setShowToolBar(final boolean argShowToolBar) {
        showToolBar = argShowToolBar;
    }

    private static boolean isShowToolBar() {
        return showToolBar;
    }

    public static void setShowSkillModifier(final boolean argShowSkillMod) {
        showSkillModifier = argShowSkillMod;
    }

    public static boolean getShowSkillModifier() {
        return showSkillModifier;
    }

    public static void setShowSkillRanks(final boolean argShowSkillRanks) {
        showSkillRanks = argShowSkillRanks;
    }

    public static boolean getShowSkillRanks() {
        return showSkillRanks;
    }

    private static void setSpellMarketPriceAdjusted(final boolean aBool) {
        spellMarketPriceAdjusted = aBool;
    }

    private static boolean isSpellMarketPriceAdjusted() {
        return spellMarketPriceAdjusted;
    }

    private static String getTmpPath() {
        return tmpPath;
    }

    /*
     * If the path starts with an @ then it's a relative path
     */
    private static String expandRelativePath(String path) {
        if (path.startsWith("@"))
        {
            path = System.getProperty("user.dir") + path.substring(1);
        }

        return path;
    }

    /**
     * Parse all the user selected RuleChecks out of the options.ini file
     * of the form:
     * aKey|Y,bKey|N,cKey|Y
     *
     * @param aString
     **/
    private static void parseRuleChecksFromOptions(final String aString) {
        if (aString.length() <= 0) {
            return;
        }

        final StringTokenizer aTok = new StringTokenizer(aString, ",");

        while (aTok.hasMoreTokens()) {
            final String bs = aTok.nextToken();
            final StringTokenizer bTok = new StringTokenizer(bs, "|");
            final String aKey = bTok.nextToken();
            final String aVal = bTok.nextToken();
            ruleCheckMap.put(aKey, aVal);
        }
    }

    /**
     * Opens the filepaths.ini file for reading
     **/
    private static void readFilePaths() {
        FileInputStream in = null;

        try {
            in = new FileInputStream(fileLocation);
            getFilepathProp().load(in);
            String fType = SettingsHandler.getFilePaths();

            if ((fType == null) || (fType.length() < 1)) {
                // make sure we have a default
                if (SystemUtils.IS_OS_MAC) {
                    fType = "mac_user";
                } else {
                    fType = "user";
                }
            }
        } catch (IOException e) {
            // Not an error, this file may not exist yet
            Log.d(TAG, "SettingsHandler will create filepaths.ini");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                //Not much to do about it...
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_close_filepaths_ini), ex);
            }
        }
    }

    /**
     * Opens the filter.ini file for reading
     * <p>
     * <br>author: Thomas Behr 10-03-02
     **/
    private static void readFilterSettings() {
        // Globals.getFilterPath() will _always_ return a string
        final String filterLocation = Globals.getFilterPath();

        FileInputStream in = null;

        try {
            in = new FileInputStream(filterLocation);
            getFilterSettings().load(in);
        } catch (IOException e) {
            // Not an error, this file may not exist yet
            Log.d(TAG, "SettingsHandler will create filter.ini");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                //Not much to do about it...
                Log.e(TAG, "SettingsHandler can not close filter.ini", ex);
            }
        }
    }

    /*
     * setup relative paths
     */
    private static String retractRelativePath(String path) {
        final File userDir = new File(System.getProperty("user.dir"));

        if (path.startsWith(userDir.getAbsolutePath())) {
            path = "@" + path.substring(userDir.getAbsolutePath().length());
        }

        return path;
    }

    /**
     * Opens the filter.ini file for writing
     * <p>
     * <br>author: Thomas Behr 10-03-02
     */
    private static void writeFilterSettings() {
        // Globals.getFilterPath() will _always_ return a string
        final String filterLocation = Globals.getFilterPath();
        final String header = getPropertiesFileHeader(
                "# filter.ini -- filters set in pcgen");

        FileOutputStream out = null;

        try {
            out = new FileOutputStream(filterLocation);
            getFilterSettings().store(out, header);
        } catch (FileNotFoundException fnfe) {
            final File f = new File(fileLocation);
            if (!f.canWrite()) {
                Log.e(TAG, LanguageBundle.getFormattedString(R.string.SettingsHandler_filter_ini_readonly, filterLocation));
            } else {
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_write_filter_ini), fnfe);
            }
        } catch (IOException e) {
            Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_write_filter_ini), e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                //Not much to do about it...
                Log.e(TAG, LanguageBundle.getString(R.string.SettingsHandler_can_not_close_filter_ini_write), ex);
            }
        }

        // remove old filter stuff!
        for (Iterator<Object> it = getOptions().keySet().iterator(); it.hasNext(); ) {
            if (((String) it.next()).startsWith("pcgen.filters."))
            {
                it.remove();
            }
        }
    }

    /**
     * Shows the program memory use in the status bar if {@code true}.
     *
     * @return show memory setting for the status bar
     */
    public static boolean isShowMemoryArea() {
        return showMemoryArea;
    }

    /**
     * Shows character portrait preview in the file chooser if {@code true}.
     *
     * @return show portrait preview
     */
    public static boolean isShowImagePreview() {
        return showImagePreview;
    }

    /**
     * Toggles displaying the character portrait preview in the file chooser
     *
     * @param showImagePreview {@code true} to show portrait preview
     */
    public static void setShowImagePreview(final boolean showImagePreview) {
        SettingsHandler.showImagePreview = showImagePreview;
    }

    /**
     * @return The showSingleBoxPerBundle value.
     */
    public static boolean getShowSingleBoxPerBundle() {
        return showSingleBoxPerBundle;
    }

    /**
     * Set the showSingleBoxPerBundle value.
     *
     * @param b The new showSingleBoxPerBundle value.
     */
    public static void setShowSingleBoxPerBundle(boolean b) {
        showSingleBoxPerBundle = b;
    }

    private static boolean outputDeprecationMessages = true;

    public static boolean outputDeprecationMessages() {
        return outputDeprecationMessages;
    }

    public static void setOutputDeprecationMessages(boolean b) {
        outputDeprecationMessages = b;
    }

    private static boolean inputUnconstructedMessages = true;

    public static boolean inputUnconstructedMessages() {
        return inputUnconstructedMessages;
    }

    public static void setInputUnconstructedMessages(boolean b) {
        inputUnconstructedMessages = b;
    }

}
