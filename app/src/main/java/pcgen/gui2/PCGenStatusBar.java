/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 * Android Conversion by Michael Isaacson <michael@redlightning.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.gui2;

import java.util.List;
import java.util.logging.LogRecord;

import pcgen.gui2.util.StatusWorker;
import pcgen.gui2.util.SwingWorker;
import pcgen.system.PCGenTask;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.redlightning.pcgen.MainActivity;
import net.redlightning.pcgen.R;

/**
 * This is the southern component of the MainActivity.
 * It will show source loading progress and a corresponding error icon
 * (if there are errors)
 * TODO: add support for concurrent task execution
 */
public final class PCGenStatusBar extends View
{
	private final MainActivity frame;
	private final TextView messageLabel;
	private final ProgressBar progressBar;
	private final TextView loadStatusLabel;

	public PCGenStatusBar(Context context){
		super(context);
		this.frame = null;

		View layout = LayoutInflater.from(context).inflate(R.layout.panel_status_bar, null);
		this.loadStatusLabel = layout.findViewById(R.id.status_load_status_label);
		this.messageLabel = layout.findViewById(R.id.status_message_label);
		this.progressBar = layout.findViewById(R.id.status_progress);
	}

	public PCGenStatusBar(MainActivity frame)
	{
		super(frame);
		this.frame = frame;
		View layout = LayoutInflater.from(frame).inflate(R.layout.panel_status_bar, null);
		this.loadStatusLabel = layout.findViewById(R.id.status_load_status_label);
		this.messageLabel = layout.findViewById(R.id.status_message_label);
		this.progressBar = layout.findViewById(R.id.status_progress);
		progressBar.setVisibility(View.INVISIBLE);
	}

	public void setContextMessage(String message)
	{
		messageLabel.setText(message);
	}

	public String getContextMessage()
	{
		return messageLabel.getText().toString();
	}

	public ProgressBar getProgressBar()
	{
		return progressBar;
	}

	public void setSourceLoadErrors(List<LogRecord> errors) {
		if (errors != null && !errors.isEmpty()) {
			loadStatusLabel.setCompoundDrawables(getContext().getDrawable(R.drawable.alert16), null, null, null);
		} else {
			loadStatusLabel.setCompoundDrawables(getContext().getDrawable(R.drawable.ok16), null, null, null);
		}
	}

	/**
	 * This creates a swing worker that encapsulates a PCGenTask.
	 * As the worker is executed information regarding its progress
	 * will be updated to the PCGenStatusBar's progress bar.
	 * Upon completion of the task execution the worker returns a
	 * list of error messages that occurred during the task execution.
	 * Its up to the caller of this method figure out what to do with
	 * the messages (if any).
	 * @param taskName a string describing the task
	 * @param task a PCGenTask
	 * @return a SwingWorker
	 */
	public SwingWorker<List<LogRecord>> createWorker(String taskName, PCGenTask task)
	{
		return new StatusWorker(taskName, task, this);
	}

	public MainActivity getFrame()
	{
		return frame;
	}

	/**
	 * Shows the progress bar, in indeterminate mode
	 * 
	 * @param msg message to show on status bar
	 * @param indeterminate show as an indeterminate when true
	 */
	public void startShowingProgress(final String msg, boolean indeterminate)
	{
		setVisibility(View.VISIBLE);
		setContextMessage(msg);
		getProgressBar().setVisibility(View.VISIBLE);
		getProgressBar().setIndeterminate(indeterminate);
		getProgressBar().setTooltipText(msg);
	}

	/**
	 * Clears the progress bar and turns off the wait cursor
	 */
	public void endShowingProgress()
	{
		setContextMessage(null);
		getProgressBar().setTooltipText(null);
		getProgressBar().setVisibility(View.INVISIBLE);
	}
}
