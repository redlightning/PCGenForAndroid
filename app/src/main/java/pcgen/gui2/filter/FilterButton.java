/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 * Android Conversion by Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.gui2.filter;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;

import com.google.android.material.switchmaterial.SwitchMaterial;

import org.apache.commons.lang3.StringUtils;

import pcgen.gui2.UIPropertyContext;
import pcgen.system.PropertyContext;
import timber.log.Timber;

/**
 * This class represents a simple filter represented as a toggle button. When the button is selected
 * (i.e. pressed) the filter assigned to this button will become active. When deselected the filter
 * will become inactive. Selecting and deselecting the button will trigger its FilterHandler
 * to refilter its contents.
 */
public class FilterButton<C, E> extends SwitchMaterial implements DisplayableFilter<C, E> {

    private FilterHandler filterHandler;
    private Filter<C, E> filter;
    private final PropertyContext filterContext;

    public FilterButton(Context context){
    	super(context);
    	filterContext = null;
    	Timber.w("Use of default FilterButton constructor");
	}

    public FilterButton(Context context, String prefKey) {
        this(context, prefKey, false);
    }

    public FilterButton(Context context, String prefKey, boolean defaultSelectedState) {
        super(context);
        if (StringUtils.isEmpty(prefKey)) {
            throw new NullPointerException("prefKey cannot be null");
        }

        PropertyContext baseContext = UIPropertyContext.createContext("filterPrefs");
        filterContext = baseContext.createChildContext(prefKey);
        setSelected(filterContext.initBoolean("active", defaultSelectedState));
        this.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                filterHandler.refilter();
                filterContext.setBoolean("active", isSelected());
            }
        });
    }

    @Override
    public View getFilterComponent() {
        return this;
    }

    @Override
    public void setFilterHandler(FilterHandler handler) {
        this.filterHandler = handler;
    }

    public void setFilter(Filter<C, E> filter) {
        this.filter = filter;
    }

    @Override
    public boolean accept(C context, E element) {
        //If this button is not selected treat it as if this filter always accepts
        return !isEnabled() || !isSelected() || filter.accept(context, element);
    }
}
