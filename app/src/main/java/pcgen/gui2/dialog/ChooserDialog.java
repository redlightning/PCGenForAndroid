/*
 * Copyright James Dempsey, 2012
 * Android port Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.redlightning.pcgen.R;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import pcgen.core.chooser.InfoWrapper;
import pcgen.facade.core.ChooserFacade;
import pcgen.facade.core.InfoFacade;
import pcgen.facade.util.ListFacade;

/**
 * The Class {@code ChooserDialog} provides a general dialog to allow the
 * user to select from a number of predefined choices. A ChooserFacade instance 
 * must be supplied, this defines the choices available, the text to be displayed 
 * on screen and the actions to be taken when the user confirms their choices. The 
 * chooser is generally displayed via a call to UIDelgate.showGeneralChooser.
 * <p>
 * This class is based heavily on Connor Petty's LanguageChooserDialog class.
 */
public class ChooserDialog extends AlertDialog {
	private final ChooserFacade chooser;
	private ListView availTable;
	private EditText availInput;
	private TextView remainingLabel;
	private final ListFacade<InfoFacade> availableList;
    private final ListFacade<InfoFacade> selectedList;
	private boolean committed;

	/**
	 * Create a new instance of ChooserDialog for selecting from the data supplied in the chooserFacade. 
	 * @param context The context we are opening relative to.
	 * @param chooser The definition of what should be displayed.
	 */
	public ChooserDialog(Context context, ChooserFacade chooser) {
        super(context);
        setCancelable(true);
        this.chooser = chooser;
		availableList = chooser.getAvailableList();
        selectedList = chooser.getSelectedList();
		initComponents(context);
	}

	private void initComponents(Context context)
	{
		setTitle(chooser.getName());
        View root;
        if (chooser.isUserInput()) {
            root = LayoutInflater.from(context).inflate(R.layout.dialog_chooser_input, null);
            availInput = root.findViewById(R.id.chooser_input);
        } else {
            root = LayoutInflater.from(context).inflate(R.layout.dialog_chooser_list, null);
            availTable = new ListView(context);
            availTable.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            availTable.setItemsCanFocus(false);
            List<InfoFacade> items = new ArrayList<>();

            for (InfoFacade item : availableList) {
                items.add(item);
            }

            for (InfoFacade item : selectedList) {
                items.add(item);
            }

            //TODO Sort alphabetically
            ArrayAdapter<InfoFacade> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_multiple_choice, items);
            availTable.setAdapter(adapter);

            for (InfoFacade item : chooser.getSelectedList()) {
                availTable.setItemChecked(adapter.getPosition(item), true);
            }

            availTable.setOnItemClickListener((adapterView, view1, position, id) -> {
                if (availTable.isItemChecked(position)) {
                    chooser.addSelected((InfoFacade) availTable.getItemAtPosition(position));
                } else {
                    chooser.removeSelected((InfoFacade) availTable.getItemAtPosition(position));
                }
                int count = chooser.getRemainingSelections().get();
                remainingLabel.setText(context.getString(R.string.in_selRemain) + " " + count);
            });

            remainingLabel = root.findViewById(R.id.chooser_remaining);
        }


		setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_ok), ((dialogInterface, i) -> {
		    if(availInput != null) {
                String data = availInput.getText().toString();
                if (StringUtils.isNotBlank(data)) {
                    chooser.addSelected(new InfoWrapper(data));
                    chooser.commit();
                    dismiss();
                    committed = true;
                } else {
                    Toast.makeText(context, "Field cannot be left blank", Toast.LENGTH_SHORT).show();
                }
                availInput.setText("");
            } else {
                if (chooser.isRequireCompleteSelection() && chooser.getRemainingSelections().get() > 0) {
                    Toast.makeText(context, context.getString(R.string.in_chooserRequireComplete) + chooser.getRemainingSelections().get() + chooser.getName(), Toast.LENGTH_LONG).show();
                } else {
                    chooser.commit();
                    dismiss();
                    committed = true;
                }
            }
        }));

		setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_cancel), ((dialogInterface, i) -> {
            chooser.rollback();
            dismiss();
        }));
	}

	/**
	 * Returns the means by which the dialog was closed.   
	 * @return the committed status, false for cancelled, true for OKed. 
	 */
	public boolean isCommitted() {
        return committed;
    }
}
