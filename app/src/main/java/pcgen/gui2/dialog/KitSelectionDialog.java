/*
 * Copyright Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;

import net.redlightning.pcgen.R;

import pcgen.facade.core.CharacterFacade;
import pcgen.gui2.kits.KitPanel;

/**
 * The Class {@code KitSelectionDialog} provides a pop-up dialog that allows
 * the user to add kits to a character. Kits are prepared groups of equipment and 
 * other rules items.  
 */
public class KitSelectionDialog extends AlertDialog {

	/**
	 * Create a new instance of KitSelectionDialog
     * @param context The parent frame we are displaying over.
	 * @param character The character being displayed.
	 */
    public KitSelectionDialog(Context context, CharacterFacade character) {
        super(context);
        setCancelable(true);
        setTitle(context.getString(R.string.in_mnuEditAddKit));
        setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_close), (dialogInterface, i) -> dismiss());
        KitPanel kitPanel = new KitPanel(context, character);
        setView(kitPanel);
	}
}
