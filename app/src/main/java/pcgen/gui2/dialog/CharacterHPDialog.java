/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import pcgen.core.RollingMethods;
import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.CharacterLevelFacade;
import pcgen.facade.core.CharacterLevelsFacade;
import pcgen.facade.core.ClassFacade;


public final class CharacterHPDialog extends AlertDialog {

    private final CharacterLevelsFacade levels;
    private int hpTotal = 0;


    private CharacterHPDialog(Context context, CharacterFacade character) {
        super(context);
        this.levels = character.getCharacterLevelsFacade();

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.dialog_character_hp, null);
        TextView totalHp = layout.findViewById(R.id.character_hp_total);
        Button rerollAllButton = layout.findViewById(R.id.character_hp_reroll);
        rerollAllButton.setOnClickListener(View -> {
            for (int l = 0; l < levels.getSize(); l++) {
                CharacterLevelFacade level = levels.getElementAt(l);
                Integer i = Integer.valueOf(levels.getClassTaken(level).getHD());
                int rolled = RollingMethods.roll(i);
                levels.setHPRolled(level, rolled);
            }
        });

        TableLayout table = layout.findViewById(R.id.character_hp_table);

        for (int lvl = 0; lvl < levels.getSize(); lvl++) {
            CharacterLevelFacade level = levels.getElementAt(lvl);
            ClassFacade c = levels.getClassTaken(level);

            TableRow row = new TableRow(context);

            TextView level_view = new TextView(context);
            level_view.setText(String.valueOf(lvl + 1));

            TextView class_view = new TextView(context);
            class_view.setText(c.getAbbrev());

            TextView sides_view = new TextView(context);
            sides_view.setText(c.getHD());

            TextView total_view = new TextView(context);
            total_view.setText(String.valueOf(levels.getHPGained(level)));
            hpTotal += levels.getHPGained(level);

            TextView adj_view = new TextView(context);
            adj_view.setText(levels.getHPGained(level) - levels.getHPRolled(level));

            TextView rolled_view = new TextView(context);
            rolled_view.setText(levels.getHPRolled(level));
            rolled_view.setTag("Rolled" + (lvl + 1));

            Button reroll_view = new Button(context);
            reroll_view.setText(R.string.reroll);
            reroll_view.setTag(Integer.valueOf(lvl + 1)); //To pass in the level # to the listener
            reroll_view.setOnClickListener((View v) -> {
                int currentLevel = (Integer) v.getTag();
                CharacterLevelFacade levelFacade = levels.getElementAt(currentLevel);
                Integer i = Integer.valueOf(levels.getClassTaken(levelFacade).getHD());

                hpTotal = hpTotal - levels.getHPRolled(levelFacade);

                int rolled = RollingMethods.roll(i);
                levels.setHPRolled(levelFacade, rolled);
                //Update Rolled cell
                TextView rolledView = table.findViewWithTag("Rolled" + currentLevel);
                rolledView.setText(String.valueOf(rolled));

                // Update total
                hpTotal = hpTotal + rolled;
                total_view.setText(context.getString(R.string.in_iskTotal) + " " + hpTotal);
            });

            row.addView(level_view);
            row.addView(class_view);
            row.addView(sides_view);
            row.addView(total_view);
            row.addView(adj_view);
            row.addView(rolled_view);
            row.addView(reroll_view);

            table.addView(row);

            total_view.setText(context.getString(R.string.in_iskTotal) + " " + hpTotal);
        }
    }

    public static void showHPDialog(Context context, CharacterFacade character) {
        CharacterHPDialog dialog = new CharacterHPDialog(context, character);
        dialog.show();
    }
}
