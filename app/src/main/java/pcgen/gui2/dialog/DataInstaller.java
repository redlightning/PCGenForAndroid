/*
 * Copyright Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package pcgen.gui2.dialog;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import pcgen.cdom.enumeration.Destination;
import pcgen.cdom.enumeration.ObjectKey;
import pcgen.cdom.enumeration.StringKey;
import pcgen.core.InstallableCampaign;
import pcgen.core.utils.CoreUtility;
import pcgen.persistence.PersistenceLayerException;
import pcgen.persistence.lst.InstallLoader;
import pcgen.system.ConfigurationSettings;
import pcgen.system.FacadeFactory;
import pcgen.system.LanguageBundle;
import pcgen.system.PCGenSettings;
import timber.log.Timber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import net.redlightning.pcgen.R;

/**
 * {@code DataInstaller} is responsible for managing the installation of
 * a data set including the selection of the set and the install options.
 */
public class DataInstaller extends Fragment {
    private static final String OUTPUTSHEETS_FOLDER = "outputsheets/";
    private static final String DATA_FOLDER = "data/";
    private TextView dataSetSel;
    private WebView dataSetDetails;
    private RadioButton locDataButton;
    private RadioButton locVendorDataButton;
    private RadioButton locHomebrewDataButton;
    private File currDataSet;
    private File currFolder;

    public DataInstaller(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.dialog_data_installer, null);
        currFolder = new File(context.getApplicationInfo().dataDir);
        dataSetDetails = layout.findViewById(R.id.di_set_info);
		dataSetSel = layout.findViewById(R.id.di_dataset);
		locDataButton = layout.findViewById(R.id.di_data);
		locVendorDataButton = layout.findViewById(R.id.di_vendor);
		locHomebrewDataButton = layout.findViewById(R.id.di_homebrew);

		Button selectButton = layout.findViewById(R.id.di_select_button);
        selectButton.setOnClickListener(view -> {
            new com.obsez.android.lib.filechooser.ChooserDialog(context)
                    .withStartFile(context.getApplicationInfo().dataDir)
                    .withFilter(false, "zip", "pcz")
                    .withChosenListener((path, pathFile) -> {
                        currDataSet = new File(path);
                        readDataSet(currDataSet);
                    })
                    .withOnCancelListener(DialogInterface::cancel)
                    .build()
                    .show();
		});

        Button installButton = layout.findViewById(R.id.di_install_button);
        installButton.setOnClickListener(view -> {
            if (installDataSource(currDataSet, getSelectedDestination())) {
                //PCGen_Frame1.getInst().getMainSource().refreshCampaigns();
                //TODO: Refresh the data cleanly.
//					PersistenceManager.getInstance().refreshCampaigns();
//					FacadeFactory.refresh();
            }
        });
    }

    /**
     * Install a data set (campaign) into the current PCGen install.
     *
     * @param dataSet the data set (campaign) to be installed.
     * @param dest    The location the data is to be installed to.
     * @return true, if install data source
     */
    private boolean installDataSource(File dataSet, Destination dest) {
        // Get the directory the data is to be stored in
        if (dataSet == null) {
            Toast.makeText(this.getContext(), R.string.in_diDataSetNotSelected, Toast.LENGTH_LONG).show();
            return false;
        }
        if (dest == null) {
            Toast.makeText(this.getContext(), R.string.in_diDataFolderNotSelected, Toast.LENGTH_LONG).show();
            return false;
        }
        File destDir;
        switch (dest) {
            case VENDORDATA:
                destDir = new File(PCGenSettings.getVendorDataDir());
                break;

            case HOMEBREWDATA:
                destDir = new File(PCGenSettings.getHomebrewDataDir());
                break;

            case DATA:
            default:
                destDir = new File(ConfigurationSettings.getPccFilesDir());
                break;
        }

        // Check chosen dir exists
        if (!destDir.exists()) {
            Toast.makeText(this.getContext(), LanguageBundle
                    .getFormattedString(R.string.in_diDataFolderNotExist, destDir
                            .getAbsoluteFile()), Toast.LENGTH_LONG).show();
            return false;
        }

        // Scan for non standard files and files that would be overwritten
        List<String> directories = new ArrayList<>();
        List<String> files = new ArrayList<>();
        if (!populateFileAndDirLists(dataSet, directories, files)) {
            return false;
        }
        if (!checkNonStandardOK(files)) {
            return false;
        }
        if (!checkOverwriteOK(files, destDir)) {
            return false;
        }

        if (!createDirectories(directories, destDir)) {
            return false;
        }

        // Navigate through the zip file, processing each file
        return createFiles(dataSet, destDir, files);
    }

    /**
     * Gets the currently selected destination.
     *
     * @return the selected destination
     */
    private Destination getSelectedDestination() {
        if (locDataButton.isSelected()) {
            return Destination.DATA;
        }
        if (locVendorDataButton.isSelected()) {
            return Destination.VENDORDATA;
        }
        if (locHomebrewDataButton.isSelected()) {
            return Destination.HOMEBREWDATA;
        }
        return null;
    }

    /**
     * Read data set.
     *
     * @param dataSet the data set
     * @return true, if successful
     */
    private boolean readDataSet(File dataSet) {
        // Open the ZIP file
		InstallableCampaign campaign;
		try (ZipFile in = new ZipFile(dataSet)) {
            // Get the install file in a case insensitive manner
            ZipEntry installEntry = null;
            @SuppressWarnings("rawtypes")
            Enumeration entries = in.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                if (entry.getName().equalsIgnoreCase("install.lst")) {
                    installEntry = entry;
                    break;
                }
            }
            if (installEntry == null) {
                Timber.e("File " + dataSet + " is not a valid dataset - no Install.lst file");
                Toast.makeText(getContext(), LanguageBundle.getFormattedString(R.string.in_diNoInstallFile, dataSet.getName()), Toast.LENGTH_LONG).show();
                in.close();
                return false;
            }

            // Parse the install file
            InputStream inStream = in.getInputStream(installEntry);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream, StandardCharsets.UTF_8));
            StringBuilder installInfo = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                installInfo.append(line).append("\n");
            }

            final InstallLoader loader = new InstallLoader();
            loader.loadLstString(null, dataSet.toURI(), installInfo.toString());
            campaign = loader.getCampaign();
        } catch (IOException e) {
            Timber.e(e, "Failed to read data set " + dataSet + " due to ");
			Toast.makeText(getContext(), LanguageBundle.getFormattedString(R.string.in_diBadDataSet, dataSet), Toast.LENGTH_LONG).show();
            return false;
        } catch (PersistenceLayerException e) {
            Timber.e(e, "Failed to parse data set " + dataSet + " due to ");
			Toast.makeText(getContext(), LanguageBundle.getFormattedString(R.string.in_diBadDataSet, dataSet), Toast.LENGTH_LONG).show();
            return false;
        }

        // Validate that the campaign is compatible with our version
        campaign.getSafe(StringKey.MINDEVVER);
        if (!CoreUtility.isPriorToCurrent(campaign.getSafe(StringKey.MINDEVVER))) {
            if (CoreUtility.isCurrMinorVer(campaign.getSafe(StringKey.MINDEVVER))) {
                Timber.e("Dataset " + campaign.getDisplayName()
                        + " needs at least PCGen version "
                        + campaign.getSafe(StringKey.MINDEVVER)
                        + " to run. It could not be installed.");
                Toast.makeText(getContext(), LanguageBundle.getFormattedString(R.string.in_diVersionTooOldDev, campaign.getSafe(StringKey.MINDEVVER), campaign.getSafe(StringKey.MINVER)), Toast.LENGTH_LONG).show();
                return false;
            }
        }
        campaign.getSafe(StringKey.MINVER);
        if (!CoreUtility.isPriorToCurrent(campaign.getSafe(StringKey.MINVER))) {
            Timber.e("Dataset " + campaign.getDisplayName()
                    + " needs at least PCGen version " + campaign.getSafe(StringKey.MINVER)
                    + " to run. It could not be installed.");
            Toast.makeText(this.getContext(), LanguageBundle.getFormattedString(R.string.in_diVersionTooOld, campaign.getSafe(StringKey.MINVER)), Toast.LENGTH_LONG).show();
            return false;
        }

        // Display the info
        dataSetSel.setText(dataSet.getAbsolutePath());
        dataSetDetails.loadData(FacadeFactory.getCampaignInfoFactory().getHTMLInfo(campaign), "text/html; charset=utf-8", "utf-8");
        if (campaign.get(ObjectKey.DESTINATION) == null) {
            locDataButton.setSelected(false);
            locVendorDataButton.setSelected(false);
            locHomebrewDataButton.setSelected(false);
        } else {
            switch (campaign.get(ObjectKey.DESTINATION)) {
                case DATA:
                    locDataButton.setSelected(true);
                    break;
                case VENDORDATA:
                    locVendorDataButton.setSelected(true);
                    break;
                case HOMEBREWDATA:
                    locHomebrewDataButton.setSelected(true);
                    break;
            }
        }
        currDataSet = dataSet;
        return true;
    }


    /**
     * Check for any non standard files being installed and check with the
     * user if there are. Note if the user says no to installing the non
     * standard files they will be removed from the file list
     *
     * @param files the names of the files being installed.
     * @return Should the install process continue
     */
    private boolean checkNonStandardOK(Collection<String> files) {
		final boolean[] returnVal = {true};
        Collection<String> nonStandardFiles = new ArrayList<>();
        for (String filename : files) {
            if (!filename.toLowerCase().startsWith(DATA_FOLDER)
                    && !filename.toLowerCase().startsWith(OUTPUTSHEETS_FOLDER)) {
                nonStandardFiles.add(filename);
            }
        }

        if (!nonStandardFiles.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (String filename : nonStandardFiles) {
                msg.append(' ').append(filename).append("\n");
            }
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			builder.setTitle(R.string.in_diNonStandardFiles);
			builder.setMessage(msg.toString());
			builder.setPositiveButton(R.string.in_ok, (dialogInterface, i) -> {
				for (String filename : nonStandardFiles) {
					files.remove(filename);
				}
			});
			builder.setNegativeButton(R.string.in_cancel, (dialogInterface, i) -> {
				returnVal[0] = false;
			});
			builder.show();
        }

        return returnVal[0];
    }

    /**
     * Check for any files that would be overwritten and confirm it is ok
     * with the user.
     *
     * @param files   the names of the files being installed.
     * @param destDir the destination data directory
     * @return true, if successful
     */
    private boolean checkOverwriteOK(Collection<String> files, File destDir) {
		final boolean[] returnVal = {true};
        Collection<String> existingFiles = new ArrayList<>();
        Collection<String> existingFilesCorr = new ArrayList<>();
        for (String filename : files) {
            String correctedFilename = correctFileName(destDir, filename);
            if (new File(correctedFilename).exists()) {
                existingFiles.add(filename);
                existingFilesCorr.add(correctedFilename);
            }
        }

        if (!existingFiles.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (String filename : existingFilesCorr) {
                msg.append(' ').append(filename).append("\n");
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.in_diOverwriteFiles);
            builder.setMessage(msg.toString());
            builder.setPositiveButton(R.string.in_ok, (dialogInterface, i) -> {
				for (String filename : existingFiles) {
					files.remove(filename);
				}
			});
            builder.setNegativeButton(R.string.in_cancel, (dialogInterface, i) -> {
				returnVal[0] = false;
			});
            builder.show();
        }

        return returnVal[0];
    }

    /**
     * Copy the contents of the input stream to the output stream. Used
     * here to write the zipped file to the install location.
     *
     * @param in  the input stream
     * @param out the output stream
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void copyInputStream(InputStream in, OutputStream out)
            throws IOException {
        byte[] buffer = new byte[1024];
        int len;

        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }

        in.close();
        out.close();
    }

    /**
     * Correct the file name to account for the selected data directory and
     * preference based folder locations such as output sheets.
     *
     * @param destDir  the destination data directory
     * @param fileName the file name to be corrected.
     * @return the corrected file name.
     */
    private String correctFileName(File destDir, String fileName) {
        if (fileName.toLowerCase().startsWith(DATA_FOLDER)) {
            fileName = destDir.getAbsolutePath() + fileName.substring(4);
        } else if (fileName.toLowerCase().startsWith(OUTPUTSHEETS_FOLDER)) {
            fileName =
                    new File(ConfigurationSettings.getOutputSheetsDir())
                            .getAbsolutePath() + fileName.substring(12);
        }
        return fileName;
    }

    /**
     * Creates the directories needed by the installer, where they
     * do not already exist.
     *
     * @param directories the directories
     * @param destDir     the destination data directory
     * @return true, if successful
     */
    private boolean createDirectories(Iterable<String> directories, File destDir) {
        for (String dirname : directories) {
            String corrDirname = correctFileName(destDir, dirname);
            File dir = new File(corrDirname);
            if (!dir.exists()) {
                Timber.i("Creating directory: " + dir);
                if (!dir.mkdirs()) {
                    Toast.makeText(this.getContext(), R.string.in_diDirNotCreated, Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Creates the files in the archive, as filtered by previous user actions.
     *
     * @param dataSet the data set (campaign) to be installed.
     * @param destDir the destination data directory
     * @param files   the list of file names
     * @return true, if all files created ok
     */
    private boolean createFiles(File dataSet, File destDir, Iterable<String> files) {
        String corrFilename = "";
        try (ZipFile in = new ZipFile(dataSet)) {
            for (String filename : files) {
                ZipEntry entry = in.getEntry(filename);
                corrFilename = correctFileName(destDir, filename);
                Timber.d("Extracting file: " + filename + " to " + corrFilename);
                copyInputStream(
                        in.getInputStream(entry),
                        new BufferedOutputStream(new FileOutputStream(corrFilename)));
            }
            return true;
        } catch (IOException e) {
            Timber.e(e, "Failed to read data set " + dataSet + " or write file " + corrFilename + " due to ");
            Toast.makeText(this.getContext(), R.string.in_diWriteFail, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * Populate the lists of files and directories to be installed.
     * Navigate through the zip file, processing each file.
     *
     * @param dataSet     the data set (campaign) to be installed.
     * @param directories the list of directory names
     * @param files       the list of file names
     * @return true, if populate file and dir lists
     */
    @SuppressWarnings("rawtypes")
    private boolean populateFileAndDirLists(File dataSet, Collection<String> directories, Collection<String> files) {
        try (ZipFile in = new ZipFile(dataSet)) {
            Enumeration entries = in.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                if (entry.isDirectory()) {
                    directories.add(entry.getName());
                } else if (!entry.getName().equalsIgnoreCase("install.lst")) {
                    files.add(entry.getName());
                }
            }
        } catch (IOException e) {
            Timber.e(e, "Failed to read data set " + dataSet + " due to ");
            Toast.makeText(getContext(), LanguageBundle.getFormattedString(R.string.in_diBadDataSet, dataSet), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
