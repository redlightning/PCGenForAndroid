/*
 * Copyright 2011 Connor Petty <cpmeister@users.sourceforge.net>
 * Android Port: Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

import net.redlightning.pcgen.MainActivity;
import net.redlightning.pcgen.R;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.fop.apps.MimeConstants;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import pcgen.cdom.base.Constants;
import pcgen.facade.core.CharacterFacade;
import pcgen.system.BatchExporter;
import pcgen.system.ConfigurationSettings;

/**
 * Dialog to allow the preview of character export.
 */
public final class PrintPreviewDialog extends AlertDialog {
    private static final String TAG = PrintPreviewDialog.class.getCanonicalName();
    private final CharacterFacade character;
    private Spinner sheetBox;
    private WebView previewView;
    private ProgressBar progressBar;
    private final MainActivity frame;

    private PrintPreviewDialog(MainActivity frame) {
        super(frame);
        this.frame = frame;
        this.character = frame.getSelectedCharacterRef();
        initComponents(frame);
        new SheetLoader().execute();
    }

    public static void showPrintPreviewDialog(MainActivity frame) {
        AlertDialog dialog = new PrintPreviewDialog(frame);
        dialog.show();
    }

    private void initComponents(Context context) {
        View root = LayoutInflater.from(context).inflate(R.layout.dialog_print_preview, null);
        sheetBox = root.findViewById(R.id.pp_templates);
        sheetBox.setOnItemClickListener((parent, view, position, id) -> new PreviewLoader((URI) sheetBox.getSelectedItem()).execute());
        progressBar = root.findViewById(R.id.pp_progressBar);
        previewView = root.findViewById(R.id.pp_preview);

        setButton(AlertDialog.BUTTON_NEGATIVE, frame.getString(R.string.in_cancel), (dialogInterface, i) -> dismiss());
        setButton(AlertDialog.BUTTON_POSITIVE, frame.getString(R.string.in_mnuFilePrint), (dialogInterface, i) -> {
            PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
            PrintDocumentAdapter printAdapter = previewView.createPrintDocumentAdapter(character.getNameRef().get());

            // Create a print job with name and adapter instance
            String jobName = context.getString(R.string.app_name) + " Document";
            PrintJob printJob = printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());

            // Save the job object for later status checking
            //mPrintJobs.add(printJob);
            dismiss();
        });
    }

    private class PreviewLoader extends AsyncTask<Object, Integer, ByteArrayOutputStream> {
        private final URI uri;

        PreviewLoader(URI uri) {
            this.uri = uri;
            progressBar.setIndeterminate(true);
            sheetBox.setEnabled(false);
        }

        @Override
        protected ByteArrayOutputStream doInBackground(Object... objects) {
            URI osPath = new File(ConfigurationSettings.getOutputSheetsDir()).toURI();
            File xsltFile = new File(osPath.resolve(uri));
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();

            BatchExporter.exportCharacterToPDF(character, outStream, xsltFile);
            return outStream;
        }

        @Override
        protected void onPostExecute(ByteArrayOutputStream outputStream) {
            super.onPostExecute(outputStream);
            progressBar.setIndeterminate(false);

            try {
                String page = new String(outputStream.toByteArray(), "UTF-8");
                previewView.loadData(page, MimeConstants.MIME_PDF, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Log.e(TAG, "Could not load sheet: wrong encoding", ex.getCause());
            }
        }
    }

    private class SheetLoader extends AsyncTask<Object[], Integer, Collection<File>> implements FilenameFilter {

        @Override
        public boolean accept(@NotNull File dir, @NotNull String name) {
            return dir.getName().equalsIgnoreCase("pdf");
        }

        @Override
        protected Collection<File> doInBackground(Object[]... objects) {
            IOFileFilter pdfFilter = FileFilterUtils.asFileFilter(this);
            IOFileFilter suffixFilter = FileFilterUtils.notFileFilter(new SuffixFileFilter(".fo"));
            IOFileFilter sheetFilter = FileFilterUtils.prefixFileFilter(Constants.CHARACTER_TEMPLATE_PREFIX);
            IOFileFilter fileFilter = FileFilterUtils.and(pdfFilter, suffixFilter, sheetFilter);

            IOFileFilter dirFilter = TrueFileFilter.INSTANCE;
            File dir = new File(ConfigurationSettings.getOutputSheetsDir());
            return FileUtils.listFiles(dir, fileFilter, dirFilter);
        }

        @Override
        protected void onPostExecute(Collection<File> files) {
            super.onPostExecute(files);
            List<String> fileList = new ArrayList<>();

            for (File file : files) {
                fileList.add(file.getPath());
            }
            ArrayAdapter<String> fileAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, fileList);
            sheetBox.setAdapter(fileAdapter);
        }
    }
}
