package pcgen.gui2.dialog

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import android.preference.*
import net.redlightning.pcgen.R

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
class PreferencesActivity : AppCompatPreferenceActivity() {

    val PREF_THEME = "lightTheme"
//    bindPreferenceSummaryToValue(findPreference("autoloadSourcesAtStart"))
//    bindPreferenceSummaryToValue(findPreference("autoloadSourcesWithPC"))
//    bindPreferenceSummaryToValue(findPreference("optionAllowedInSources"))
//    bindPreferenceSummaryToValue(findPreference("saveCustomInLst"))
//    bindPreferenceSummaryToValue(findPreference("showLicense"))
//    bindPreferenceSummaryToValue(findPreference("showSponsorsOnLoad"))
//    bindPreferenceSummaryToValue(findPreference("showMatureOnLoad"))
//    bindPreferenceSummaryToValue(findPreference("allowOverrideDuplicates"))
//    bindPreferenceSummaryToValue(findPreference("generation_mode"))
//    bindPreferenceSummaryToValue(findPreference("optionSourcesAllowMultiLine"))
//    bindPreferenceSummaryToValue(findPreference("allStatsValue"))
//    bindPreferenceSummaryToValue(findPreference("hpPercent"))
//    bindPreferenceSummaryToValue(findPreference("maxHpAtFirstLevel"))
//    bindPreferenceSummaryToValue(findPreference("ignoreMonsterHDCap"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
    }

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragmentCompat::class.java.name == fragmentName
                || CharacterPreferenceFragment::class.java.name == fragmentName
                || PCGenPreferenceFragment::class.java.name == fragmentName
    }

    /**
     * This fragment shows character preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    class CharacterPreferenceFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.pref_character, rootKey)
        }
    }

    /**
     * This fragment shows PC Gen preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    class PCGenPreferenceFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.pref_pcgen, rootKey)
        }
    }

    companion object {
        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
            val stringValue = value.toString()

            if (preference is ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                val index = preference.findIndexOfValue(stringValue)

                // Set the summary to reflect the new value.
                preference.setSummary(
                        if (index >= 0)
                            preference.entries[index]
                        else
                            null)
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.summary = stringValue
            }
            true
        }

        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListener
         */
        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }
    }
}
