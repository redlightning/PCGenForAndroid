/*
 * Copyright 2010 Connor Petty <cpmeister@users.sourceforge.net>
 * Android conversion 2018 Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import java.util.ArrayList;
import java.util.List;

import pcgen.facade.core.LanguageChooserFacade;
import pcgen.facade.core.LanguageFacade;

public class LanguageChooserDialog extends AlertDialog {

	private final LanguageChooserFacade chooser;

    public LanguageChooserDialog(Context context, LanguageChooserFacade chooser) {
        super(context);
        setCancelable(true);
		this.chooser = chooser;
        initComponents(context);
    }

    private void initComponents(Context context) {
        setTitle(chooser.getName());

        View root = LayoutInflater.from(context).inflate(R.layout.dialog_language_chooser, null);

        TextView remainingLabel = root.findViewById(R.id.lang_remaining);
        int remainCount = chooser.getRemainingSelections().get();
        remainingLabel.setText(context.getString(R.string.in_selRemain) + " " + remainCount);

        ListView listView = root.findViewById(R.id.lang_selection_list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setItemsCanFocus(false);

        List<LanguageFacade> languages = new ArrayList<>();

        for (LanguageFacade lang : chooser.getAvailableList()) {
            languages.add(lang);
        }

        for (LanguageFacade lang : chooser.getSelectedList()) {
            languages.add(lang);
        }

        //TODO Sort the list of languages alphabetically
        //languages.sort(Comparator.naturalOrder());

        ArrayAdapter<LanguageFacade> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_multiple_choice, languages);
        listView.setAdapter(adapter);

        for (LanguageFacade lang : chooser.getSelectedList()) {
            listView.setItemChecked(adapter.getPosition(lang), true);
        }

        listView.setOnItemClickListener((adapterView, view1, position, id) -> {
            if (listView.isItemChecked(position)) {
                chooser.addSelected((LanguageFacade) listView.getItemAtPosition(position));
            } else {
                chooser.removeSelected((LanguageFacade) listView.getItemAtPosition(position));
            }
            int count = chooser.getRemainingSelections().get();
            remainingLabel.setText(context.getString(R.string.in_selRemain) + " " + count);
        });

        setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_ok), (dialogInterface, i) -> {
            chooser.rollback();
            dismiss();
        });

        setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.in_cancel), (dialogInterface, i) -> {
            chooser.commit();
            dismiss();
        });
	}
}
