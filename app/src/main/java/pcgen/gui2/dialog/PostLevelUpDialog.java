/*
 * Copyright James Dempsey, 2012
 * Android conversion 2018 Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import java.util.Arrays;
import java.util.stream.IntStream;

import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.CharacterLevelFacade;
import pcgen.facade.core.CharacterLevelsFacade;
import pcgen.facade.core.ClassFacade;

/**
 * The Class {@code PostLevelUpDialog} provides a display of the results
 * of levelling up a character. 
 */
public final class PostLevelUpDialog extends AlertDialog {
	private final CharacterLevelsFacade levels;
	private final int oldLevel;
	private final int numLevels;

    private PostLevelUpDialog(Context context, CharacterFacade character, int oldLevel) {
        super(context);
        setCancelable(true);
		this.oldLevel = oldLevel;
		this.levels = character.getCharacterLevelsFacade();
		numLevels = character.getCharacterLevelsFacade().getSize() - oldLevel;
        initComponents(context);
	}

	/**
	 * Display the post levelling dialog for a character. This will display a 
	 * list of levels just added along with the hit points and skill points 
	 * gained. The hit points gained may be edited.
     *
     * @param context The component we should appear above.
	 * @param character The character that has been levelled up.
	 * @param oldLevel The character's level before the level up action.
	 */
    public static void showPostLevelUpDialog(Context context, CharacterFacade character, int oldLevel) {
		int size = character.getCharacterLevelsFacade().getSize();
        if (size - oldLevel + 1 < 1) {
			return;
		}

        PostLevelUpDialog dialog = new PostLevelUpDialog(context, character, oldLevel);
        dialog.show();
    }

    private void initComponents(Context context) {
        TableLayout table = (TableLayout) LayoutInflater.from(context).inflate(R.layout.dialog_post_level_up, null);

        for (int i = oldLevel; i < (numLevels + oldLevel); i++) {
            TableRow levelRow = new TableRow(context);
            TextView levelView = new TextView(context);
            TextView classView = new TextView(context);
            TextView gainedView = new TextView(context);
            TextView rolledView = new TextView(context);
            TextView pointView = new TextView(context);

            CharacterLevelFacade level = levels.getElementAt(i);
            ClassFacade classFacade = levels.getClassTaken(level);

            levelView.setText(String.valueOf(i + 1));
            classView.setText(classFacade.toString());
            gainedView.setText(String.valueOf(levels.getHPGained(level)));
            rolledView.setText(String.valueOf(levels.getHPRolled(level)));
            pointView.setText(String.valueOf(levels.getGainedSkillPoints(level)));

            levelRow.addView(levelView);
            levelRow.addView(classView);
            if (i == oldLevel) {
                String hd = levels.getClassTaken(levels.getElementAt(oldLevel)).getHD();
                int max = Integer.parseInt(hd);
                int[] range = IntStream.rangeClosed(1, max).toArray();
                Integer[] hdRange = Arrays.stream(range).boxed().toArray(Integer[]::new);

                Spinner rolledHP = new Spinner(context);
                ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, hdRange);

                rolledHP.setAdapter(spinnerArrayAdapter);
                rolledHP.setSelection(spinnerArrayAdapter.getPosition(levels.getHPRolled(levels.getElementAt(oldLevel))));
                rolledHP.setOnItemClickListener((adapterView, view, position, id) -> {
                    CharacterLevelFacade levelFacade = levels.getElementAt(oldLevel);
                    levels.setHPRolled(levelFacade, spinnerArrayAdapter.getItem(position));
                    gainedView.setText(String.valueOf(levels.getHPGained(levelFacade)));
                });
                levelRow.addView(rolledHP);
            } else {
                levelRow.addView(rolledView);
            }
            levelRow.addView(gainedView);
            levelRow.addView(pointView);

            table.addView(levelRow);
        }
    }
}
