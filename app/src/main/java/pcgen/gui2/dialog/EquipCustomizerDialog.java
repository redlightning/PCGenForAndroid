/*
 * Copyright Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;

import net.redlightning.pcgen.R;

import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.EquipmentBuilderFacade;
import pcgen.gui2.equip.EquipCustomPanel;

/**
 * The Class {@code EquipCustomizerDialog} provides a pop-up dialog that allows
 * the user to build up custom equipment items by adding equipment modifiers and
 * setting the name, cost etc.  
 */
public class EquipCustomizerDialog extends AlertDialog {
	private boolean purchase;
	private boolean cancelled;

	/**
	 * Create a new instance of KitSelectionDialog
     * @param context The parent frame we are displaying over.
	 * @param character The character being displayed.
	 */
    public EquipCustomizerDialog(Context context, CharacterFacade character, EquipmentBuilderFacade builder) {
        super(context);
        setCancelable(true);
        setTitle(R.string.in_itemCustomizer);
        setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_cancel), (dialogInterface, i) -> {
            cancelled = true;
            dismiss();
        });
        setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_buy), (dialogInterface, i) -> {
            purchase = true;
            dismiss();
        });
        setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.in_ok), (dialogInterface, i) -> dismiss());

        EquipCustomPanel equipCustomPanel = new EquipCustomPanel(context, character, builder);
        setView(equipCustomPanel);
	}

	/**
	 * @return
	 */
	public boolean isPurchase()
	{
		return purchase;
	}

	public boolean isCancelled()
	{
		return cancelled;
	}

}
