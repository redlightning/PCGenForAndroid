/*
 * Copyright Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.Toast;

import net.redlightning.pcgen.R;

import java.util.ArrayList;
import java.util.List;

import pcgen.facade.core.ChooserFacade;
import pcgen.facade.core.InfoFacade;
import pcgen.facade.util.ListFacade;
import pcgen.gui2.util.MultiSpinner;
import timber.log.Timber;

/**
 * Dialog to make a single options selection from a list
 */
public class RadioChooserDialog extends AlertDialog implements MultiSpinner.MultiSpinnerListener {
	private final ChooserFacade chooser;
	private boolean committed;


    public RadioChooserDialog(Context context, ChooserFacade chooser) {
		super(context);
		this.chooser = chooser;
		setCancelable(true);
		setTitle(chooser.getName());

		List<String> options = new ArrayList<>();
		ListFacade<InfoFacade> availableList = chooser.getAvailableList();
		for (InfoFacade infoFacade : availableList) {
			options.add(infoFacade.toString());
		}

		MultiSpinner spinner = new MultiSpinner(context);
		spinner.setItems(options, context.getString(R.string.in_ieDefault), this);
		setView(spinner);

		setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_ok), (dialogInterface, i) -> {
			for (int x = 0; x < chooser.getAvailableList().getSize(); x++) {
				if (spinner.getSelectedItem().equals(chooser.getAvailableList().getElementAt(i).toString())) {
					chooser.addSelected(chooser.getAvailableList().getElementAt(i));
					break;
				}
			}

			if (chooser.isRequireCompleteSelection() && chooser.getRemainingSelections().get() > 0) {
				Toast.makeText(context, R.string.in_chooserRequireComplete, Toast.LENGTH_LONG).show();
			} else {
				chooser.commit();
				committed = true;
				dismiss();
			}
		});

		setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_cancel), (dialogInterface, i) -> {
			chooser.rollback();
			dismiss();
		});
	}

	/**
	 * Returns the means by which the dialog was closed.   
	 * @return the committed status, false for cancelled, true for OKed. 
	 */
	public boolean isCommitted()
	{
		return committed;
	}

	@Override
	public void onItemsSelected(boolean[] selected) {
		Timber.i("No action taken for RadioChooserDialog selection");
		//TODO Does any action need to be taken for RadioChooserDialog selection?
	}
}
