/*
 * Copyright 2008 (C) James Dempsey
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import net.redlightning.pcgen.R;


/**
 * The Class {@code DIWarningDialog} is responsible for
 * displaying warnings for the data installer. The list of 
 * files will be displayed in a scrollable area.
 */
public class DIWarningDialog extends AlertDialog {
	private final String fileText;
	private final String introText;
	
	/** The result selected by the user. */
    private int result = AlertDialog.BUTTON_NEUTRAL;

	/**
	 * Instantiates a new warning dialog for the data installer.
     *
     * @param context the parent frame
	 * @param fileList the file list as a text string, one file per line
	 * @param introText the intro text to explain the dialogs purpose to the user.
	 */
    public DIWarningDialog(Context context, String fileList, String introText) {
        super(context);
        setTitle(R.string.in_dataInstaller);
        setCancelable(false);

		fileText = fileList;
		this.introText = introText;

        initComponents(context);
	}
	
	/**
	 * Gets the response.
	 * 
	 * @return the response
	 */
	public int getResponse()
	{
		return result;
	}

	/**
	 * Initialises the user interface.
	 */
    private void initComponents(Context context) {
        View layout = LayoutInflater.from(context).inflate(R.layout.dialog_di_warning, null);
        TextView intro = layout.findViewById(R.id.introText);
        intro.setText(introText);

        EditText messageArea = layout.findViewById(R.id.messageText);
        messageArea.setEnabled(false);
		messageArea.setText(fileText);
        setView(layout);

        setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_yes), (dialogInterface, i) -> {
            result = AlertDialog.BUTTON_POSITIVE;
            dismiss();
        });

        setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.in_cancel), (dialogInterface, i) -> {
            result = AlertDialog.BUTTON_NEUTRAL;
            dismiss();
        });

        setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_no), (dialogInterface, i) -> {
            result = AlertDialog.BUTTON_NEGATIVE;
            dismiss();
        });
	}
}
