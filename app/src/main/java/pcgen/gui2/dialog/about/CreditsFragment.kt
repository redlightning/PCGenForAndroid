package pcgen.gui2.dialog.about

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView

import net.redlightning.pcgen.R

class CreditsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.fragment_about_credits, container, false)
        val coderList = layout.findViewById<ListView>(R.id.about_credits_monkey_list)
        var coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.code_monkeys, android.R.layout.simple_spinner_item)
        coderList.adapter = coderAdapter

        val codeButton = layout.findViewById<Button>(R.id.credits_button_code)
        val listButton = layout.findViewById<Button>(R.id.credits_button_list)
        val testButton = layout.findViewById<Button>(R.id.credits_button_test)
        val engButton = layout.findViewById<Button>(R.id.credits_button_eng)

        codeButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.code_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        listButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.list_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        testButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.test_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        engButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.engineering_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        return layout
    }

    fun getLayout(context: Context, inflater: LayoutInflater): View? {
        val layout = inflater.inflate(R.layout.fragment_about_credits, null,false)
        val coderList = layout.findViewById<ListView>(R.id.about_credits_monkey_list)
        var coderAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.code_monkeys, android.R.layout.simple_spinner_item)
        coderList.adapter = coderAdapter

        val codeButton = layout.findViewById<Button>(R.id.credits_button_code)
        val listButton = layout.findViewById<Button>(R.id.credits_button_list)
        val testButton = layout.findViewById<Button>(R.id.credits_button_test)
        val engButton = layout.findViewById<Button>(R.id.credits_button_eng)

        codeButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(context, R.array.code_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        listButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(context, R.array.list_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        testButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(context, R.array.test_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        engButton.setOnClickListener {
            coderAdapter = ArrayAdapter.createFromResource(context, R.array.engineering_monkeys, android.R.layout.simple_spinner_item)
            coderList.adapter = coderAdapter
            coderAdapter.notifyDataSetChanged()
        }
        return layout
    }
}
