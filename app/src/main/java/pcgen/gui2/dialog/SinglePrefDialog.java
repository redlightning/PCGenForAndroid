/*
 * Copyright James Dempsey, 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;

import net.redlightning.pcgen.R;

import pcgen.gui2.prefs.PCGenPrefsPanel;

/**
 * The Class {@code SinglePrefDialog} displays a single
 * preference panel to the user.
 */
public class SinglePrefDialog extends AlertDialog {

	/**
	 * Create a new modal SinglePrefDialog to display a particular panel.
	 *  
	 * @param context The parent context
	 * @param prefsPanel The panel to be displayed.
	 */
	public SinglePrefDialog(Context context, PCGenPrefsPanel prefsPanel)
	{
		super(context);

		setTitle(prefsPanel.getTitle());
		setView(prefsPanel);

		this.setButton(BUTTON_POSITIVE, getContext().getString(R.string.in_ok), (dialogInterface, i) -> {
			prefsPanel.setOptionsBasedOnControls();
			dismiss();
		});
		this.setButton(BUTTON_NEGATIVE, getContext().getString(R.string.in_cancel), (dialogInterface, i) -> dismiss());
		prefsPanel.applyOptionValuesToControls();
	}
}
