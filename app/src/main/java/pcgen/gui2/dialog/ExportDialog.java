/*
 * Copyright 2011 Connor Petty <cpmeister@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.gui2.dialog;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

import pcgen.cdom.base.Constants;
import pcgen.core.Globals;
import pcgen.core.SettingsHandler;
import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.PartyFacade;
import pcgen.gui2.UIPropertyContext;
import pcgen.gui2.util.FacadeComboBoxModel;
import pcgen.io.ExportUtilities;
import pcgen.system.BatchExporter;
import pcgen.system.CharacterManager;
import pcgen.system.ConfigurationSettings;
import pcgen.system.PCGenSettings;
import timber.log.Timber;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.obsez.android.lib.filechooser.ChooserDialog;

import net.redlightning.pcgen.MainActivity;
import net.redlightning.pcgen.R;

/**
 * The dialog provides the list of output sheets for a character or party to be exported to.
 * TODO Export really needs to be tested, as this was a messy conversion
 */
public final class ExportDialog extends AlertDialog {
    private static final String PDF_EXPORT_DIR_PROP = "pdfExportDir";
    private static final String HTML_EXPORT_DIR_PROP = "htmlExportDir";
    private static final String PARTY_COMMAND = "PARTY";
    private static final String EXPORT_TO_COMMAND = "EXPORT_TO";
    private static final String EXPORT_COMMAND = "EXPORT";
    private static final String CLOSE_COMMAND = "CLOSE";

    public static void showExportDialog(MainActivity parent) {
        ExportDialog dialog = new ExportDialog(parent);
        dialog.show();
    }

    private final MainActivity pcgenFrame;
    private final FacadeComboBoxModel<CharacterFacade> characterBoxModel;
    private final Spinner characterBox;
    private final SwitchMaterial partyBox;
    private final Spinner exportBox;
    private final Spinner fileList;
    private final FileSearcher fileSearcher;
    private Collection<File> allTemplates = null;

    private ExportDialog(MainActivity parent) {
        super(parent);
        this.pcgenFrame = parent;
        this.characterBoxModel = new FacadeComboBoxModel(CharacterManager.getCharacters(), parent.currentCharacterRef);
        LayoutInflater inflater = LayoutInflater.from(parent);
        View layout = inflater.inflate(R.layout.dialog_export, null);
        partyBox = layout.findViewById(R.id.entire_party);
        characterBox = layout.findViewById(R.id.character_select);
        exportBox = layout.findViewById(R.id.export_to);
        fileList = layout.findViewById(R.id.templates);

		setTitle("Export a PC or Party");
        setButton(Dialog.BUTTON_POSITIVE, "Export", (dialogInterface, i) -> doExport());
        setButton(Dialog.BUTTON_NEGATIVE, "Close", (dialogInterface, i) -> dismiss());

        fileSearcher = new FileSearcher();
        fileSearcher.execute();
    }

    @Override
    public void dismiss() {
        fileSearcher.cancel(false);
        characterBoxModel.setReference(null);
        characterBoxModel.setListFacade(null);
        super.dismiss();
    }

//    private void initComponents() {
//        characterBox.setRenderer(new DefaultListCellRenderer() {
//            @Override
//            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
//                CharacterFacade character = (CharacterFacade) value;
//                return super.getListCellRendererComponent(list, character.getNameRef().get(), index, isSelected, cellHasFocus);
//            }
//        });
//
//        UIPropertyContext context = UIPropertyContext.createContext("ExportDialog");
//		for (SheetFilter filter : SheetFilter.values()) {
//			if (defaultOSType.equals(filter.toString())) {
//				exportBox.setSelectedItem(filter);
//			}
//		}
//    }
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        if (PARTY_COMMAND.equals(e.getActionCommand())) {
//            characterBox.setEnabled(!partyBox.isSelected());
//            refreshFiles();
//        } else if (EXPORT_TO_COMMAND.equals(e.getActionCommand())) {
//            UIPropertyContext context = UIPropertyContext.createContext("ExportDialog");
//            context.setProperty(UIPropertyContext.DEFAULT_OS_TYPE, exportBox.getSelectedItem().toString());
//            refreshFiles();
//        }
//    }

    private void doExport() {
        export(SheetFilter.PDF == exportBox.getSelectedItem());
    }

    private void setWorking(boolean working) {
        fileList.setEnabled(!working);
        exportBox.setEnabled(!working);
    }

    private void export(boolean pdf) {
        UIPropertyContext context = UIPropertyContext.createContext("ExportDialog");
        File baseDir = null;
        {
            String path;
            if (pdf) {
                path = context.getProperty(PDF_EXPORT_DIR_PROP);
            } else {
                path = context.getProperty(HTML_EXPORT_DIR_PROP);
            }
            if (path != null) {
                baseDir = new File(path);
            }
        }
        if (baseDir == null || !baseDir.isDirectory()) {
            baseDir = SystemUtils.getUserHome();
        }
        URI uri = (URI) fileList.getSelectedItem();
        AtomicReference<File> outFile = new AtomicReference<>();
        ChooserDialog fcExport = new ChooserDialog(pcgenFrame)
                .withFilter(false, false)
                .withStartFile(baseDir.getAbsolutePath())
                // to handle the result(s)
                .withChosenListener((path, pathFile) -> {
                    if (pdf) {
                        context.setProperty(PDF_EXPORT_DIR_PROP, path);
                    } else {
                        context.setProperty(HTML_EXPORT_DIR_PROP, path);
                    }
                    outFile.set(pathFile);
                })
                .withFilter(true, ExportUtilities.getOutputExtension(uri.toString(), pdf))
                .build()
                .show();


        String name;
        File path;
        if (!partyBox.isSelected()) {
            CharacterFacade character = (CharacterFacade) characterBox.getSelectedItem();
            path = character.getFileRef().get();
            if (path != null) {
                path = path.getParentFile();
            } else {
                path = new File(PCGenSettings.getPcgDir());
            }
            name = character.getTabNameRef().get();
            if (StringUtils.isEmpty(name)) {
                name = character.getNameRef().get();
            }
        } else {
            path = new File(PCGenSettings.getPcgDir());
            name = "Entire Party";
        }
//        if (pdf) {
//            fcExport.setSelectedFile(new File(path, name + ".pdf"));
//        } else {
//            fcExport.setSelectedFile(new File(path, name + "." + exportBox.getSelectedItem()));
//        }
//        fcExport.setDialogTitle("Export " + name);
//        if (fcExport.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
//            return;
//        }
//
//        final File outFile = fcExport.getSelectedFile();
//        if (pdf) {
//            context.setProperty(PDF_EXPORT_DIR_PROP, outFile.getParent());
//        } else {
//            context.setProperty(HTML_EXPORT_DIR_PROP, outFile.getParent());
//        }

        if (StringUtils.isEmpty(outFile.get().getName())) {
            pcgenFrame.showErrorMessage("PCGen", "You must set a filename.");
            return;
        }

        if (outFile.get().isDirectory()) {
            pcgenFrame.showErrorMessage("PCGen", "You cannot overwrite a directory with a file.");
            return;
        }

//        if (outFile.exists() && !SettingsHandler.getAlwaysOverwrite()) {
//            int reallyClose = JOptionPane.showConfirmDialog(this,
//                    "The file " + outFile.getName()
//                            + " already exists, are you sure you want to overwrite it?",
//                    "Confirm overwriting "
//                            + outFile.getName(), JOptionPane.YES_NO_OPTION);
//
//            if (reallyClose != JOptionPane.YES_OPTION) {
//                return;
//            }
//        }
        try {
            if (pdf) {
                new PDFExporter(outFile.get(), exportBox.getSelectedItem().toString(), name).execute();
            } else {
                if (!printToFile(outFile.get())) {
                    String message = "The character export failed. Please see the log for details.";
                    pcgenFrame.showErrorMessage(Constants.APPLICATION_NAME, message);
                    return;
                }
                //maybeOpenFile(pcgenFrame, outFile);
                Globals.executePostExportCommandStandard(outFile.get().getAbsolutePath());
            }
        } catch (IOException ex) {
            pcgenFrame.showErrorMessage("PCGen", "Could not export " + name
                    + ". Try another filename.");
            Timber.e(ex, "Could not export %s", name);
        }
    }

    private void maybeOpenFile(Context context, File file) {
//        String value = context.getProperty(UIPropertyContext.ALWAYS_OPEN_EXPORT_FILE);
//        Boolean openFile = StringUtils.isEmpty(value) ? null : Boolean.valueOf(value);
//        if (openFile == null) {
//            CheckBox checkbox = new CheckBox(context);
//            checkbox.setText(R.string.in_always_perform);
//
//            View message = PCGenFrame.buildMessageLabelPanel(
//                    "Do you want to open " + file.getName() + "?",
//                    checkbox);
//            int ret = JOptionPane.showConfirmDialog(this, message, "Select an Option", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//            if (ret == JOptionPane.CLOSED_OPTION) {
//                return;
//            }
//            openFile = BooleanUtils.toBoolean(ret, JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
//            if (checkbox.isSelected()) {
//                context.setBoolean(UIPropertyContext.ALWAYS_OPEN_EXPORT_FILE, openFile);
//            }
//        }
//        if (!openFile) {
//            return;
//        }
//
//        try {
//            Desktop.getDesktop().open(file);
//        } catch (IOException ex) {
//            String message = "Failed to open " + file.getName();
//            pcgenFrame.showErrorMessage(Constants.APPLICATION_NAME, message);
//            Timber.e(ex, message);
//        }
    }

    private File getSelectedTemplate() {
        File osDir;
        String outputSheetDirectory = SettingsHandler.getGame().getOutputSheetDirectory();
        if (outputSheetDirectory == null) {
            osDir = new File(ConfigurationSettings.getOutputSheetsDir());
            outputSheetDirectory = "";
        } else {
            osDir = new File(ConfigurationSettings.getOutputSheetsDir(), outputSheetDirectory);
        }
        URI osPath = new File(osDir, ((SheetFilter) exportBox.getSelectedItem()).getPath()).toURI();
        URI uri = (URI) fileList.getSelectedItem();
        return new File(osPath.resolve(uri));
    }

    private boolean printToFile(File outFile)
            throws IOException {
        File template = getSelectedTemplate();

        if (partyBox.isSelected()) {
            SettingsHandler.setSelectedPartyHTMLOutputSheet(template.getAbsolutePath());
            PartyFacade party = CharacterManager.getCharacters();
            return BatchExporter.exportPartyToNonPDF(party, outFile, template);
        } else {
            CharacterFacade character = (CharacterFacade) characterBox.getSelectedItem();
            return BatchExporter.exportCharacterToNonPDF(character, outFile, template);
        }
    }

    private void refreshFiles() {
        if (allTemplates != null) {
            String outputSheetsDir;
            SheetFilter sheetFilter = (SheetFilter) exportBox.getSelectedItem();
            IOFileFilter ioFilter = FileFilterUtils.asFileFilter(sheetFilter);
            IOFileFilter prefixFilter;
            String defaultSheet = null;
            String outputSheetDirectory = SettingsHandler.getGame().getOutputSheetDirectory();
            if (outputSheetDirectory == null) {
                outputSheetsDir = ConfigurationSettings.getOutputSheetsDir() + "/" +
                        sheetFilter.getPath();
            } else {
                outputSheetsDir = ConfigurationSettings.getOutputSheetsDir() + "/" +
                        outputSheetDirectory + "/" + sheetFilter.getPath();
            }

            if (partyBox.isSelected()) {
                prefixFilter = FileFilterUtils.prefixFileFilter(Constants.PARTY_TEMPLATE_PREFIX);
            } else {
                CharacterFacade character = (CharacterFacade) characterBox.getSelectedItem();
                prefixFilter = FileFilterUtils.prefixFileFilter(Constants.CHARACTER_TEMPLATE_PREFIX);
                defaultSheet = character.getDefaultOutputSheet(sheetFilter == SheetFilter.PDF);
                if (StringUtils.isEmpty(defaultSheet)) {
                    defaultSheet = outputSheetsDir + "/" +
                            SettingsHandler.getGame().getOutputSheetDefault(sheetFilter.getTag());
                }
            }
            IOFileFilter filter = FileFilterUtils.and(prefixFilter, ioFilter);
            List<File> files = FileFilterUtils.filterList(filter, allTemplates);
            Collections.sort(files);

            URI osPath = new File(outputSheetsDir).toURI();
            URI[] uriList = new URI[files.size()];
            for (int i = 0; i < uriList.length; i++) {
                uriList[i] = files.get(i).toURI();
            }
            ArrayAdapter<URI> adapter = new ArrayAdapter<>(pcgenFrame, android.R.layout.simple_spinner_item, uriList);
            fileList.setAdapter(adapter);

            if (StringUtils.isNotEmpty(defaultSheet)) {
                URI defaultPath = new File(defaultSheet).toURI();
                fileList.setSelection(Arrays.asList(uriList).indexOf(defaultPath));
            }
        }
    }

    private class PDFExporter extends AsyncTask<Object, Object, Object> {
        private final File saveFile;
        private final String name;

        public PDFExporter(File saveFile, String extension, String name) {
            this.saveFile = saveFile;
            this.name = name;
            setWorking(true);
        }

        @Override
        protected Object doInBackground(Object... objects) {
            boolean result;
            if (partyBox.isSelected()) {
                PartyFacade party = CharacterManager.getCharacters();
                result =
                        BatchExporter.exportPartyToPDF(party, saveFile,
                                getSelectedTemplate());
            } else {
                CharacterFacade character =
                        (CharacterFacade) characterBox.getSelectedItem();
                result =
                        BatchExporter.exportCharacterToPDF(character, saveFile,
                                getSelectedTemplate());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Object result) {
            boolean exception = true;
            try {
                if (!((Boolean) get())) {
                    pcgenFrame.showErrorMessage("Could not export " + name, "Error occurred while exporting. See log for details.");
                } else {
                    exception = false;
                }
            } catch (InterruptedException ex) {
                // Take no action as we are probably being asked to shut down.
            } catch (ExecutionException ex) {
                Timber.e(ex, "Could not export %s", name);
                pcgenFrame.showErrorMessage("Could not export " + name, "Error occurred while exporting. See log for details.");
            } finally {
                if (!exception) {
                    Globals.executePostExportCommandPDF(saveFile.getAbsolutePath());
                }
                setWorking(false);
//                if (!exception) {
//                    maybeOpenFile(pcgenFrame, saveFile);
//                }
            }
        }
    }

    private class FileSearcher extends AsyncTask<Collection<File>, Object, Collection<File>> {

        @Override
        protected Collection<File> doInBackground(Collection<File>... object) {
            File dir;
            String outputSheetDirectory = SettingsHandler.getGame().getOutputSheetDirectory();
            if (outputSheetDirectory == null) {
                Timber.e("OUTPUTSHEET|DIRECTORY not defined for game mode %s", SettingsHandler.getGame());
                dir = new File(ConfigurationSettings.getOutputSheetsDir());
                outputSheetDirectory = "";
            } else {
                dir = new File(ConfigurationSettings.getOutputSheetsDir(), outputSheetDirectory);
                if (!dir.isDirectory()) {
                    try {
                        Timber.e("Unable to find game mode outputsheets at " + dir.getCanonicalFile() + ". Trying base.");

                        dir = new File(ConfigurationSettings.getOutputSheetsDir());
                        outputSheetDirectory = "";
                    } catch (IOException e) {
                        Timber.e(e);
                    }
                }
            }
            if (!dir.isDirectory()) {
                try {
                    Timber.e("Unable to find outputsheets folder at " + dir.getCanonicalFile() + ".");
                } catch (IOException e) {
                    Timber.e(e);
                }
                return Collections.emptyList();
            }
            IOFileFilter fileFilter = FileFilterUtils.notFileFilter(new SuffixFileFilter(".fo"));
            return FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
        }

        @Override
        protected void onPostExecute(Collection<File> result) {
            if (isCancelled()) {
                return;
            }
            try {
                allTemplates = get();
                refreshFiles();
            } catch (InterruptedException | ExecutionException ex) {
                Timber.e(ex, "failed to search files");
            }
        }
    }

    private enum SheetFilter implements FilenameFilter {
        HTMLXML("htmlxml", "Standard", "HTM"),
        PDF("pdf", "PDF", "PDF"),
        TEXT("text", "Text", "TXT");
        private final String dirFilter;
        private final String description;
        private final String tag;

        private SheetFilter(String dirFilter, String description, String tag) {
            this.dirFilter = dirFilter;
            this.description = description;
            this.tag = tag;
        }

        public String getPath() {
            return dirFilter;
        }

        @Override
        public String toString() {
            return description;
        }

        public String getTag() {
            return tag;
        }

        @Override
        public boolean accept(File dir, String name) {
            return dir.getName().equalsIgnoreCase(dirFilter)
                    && !name.endsWith("~");
        }

    }

}
