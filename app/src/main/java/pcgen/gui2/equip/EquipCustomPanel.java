/*
 * Copyright Michael Isaacson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.equip;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.redlightning.pcgen.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kotlin.collections.ArrayDeque;
import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.EquipModFacade;
import pcgen.facade.core.EquipmentBuilderFacade;
import pcgen.facade.core.EquipmentBuilderFacade.EquipmentHead;
import pcgen.facade.core.EquipmentFacade;
import pcgen.facade.core.SizeAdjustmentFacade;
import pcgen.facade.util.DefaultListFacade;
import pcgen.facade.util.DefaultReferenceFacade;
import pcgen.facade.util.ListFacade;
import pcgen.facade.util.event.ListEvent;
import pcgen.facade.util.event.ListListener;
import pcgen.facade.util.event.ReferenceEvent;
import pcgen.gui2.filter.Filter;
import pcgen.gui2.filter.FilteredListFacade;
import pcgen.gui2.filter.FilteredTreeViewTable;
import pcgen.gui2.tabs.models.CharacterComboBoxModel;
import pcgen.gui2.tools.InfoPane;
import pcgen.gui2.util.TreeColumnCellRenderer;
import pcgen.gui2.util.treeview.DataView;
import pcgen.gui2.util.treeview.DataViewColumn;
import pcgen.gui2.util.treeview.DefaultDataViewColumn;
import pcgen.gui2.util.treeview.TreeView;
import pcgen.gui2.util.treeview.TreeViewModel;
import pcgen.gui2.util.treeview.TreeViewPath;
import pcgen.system.LanguageBundle;

/**
 * The Class {@code EquipCustomPanel} displays an available/selected table
 * pair to allow the creation of a custom piece of equipment..
 */
public class EquipCustomPanel extends View {

    private final FilteredTreeViewTable<Object, EquipModFacade> availableTable;
    private final FilteredTreeViewTable<Object, EquipModFacade> selectedTable;
    private final Button nameButton;
    private final Button spropButton;
    private final Button costButton;
    private final Button weightButton;
    private final Button damageButton;
    private final Spinner headCombo;
    private final Spinner sizeCombo;
    private final Button addButton;
    private final Button removeButton;
    private final WebView equipModInfoPane;
    private final WebView equipInfoPane;
    private final CharacterFacade character;
    private final TreeColumnCellRenderer renderer;

    private final EquipmentBuilderFacade builder;
    private EquipInfoHandler equipInfoHandler;

    private final ListFacade<EquipmentHead> validHeads;
    private HeadBoxModel headBoxModel;
    private SizeBoxModel sizeBoxModel;
    private EquipmentHead currentHead = EquipmentHead.PRIMARY;

    private LinearLayoutManager availableLayoutManager;
	private LinearLayoutManager selectedLayoutManager;
	private RecyclerView availableRecycler;
	private RecyclerView selectedRecycler;
	private RecyclerView.Adapter<EquipModViewHolder> avialableAdapter;
	private RecyclerView.Adapter<EquipModViewHolder> selectedAdapter;

	private Map<EquipmentHead, EquipModTreeViewModel> availEqmodModelMap;
    private Map<EquipmentHead, EquipModTreeViewModel> selectedEqmodModelMap;

    /**
     * Create a new instance of EquipCustomPanel for a character.
     *
     * @param character The character being displayed.
     * @param builder   The equipment builder to be used for creating the item.
     */
    public EquipCustomPanel(Context context, CharacterFacade character, EquipmentBuilderFacade builder) {
        super(context);
        this.character = character;
        this.builder = builder;
        validHeads = new DefaultListFacade<>(builder.getEquipmentHeads());
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.layout_customize_equipment, null);

        availableTable = new FilteredTreeViewTable<>();
        selectedTable = new FilteredTreeViewTable<>();

        nameButton = layout.findViewById(R.id.ec_name_button);
        nameButton.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.in_eqCust_NewName);
            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(builder.getEquipment().toString());
            dialogBuilder.setView(input);
            dialogBuilder.setPositiveButton("OK", (dialog, which) -> {
                builder.setName(input.getText().toString());
                equipInfoHandler.refreshInfo();
            });
            dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            dialogBuilder.show();
        });

        spropButton = layout.findViewById(R.id.ec_sprop_button);
        spropButton.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.in_eqCust_NewSProp);
            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(builder
                    .getEquipment().getRawSpecialProperties());
            dialogBuilder.setView(input);
            dialogBuilder.setPositiveButton("OK", (dialog, which) -> {
                builder.setSProp(input.getText().toString());
                equipInfoHandler.refreshInfo();
            });
            dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            dialogBuilder.show();
        });

        costButton = layout.findViewById(R.id.ec_cost_button);
        costButton.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.in_eqCust_NewCost);
            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setText(String.valueOf(character.getInfoFactory().getCost(builder.getEquipment())));
            dialogBuilder.setView(input);
            dialogBuilder.setPositiveButton("OK", (dialog, which) -> {
                builder.setCost(input.getText().toString());
                equipInfoHandler.refreshInfo();
            });
            dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            dialogBuilder.show();
        });

        weightButton = layout.findViewById(R.id.ec_weight_button);
        weightButton.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.in_eqCust_NewWeight);
            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setText(String.valueOf(character.getInfoFactory().getWeight(builder.getEquipment())));
            dialogBuilder.setView(input);
            dialogBuilder.setPositiveButton("OK", (dialog, which) -> {
                builder.setWeight(input.getText().toString());
                equipInfoHandler.refreshInfo();
            });
            dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            dialogBuilder.show();
        });

        damageButton = layout.findViewById(R.id.ec_damage_button);
        damageButton.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.in_eqCust_NewDamage);
            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(String.valueOf(builder.getDamage()));
            dialogBuilder.setView(input);
            dialogBuilder.setPositiveButton("OK", (dialog, which) -> {
                builder.setDamage(input.getText().toString());
                equipInfoHandler.refreshInfo();
            });
            dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            dialogBuilder.show();
        });

        addButton = layout.findViewById(R.id.ec_add_modifier);
        addButton.setOnClickListener(view -> {
            List<Object> data = availableTable.getSelectedData();
            for (Object eqMod : data) {
                if (eqMod instanceof EquipModFacade) {
                    builder.addModToEquipment((EquipModFacade) eqMod, currentHead);
                }
            }
            equipInfoHandler.refreshInfo();
            availableTable.refilter();
        });

        this.removeButton = layout.findViewById(R.id.ec_remove_modifier);
        removeButton.setOnClickListener(view -> {
            List<Object> data = selectedTable.getSelectedData();
            for (Object eqMod : data) {
                if (eqMod instanceof EquipModFacade) {
                    builder.removeModFromEquipment((EquipModFacade) eqMod,
                            currentHead);
                }
            }
            equipInfoHandler.refreshInfo();
            availableTable.refilter();
        });

        headCombo = layout.findViewById(R.id.ec_head_spinner);

        sizeCombo = layout.findViewById(R.id.ec_size_spinner);
        ListFacade<SizeAdjustmentFacade> sizesFacade = character.getDataSet().getSizes();
        List<SizeAdjustmentFacade> sizes = new ArrayList<>();

        for(SizeAdjustmentFacade saf : sizesFacade) {
            sizes.add(saf);
        }
        ArrayAdapter<SizeAdjustmentFacade> sizesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, sizes);
        sizesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeCombo.setOnItemClickListener((adapterView, view, i, l) -> builder.setSize(sizesAdapter.getItem(i)));
        sizeCombo.setAdapter(sizesAdapter);

        this.equipModInfoPane = findViewById(R.id.ec_mod_info);
        this.equipInfoPane = findViewById(R.id.ec_equip_info);

        this.renderer = new EquipQualifiedTreeCellRenderer(character, builder.getEquipment());

        initHeadMaps();
        initComponents(context);
        initDefaults();
    }

    /**
     * Setup any data related to multiple equipment heads.
     */
    private void initHeadMaps() {
        availEqmodModelMap = new HashMap<>();
        selectedEqmodModelMap = new HashMap<>();

        for (EquipmentHead head : validHeads) {
            availEqmodModelMap.put(head, new EquipModTreeViewModel(character, builder, head, true));
            selectedEqmodModelMap.put(head, new EquipModTreeViewModel(character, builder, head, false));
        }
    }

    private void initComponents(Context context) {
        if (validHeads.getSize() > 1) {
            headCombo.setVisibility(VISIBLE);
        }

        availableTable.setDisplayableFilter(bar);
        availableTable.setTreeViewModel(availEqmodModelMap.get(currentHead));
        availableTable.setTreeCellRenderer(renderer);

        availPanel.add(new JScrollPane(availableTable), BorderLayout.CENTER);

        if (builder.isWeapon()) {
            damageButton.setVisibility(View.VISIBLE);
        }

        if (builder.isResizable()) {
            sizeCombo.setVisibility(View.VISIBLE);
        }

        selectedTable.setTreeViewModel(selectedEqmodModelMap.get(currentHead));
        selectedTable.setTreeCellRenderer(renderer);
        selPanel.add(new JScrollPane(selectedTable), BorderLayout.CENTER);




		availableLayoutManager = new LinearLayoutManager(context);
		availableRecycler.setLayoutManager(availableLayoutManager);

		// specify an adapter (see also next example)
		ListFacade<EquipModFacade> eqModList = builder.getAvailList(head);

		avialableAdapter = new RecyclerView.Adapter<EquipModViewHolder>(myDataset);
		availableRecycler.setAdapter(avialableAdapter);
    }


    private void initDefaults() {
        equipInfoHandler = new EquipInfoHandler(character, builder);
        selectedTable.getSelectionModel().addListSelectionListener(equipInfoHandler);

        EquipModInfoHandler eqModInfoHandler = new EquipModInfoHandler(character, builder);
        availableTable.getSelectionModel().addListSelectionListener(eqModInfoHandler);
        selectedTable.getSelectionModel().addListSelectionListener(eqModInfoHandler);

        availableTable.addActionListener(addAction);
        sizeBoxModel = new SizeBoxModel();
        sizeCombo.setModel(sizeBoxModel);
        headBoxModel = new HeadBoxModel();
        headCombo.setModel(headBoxModel);
    }

    private class EquipInfoHandler implements ListSelectionListener {
        private final CharacterFacade character;
        private final EquipmentBuilderFacade builder2;

        public EquipInfoHandler(CharacterFacade character, EquipmentBuilderFacade builder) {
            this.character = character;
            builder2 = builder;
            refreshInfo();
        }

        private void refreshInfo() {
            EquipmentFacade equip = builder2.getEquipment();
            equipInfoPane.loadData(character.getInfoFactory().getHTMLInfo(equip), "text/html", "UTF-8");
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                refreshInfo();
            }
        }
    }

    private class EquipModInfoHandler implements ListSelectionListener {
        private final CharacterFacade character;
        private final EquipmentBuilderFacade builder;
        private EquipModFacade currObj;

        public EquipModInfoHandler(CharacterFacade character, EquipmentBuilderFacade builder) {
            this.character = character;
            this.builder = builder;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                Object obj = null;
                if (e.getSource() == availableTable.getSelectionModel()) {
                    int selectedRow = availableTable.getSelectedRow();
                    if (selectedRow != -1) {
                        obj = availableTable.getModel().getValueAt(selectedRow, 0);
                    }
                } else {
                    int selectedRow = selectedTable.getSelectedRow();
                    if (selectedRow != -1) {
                        obj = selectedTable.getModel().getValueAt(selectedRow, 0);
                    }
                }
                if (obj instanceof EquipModFacade && obj != currObj) {
                    currObj = (EquipModFacade) obj;
                    equipModInfoPane.loadData(character.getInfoFactory()
                            .getHTMLInfo((EquipModFacade) obj, builder.getEquipment()), "text/html", "UTF-8");
                }
            }
        }
    }

    private static class EquipModViewHolder extends RecyclerView.ViewHolder {
		public EquipModViewHolder(@NonNull View itemView) {
			super(itemView);
		}
	}

    private static class EquipModTreeViewModel
            implements TreeViewModel<EquipModFacade>, DataView<EquipModFacade>,
            Filter<EquipmentBuilderFacade, EquipModFacade>, ListListener<EquipModFacade> {

        private static final DefaultListFacade<? extends TreeView<EquipModFacade>> treeViews = new DefaultListFacade<TreeView<EquipModFacade>>(Arrays.asList(EquipModTreeView.values()));
        private final List<DefaultDataViewColumn> columns;
        private final boolean isAvailModel;
        private final FilteredListFacade<EquipmentBuilderFacade, EquipModFacade> equipMods;
        private final EquipmentBuilderFacade builder;
        private final EquipmentHead head;

        public EquipModTreeViewModel(CharacterFacade character, EquipmentBuilderFacade builder, EquipmentHead head, boolean isAvailModel) {
            this.builder = builder;
            this.head = head;
            this.isAvailModel = isAvailModel;
            equipMods = new FilteredListFacade<>();
            equipMods.setContext(builder);
            equipMods.setFilter(this);
            if (isAvailModel) {
                ListFacade<EquipModFacade> eqModList = builder.getAvailList(head);
                equipMods.setDelegate(eqModList);
                builder.getAvailList(head).addListListener(this);
                columns = Collections.singletonList(new DefaultDataViewColumn("in_source", String.class, false));
            } else {
                columns = Collections.singletonList(new DefaultDataViewColumn("in_source", String.class, false));
            }
        }

        @Override
        public ListFacade<? extends TreeView<EquipModFacade>> getTreeViews() {
            return treeViews;
        }

        @Override
        public int getDefaultTreeViewIndex() {
            return 0;
        }

        @Override
        public DataView<EquipModFacade> getDataView() {
            return this;
        }

        @Override
        public ListFacade<EquipModFacade> getDataModel() {
            if (isAvailModel) {
                return equipMods;
            }

            return builder.getSelectedList(head);
        }

        @Override
        public Object getData(EquipModFacade element, int column) {
            if (column == 0) {
                return element.getSource();
            }
            return null;
        }

        @Override
        public void setData(Object value, EquipModFacade element, int column) {
        }

        @Override
        public List<? extends DataViewColumn> getDataColumns() {
            return columns;
        }

        @Override
        public void elementAdded(ListEvent<EquipModFacade> e) {
            //equipMods.elementAdded(e);
        }

        @Override
        public void elementRemoved(ListEvent<EquipModFacade> e) {
            //equipMods.elementRemoved(e);
        }

        @Override
        public void elementsChanged(ListEvent<EquipModFacade> e) {
            //equipMods.refilter();
        }

        @Override
        public void elementModified(ListEvent<EquipModFacade> e) {
            //equipMods.refilter();
        }

        @Override
        public boolean accept(EquipmentBuilderFacade context, EquipModFacade element) {
            return true;
        }

        @Override
        public String getPrefsKey() {
            return isAvailModel ? "EqModTreeAvail" : "EqModTreeSelected";
        }

    }

    private enum EquipModTreeView implements TreeView<EquipModFacade> {
        NAME(LanguageBundle.getString(R.string.in_nameLabel)),
        TYPE_NAME(LanguageBundle.getString(R.string.in_typeName)),
        SOURCE_NAME(LanguageBundle.getString(R.string.in_sourceName));
        private final String name;

        EquipModTreeView(String name) {
            this.name = name;
        }

        @Override
        public String getViewName() {
            return name;
        }

        @Override
        public List<TreeViewPath<EquipModFacade>> getPaths(EquipModFacade pobj) {
            switch (this) {
                case NAME:
                    return Collections.singletonList(new TreeViewPath<>(pobj));
                case TYPE_NAME:
                    TreeViewPath<EquipModFacade> path = createTreeViewPath(pobj, (Object) pobj.getDisplayType().split("\\."));
                    return Collections.singletonList(path);
                case SOURCE_NAME:
                    return Collections.singletonList(new TreeViewPath<>(pobj, pobj.getSourceForNodeDisplay()));
                default:
                    throw new InternalError();
            }
        }

        /**
         * Create a TreeViewPath for the equipment modifier and paths.
         *
         * @param pobj The equipment modifier
         * @param path The paths under which the equipment modifier should be shown.
         * @return The TreeViewPath.
         */
        private static TreeViewPath<EquipModFacade> createTreeViewPath(EquipModFacade pobj, Object... path) {
            if (path.length == 0) {
                return new TreeViewPath<>(pobj);
            }
            if (path.length > 2) {
                return new TreeViewPath<>(pobj, path[0], path[1]);
            }
            return new TreeViewPath<>(pobj, path);
        }

    }

    private class HeadBoxModel extends CharacterComboBoxModel<EquipmentHead> {

        private final DefaultReferenceFacade<EquipmentHead> headRef;

        public HeadBoxModel() {
            setListFacade(validHeads);
            headRef = new DefaultReferenceFacade<>(
                    currentHead);
            setReference(headRef);
        }

        @Override
        public void setSelectedItem(Object anItem) {
            EquipmentHead head = (EquipmentHead) anItem;
            currentHead = head;
            headRef.set(head);
            availableTable.setTreeViewModel(availEqmodModelMap.get(currentHead));
            selectedTable.setTreeViewModel(selectedEqmodModelMap.get(currentHead));
        }

        @Override
        public void referenceChanged(ReferenceEvent<EquipmentHead> e) {
            super.referenceChanged(e);
        }

    }

    private class SizeBoxModel extends CharacterComboBoxModel<SizeAdjustmentFacade> {

        public SizeBoxModel() {
            setListFacade(character.getDataSet().getSizes());
            setReference(builder.getSizeRef());
        }

        @Override
        public void setSelectedItem(Object anItem) {
            builder.setSize((SizeAdjustmentFacade) anItem);
        }

        @Override
        public void referenceChanged(ReferenceEvent<SizeAdjustmentFacade> e) {
            super.referenceChanged(e);
            equipInfoHandler.refreshInfo();
        }
    }
}
