package pcgen.gui2.tabs.summary.panel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import net.redlightning.pcgen.R
import pcgen.facade.core.CharacterFacade
import pcgen.facade.core.RaceFacade
import pcgen.facade.util.ListFacade

class RacePanel(context: Context?) : View(context) {
    var raceComboBox: Spinner? = null
    var ageField: EditText? = null
    var ageComboBox: Spinner? = null

    init {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.panel_race, null)
        raceComboBox = layout.findViewById(R.id.race_spinner)
        ageField = layout.findViewById(R.id.age_edittext)
        ageComboBox = layout.findViewById(R.id.age_spinner)



    }

    fun setRaces(character: CharacterFacade) {
        val races: ListFacade<RaceFacade> = character.dataSet.races
        val raceNames: MutableList<RaceFacade> = mutableListOf()
        races.forEach { race ->  raceNames.add(race) }

        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, raceNames)
        raceComboBox!!.adapter = adapter
        raceComboBox!!.onItemSelectedListener == object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                character.setRace(races.getElementAt(position) as RaceFacade)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }
}