/*
 * Copyright 2010 (C) Connor Petty <cpmeister@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package pcgen.gui2.tabs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

import pcgen.core.PCAlignment;
import pcgen.facade.core.CharacterFacade;
import pcgen.facade.core.CharacterLevelFacade;
import pcgen.facade.core.CharacterLevelsFacade;
import pcgen.facade.core.ClassFacade;
import pcgen.facade.core.DataSetFacade;
import pcgen.facade.core.DeityFacade;
import pcgen.facade.core.GenderFacade;
import pcgen.facade.core.HandedFacade;
import pcgen.facade.core.InfoFacade;
import pcgen.facade.core.RaceFacade;
import pcgen.facade.core.SimpleFacade;
import pcgen.facade.core.TodoFacade;
import pcgen.facade.util.ReferenceFacade;
import pcgen.facade.util.event.ListEvent;
import pcgen.facade.util.event.ListListener;
import pcgen.facade.util.event.ReferenceEvent;
import pcgen.facade.util.event.ReferenceListener;
import pcgen.gui2.UIPropertyContext;
import pcgen.gui2.dialog.CharacterHPDialog;
import pcgen.gui2.dialog.KitSelectionDialog;
import pcgen.gui2.dialog.RandomNameDialog;
import pcgen.gui2.dialog.SinglePrefDialog;
import pcgen.gui2.prefs.CharacterStatsPanel;
import pcgen.gui2.tabs.models.CharacterComboBoxModel;
import pcgen.gui2.tabs.models.DeferredCharacterComboBoxModel;
import pcgen.gui2.tabs.models.FormattedFieldHandler;
import pcgen.gui2.tabs.models.TextFieldHandler;
import pcgen.gui2.tabs.summary.ClassLevelTableModel;
import pcgen.gui2.tabs.summary.InfoPaneHandler;
import pcgen.gui2.tabs.summary.LanguageTableModel;
import pcgen.gui2.tabs.summary.StatTableModel;
import pcgen.gui2.tools.Icons;
import pcgen.gui2.util.FacadeComboBoxModel;
import pcgen.gui2.util.FontManipulation;
import pcgen.gui2.util.SimpleTextIcon;
import pcgen.system.LanguageBundle;

/**
 * This component displays a basic summary of a character such as name,
 * alignment, race, class, and stat information.
 */
public class SummaryInfoTab extends View implements CharacterInfoTab, TodoHandler {
    private final View basicsPanel;
    private final View classPanel;
    private final EditText characterNameField;
    private final Spinner characterTypeComboBox;
    private final EditText playerNameField;
    private final EditText tabLabelField;
    private final EditText ageField;
    private final EditText expField;
    private final EditText nextlevelField;
    private final Spinner xpTableComboBox;
    private final EditText expmodField;
    private final EditText addLevelsField;
    private final EditText removeLevelsField;
    private final TableLayout statsTable;
    private final ListView classLevelTable;
    private final TableLayout languageTable;
    private final Spinner genderComboBox;
    private final Spinner handsComboBox;
    private final Spinner alignmentComboBox;
    private final Spinner deityComboBox;
    private final Spinner raceComboBox;
    private final Spinner ageComboBox;
    private final Spinner classComboBox;
    private final InfoBoxRenderer infoBoxRenderer;
    private final ClassBoxRenderer classBoxRenderer;
    private final Button generateRollsButton;
    private final Button rollMethodButton;
    private final Button createMonsterButton;
    private final Button expaddButton;
    private final Button expsubtractButton;
    private final Button addLevelsButton;
    private final Button removeLevelsButton;
    private final Button hpButton;
    private final TextView totalHPLabel;
    private final EditText infoPane;
    private final TextView statTotalLabel;
    private final TextView statTotal;
    private final TextView modTotalLabel;
    private final TextView modTotal;
    private final EditText todoPane;

    //private final Button random;
    private ScrollView langScroll;

    public SummaryInfoTab(Context context) {
        super(context);
        View root = LayoutInflater.from(context).inflate(R.layout.fragment_summary, null);

        /* Basics */
        basicsPanel = root.findViewById(R.id.panel_basic);
        characterNameField = basicsPanel.findViewById(R.id.summary_pc_name);
        //this.random = new JButton();
        //FontManipulation.xsmall(random);
        characterTypeComboBox = basicsPanel.findViewById(R.id.summary_type);
        playerNameField = basicsPanel.findViewById(R.id.summary_player);
        genderComboBox = basicsPanel.findViewById(R.id.summary_gender);
        handsComboBox = basicsPanel.findViewById(R.id.summary_handed);
        alignmentComboBox = basicsPanel.findViewById(R.id.summary_alignment);
        deityComboBox = basicsPanel.findViewById(R.id.summary_diety);

//        raceComboBox = basicsPanel.findViewById(R.id.summary_race);
//        ageField = basicsPanel.findViewById(R.id.summary_age_field);
//        ageComboBox = basicsPanel.findViewById(R.id.summary_age);
        expField = basicsPanel.findViewById(R.id.summary_current_xp);
        nextlevelField = basicsPanel.findViewById(R.id.summary_next_level);
        xpTableComboBox = basicsPanel.findViewById(R.id.summary_level_table);
        expaddButton = basicsPanel.findViewById(R.id.summary_add_xp);
        expsubtractButton = basicsPanel.findViewById(R.id.summary_subtract_xp);
        expmodField = basicsPanel.findViewById(R.id.summary_xp_mod);


        /* Class */
        classPanel = root.findViewById(R.id.panel_class);
        removeLevelsButton = classPanel.findViewById(R.id.summary_class_remove);
        addLevelsButton = classPanel.findViewById(R.id.summary_class_add);
        addLevelsField = classPanel.findViewById(R.id.summary_class_levels_mod);
        classLevelTable = classPanel.findViewById(R.id.summary_class_levels_list);

        statsTable = new TableView();
        languageTable = new TableView();
        classComboBox = new Spinner();
        tabLabelField = new EditText();
        generateRollsButton = new Button();
        rollMethodButton = new Button();
        createMonsterButton = new Button();


        hpButton = new Button();
        totalHPLabel = new TextView();
        infoPane = new JEditorPane();
        statTotalLabel = new TextView();
        statTotal = new TextView();
        modTotalLabel = new TextView();
        modTotal = new TextView();
        todoPane = new JEditorPane();
        infoBoxRenderer = new InfoBoxRenderer();
        classBoxRenderer = new ClassBoxRenderer();
        LanguageTableModel.initializeTable(languageTable);
        initTodoPanel(todoPanel);
        initMiddlePanel(scoresPanel);
        initRightPanel(rightPanel);
    }

    private void initialize(Context context) {
        removeLevelsButton.setOnClickListener(view -> {
            Number levels = (Number) addLevelsField.getText();
            character.removeCharacterLevels(levels.intValue());
        });
    }

    /**
     * Initialise the "Things to be Done" panel. Creates the required components
     * and places them in the panel.
     *
     * @param panel The panel to be initialised
     */
    private void initTodoPanel(View panel) {
        todoPane.setContentType("text/html");
        // An HTMLDocument font size and family already defaults to the UI font default, no need to add that on top.
//		String bodyRule =
//				"body { font-family: " + textFont.getFamily() + "; " + "font-size: " + //$NON-NLS-2$ //$NON-NLS-3$
//				textFont.getSize() + "pt; }";
//		((HTMLDocument) todoPane.getDocument()).getStyleSheet().addRule(
//				bodyRule);
        todoPane.setEditable(false);

    }

    private void initMiddlePanel(View middlePanel) {

        JPanel statsPanel = new JPanel();
        setPanelTitle(statsPanel, LanguageBundle.getString("in_sumAbilityScores"));
        statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));

        StatTableModel.initializeTable(statsTable);

        JPanel statsBox = new JPanel();
        statsBox.setLayout(new BoxLayout(statsBox, BoxLayout.X_AXIS));
        statsBox.add(pane);
        statsPanel.add(statsBox);

        JPanel statTotalPanel = new JPanel();
        FontManipulation.title(statTotalLabel);
        statTotalPanel.add(statTotalLabel);
        statTotalPanel.add(statTotal);
        FontManipulation.title(modTotalLabel);
        statTotalPanel.add(modTotalLabel);
        statTotalPanel.add(modTotal);
        generateRollsButton.setText(LanguageBundle.getString("in_sumGenerate_Rolls"));
        statTotalPanel.add(generateRollsButton);
        rollMethodButton.setText(LanguageBundle.getString("in_sumRoll_Method"));
        statTotalPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        statTotalPanel.add(rollMethodButton);
        statsPanel.add(statTotalPanel);

        middlePanel.add(statsPanel);

        InfoPaneHandler.initializeEditorPane(infoPane);

        pane = new JScrollPane(infoPane);
        middlePanel.add(pane);
    }

    private void initRightPanel(JPanel rightPanel) {
        rightPanel.setLayout(new GridBagLayout());
        /*
         * initialize Components
         */
        classPanel.setOpaque(false);
        ageField.setHorizontalAlignment(SwingConstants.RIGHT);
        expField.setHorizontalAlignment(SwingConstants.RIGHT);
        nextlevelField.setHorizontalAlignment(SwingConstants.RIGHT);
        nextlevelField.setEnabled(false);
        expmodField.setHorizontalAlignment(SwingConstants.RIGHT);


        classComboBox.setPrototypeDisplayValue("PrototypeDisplayValue");

        expaddButton.setMargin(new Insets(0, 8, 0, 8));
        expsubtractButton.setMargin(new Insets(0, 8, 0, 8));
        hpButton.setMargin(new Insets(0, 0, 0, 0));

        JPanel expmodPanel = new JPanel(new GridBagLayout());
        JPanel levelPanel = new JPanel();
        JLabel raceLabel = createLabel("in_sumRace");
        JLabel ageLabel = createLabel("in_sumAge");
        JLabel classLabel = createLabel("in_sumClass");
        JLabel hpLabel = createLabel("in_sumTotalHP");
        JLabel expLabel = createLabel("in_sumCurrentXp");
        JLabel nextlevelLabel = createLabel("in_sumNextlevel");
        JLabel xpTableLabel = createLabel("in_sumXpTable");
        JLabel expmodLabel = createLabel("in_sumExpMod");
        expmodLabel.setHorizontalAlignment(SwingConstants.CENTER);
        initLevelPanel(levelPanel);
        /*
         * initialize constant variables
         */
        Insets racePanelInsets = racePanel.getInsets();
        Insets classPanelInsets = classPanel.getInsets();
        /*
         * racePanel
         */
//        GridBagConstraints gbc = new GridBagConstraints();
//        gbc.fill = GridBagConstraints.BOTH;
//        gbc.insets = new Insets(racePanelInsets.top, racePanelInsets.left, 0, 0);
//        gbc.gridwidth = 2;
//        rightPanel.add(raceLabel, gbc);
//        gbc.insets = new Insets(racePanelInsets.top, 1, 1, racePanelInsets.right);
//        gbc.gridwidth = GridBagConstraints.REMAINDER;
//        rightPanel.add(raceComboBox, gbc);
//        gbc.insets = new Insets(0, racePanelInsets.left, 0, 1);
//        gbc.gridwidth = 1;
//        rightPanel.add(ageLabel, gbc);
//        gbc.insets = new Insets(1, 1, 1, 1);
//        rightPanel.add(ageField, gbc);
//        gbc.gridwidth = GridBagConstraints.REMAINDER;
//        gbc.insets = new Insets(1, 1, 1, racePanelInsets.right);
//        rightPanel.add(ageComboBox, gbc);
//        gbc.insets = new Insets(1, racePanelInsets.left, racePanelInsets.bottom, racePanelInsets.right);
//        rightPanel.add(createMonsterButton, gbc);
        /*
         * classPanel
         */
        gbc.gridwidth = 2;
        gbc.insets = new Insets(classPanelInsets.top, classPanelInsets.left, 0, 0);
        rightPanel.add(classLabel, gbc);
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.insets = new Insets(classPanelInsets.top, 0, 0, classPanelInsets.right);
        rightPanel.add(classComboBox, gbc);

        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(7, classPanelInsets.left, 0, classPanelInsets.right);
        rightPanel.add(levelPanel, gbc);
        gbc.insets.top = 0;
        gbc.insets.bottom = 10;
        gbc.weighty = 0;
        {
            JPanel hpPanel = new JPanel(new FlowLayout());
            hpPanel.add(hpLabel);
            hpPanel.add(Box.createHorizontalStrut(3));
            hpPanel.add(totalHPLabel);
            hpPanel.add(hpButton);
            rightPanel.add(hpPanel, gbc);
        }
        gbc.insets.bottom = 0;
        //gbc.ipady = 20;
        GridBagConstraints leftgbc = new GridBagConstraints();
        leftgbc.insets = new Insets(0, classPanelInsets.left, 0, 0);
        leftgbc.gridwidth = 2;
        leftgbc.fill = GridBagConstraints.BOTH;

        GridBagConstraints rightgbc = new GridBagConstraints();
        rightgbc.insets = new Insets(0, 0, 0, classPanelInsets.right);
        rightgbc.gridwidth = GridBagConstraints.REMAINDER;
        rightgbc.fill = GridBagConstraints.BOTH;

        rightPanel.add(expLabel, leftgbc);
        rightPanel.add(expField, rightgbc);
        rightPanel.add(nextlevelLabel, leftgbc);
        rightPanel.add(nextlevelField, rightgbc);
        rightPanel.add(xpTableLabel, leftgbc);
        rightPanel.add(xpTableComboBox, rightgbc);

        gbc.insets.top = 10;
        rightPanel.add(expmodLabel, gbc);
        {
            GridBagConstraints gbc2 = new GridBagConstraints();
            gbc2.fill = GridBagConstraints.HORIZONTAL;
            gbc2.weightx = 1.0;
            gbc2.insets = new Insets(0, 1, 0, 1);
            expmodPanel.add(expaddButton, gbc2);
            expmodPanel.add(expsubtractButton, gbc2);
        }
        leftgbc.insets.bottom = classPanelInsets.bottom;
        leftgbc.weightx = 0.3;
        rightPanel.add(expmodPanel, leftgbc);
        rightgbc.insets.bottom = classPanelInsets.bottom;
        rightgbc.weightx = 0.7;
        rightPanel.add(expmodField, rightgbc);

        gbc = new GridBagConstraints();
        gbc.gridx = gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 3;
        gbc.fill = GridBagConstraints.BOTH;
        rightPanel.add(racePanel, gbc);

        gbc.gridy = 3;
        gbc.gridheight = GridBagConstraints.REMAINDER;
        rightPanel.add(classPanel, gbc);
    }

    private void initLevelPanel(JPanel panel) {
        panel.setLayout(new GridBagLayout());
        JLabel addLabel = createLabel("in_sumAddLevels");
        JLabel removeLabel = createLabel("in_sumRemoveLevels");
        JLabel darrowLabel = new JLabel(Icons.button_arrow_down.getImageIcon());
        JLabel uarrowLabel = new JLabel(Icons.button_arrow_up.getImageIcon());

        addLevelsButton.setMargin(new Insets(0, 8, 0, 8));
        addLevelsField.setValue(1);
        addLevelsField.setHorizontalAlignment(SwingConstants.RIGHT);


        GridBagConstraints gbc1 = new GridBagConstraints();
        GridBagConstraints gbc2 = new GridBagConstraints();
        gbc1.weightx = gbc2.weightx = 0.5;
        gbc1.insets = new Insets(1, 0, 1, 0);
        gbc2.insets = new Insets(1, 0, 1, 0);
        gbc2.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(addLabel, gbc1);
        panel.add(removeLabel, gbc2);
        gbc1.ipadx = 30;
        panel.add(addLevelsField, gbc1);

        gbc1.ipadx = 0;
        panel.add(addLevelsButton, gbc1);
        gbc2.ipadx = 0;
        panel.add(removeLevelsButton, gbc2);
        panel.add(darrowLabel, gbc1);
        panel.add(uarrowLabel, gbc2);

        ClassLevelTableModel.initializeTable(classLevelTable);
        gbc2.weightx = 0;
        gbc2.weighty = 1;
        gbc2.fill = GridBagConstraints.BOTH;
        panel.add(new JScrollPane(classLevelTable), gbc2);
    }

    private void resetBasicsPanel() {
        basicsPanel.removeAll();

            JLabel label = createLabel("in_sumName");
            random.setText(LanguageBundle.getString("in_randomButton"));
            random.setMargin(new Insets(0, 0, 0, 0));
            gbc.insets = new Insets(0, 2, 3, 2);
            basicsPanel.add(random, gbc);

            gbc.insets = new Insets(0, 0, 3, 2);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.fill = GridBagConstraints.BOTH;
            gbc.weightx = 1.0;
            basicsPanel.add(characterNameField, gbc);
        }
        Insets insets = new Insets(0, 0, 3, 2);
        Font labelFont = null;
        addGridBagLayer(basicsPanel, labelFont, insets, "in_sumCharType", characterTypeComboBox);
        addGridBagLayer(basicsPanel, labelFont, insets, "in_sumPlayer", playerNameField);
        addGridBagLayer(basicsPanel, labelFont, insets, "in_sumTabLabel", tabLabelField);
        if (genderComboBox.getModel().getSize() != 0) {
            addGridBagLayer(basicsPanel, labelFont, insets, "in_sumGender", genderComboBox);
        }
        if (handsComboBox.getModel().getSize() != 0) {
            addGridBagLayer(basicsPanel, labelFont, insets, "in_sumHanded", handsComboBox);
        }
        if (alignmentComboBox.getModel().getSize() != 0) {
            addGridBagLayer(basicsPanel, labelFont, insets, "in_sumAlignment", alignmentComboBox);
        }
        if (deityComboBox.getModel().getSize() != 0) {
            addGridBagLayer(basicsPanel, labelFont, insets, "in_domDeityLabel", deityComboBox);
        }

        langScroll = new JScrollPane(languageTable);
        basicsPanel.add(langScroll, gbc);
        basicsPanel.revalidate();
    }

    private void addGridBagLayer(JPanel panel, Font font, Insets insets, String text, JComponent comp) {
        GridBagConstraints gbc = new GridBagConstraints();
        JLabel label = new JLabel(LanguageBundle.getString(text));
        label.setFont(font);
        gbc.anchor = java.awt.GridBagConstraints.WEST;
        gbc.gridwidth = 2;
        panel.add(label, gbc);

        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.BOTH;
        if (insets != null) {
            gbc.insets = insets;
        }
        panel.add(comp, gbc);
    }

    @Override
    public void adviseTodo(String fieldName) {
        if ("Name".equals(fieldName))
        {
            characterNameField.requestFocusInWindow();
            characterNameField.selectAll();
        } else if ("Race".equals(fieldName))
        {
            raceComboBox.requestFocusInWindow();
            highlightBorder(raceComboBox);
        } else if ("Class".equals(fieldName))
        {
            classComboBox.requestFocusInWindow();
            highlightBorder(classComboBox);
        } else if ("Languages".equals(fieldName))
        {
            highlightBorder(langScroll);
        } else if ("Ability Scores".equals(fieldName))
        {
            deityComboBox.requestFocusInWindow();
            deityComboBox.transferFocus();
        }

    }

    private void highlightBorder(final JComponent comp) {
        final Border oldBorder = comp.getBorder();
        Border highlightBorder = BorderFactory.createLineBorder(Color.GREEN, 3);
        comp.setBorder(highlightBorder);

        SwingUtilities.invokeLater(() ->
        {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // Ignored as we'll exit shortly anyway.
            }
            comp.setBorder(oldBorder);
        });
    }

    @Override
    public ModelMap createModels(final CharacterFacade character) {
        ModelMap models = new ModelMap();

        models.put(LabelAndFieldHandler.class, new LabelAndFieldHandler(character));
        models.put(ComboBoxRendererHandler.class, new ComboBoxRendererHandler(character));
        models.put(ComboBoxModelHandler.class, new ComboBoxModelHandler(character));

        models.put(RandomNameAction.class, new RandomNameAction(character,
                (JFrame) SwingUtilities.getWindowAncestor(this)));
        models.put(ClassLevelTableModel.class, new ClassLevelTableModel(character, classLevelTable, classComboBox));

        models.put(GenerateRollsAction.class, new GenerateRollsAction(character));
        models.put(RollMethodAction.class, new RollMethodAction(character,
                (JFrame) SwingUtilities.getWindowAncestor(this)));
        models.put(CreateMonsterAction.class, new CreateMonsterAction(
                character, (JFrame) SwingUtilities.getWindowAncestor(this)));
        models.put(AddLevelsAction.class, new AddLevelsAction(character));
        models.put(RemoveLevelsAction.class, new RemoveLevelsAction(character));
        models.put(StatTableModel.class, new StatTableModel(character, statsTable));
        models.put(LanguageTableModel.class, new LanguageTableModel(character, languageTable));
        models.put(InfoPaneHandler.class, new InfoPaneHandler(character, infoPane));
        models.put(ExpAddAction.class, new ExpAddAction(character));
        models.put(ExpSubtractAction.class, new ExpSubtractAction(character));
        models.put(TodoListHandler.class, new TodoListHandler(character));
        models.put(HPHandler.class, new HPHandler(character));
        return models;
    }

    @Override
    public TabTitle getTabTitle() {
        return tabTitle;
    }

    @Override
    public void storeModels(ModelMap models) {
        models.get(LabelAndFieldHandler.class).uninstall();
        models.get(ComboBoxModelHandler.class).uninstall();

        models.get(LanguageTableModel.class).uninstall();
        models.get(ClassLevelTableModel.class).uninstall();
        models.get(InfoPaneHandler.class).uninstall();
        models.get(StatTableModel.class).uninstall();
        models.get(TodoListHandler.class).uninstall();
        models.get(GenerateRollsAction.class).uninstall();
        models.get(RollMethodAction.class).uninstall();
        models.get(HPHandler.class).uninstall();

        models.get(ComboBoxRendererHandler.class).uninstall();
    }

    @Override
    public void restoreModels(ModelMap models) {
        models.get(LabelAndFieldHandler.class).install();
        models.get(ComboBoxRendererHandler.class).install();
        models.get(ComboBoxModelHandler.class).install();

        models.get(InfoPaneHandler.class).install();
        models.get(LanguageTableModel.class).install();
        models.get(StatTableModel.class).install();
        models.get(ClassLevelTableModel.class).install();
        models.get(TodoListHandler.class).install();
        models.get(GenerateRollsAction.class).install();
        models.get(RollMethodAction.class).install();
        models.get(HPHandler.class).install();

        random.setAction(models.get(RandomNameAction.class));
        generateRollsButton.setAction(models.get(GenerateRollsAction.class));
        rollMethodButton.setAction(models.get(RollMethodAction.class));
        createMonsterButton.setAction(models.get(CreateMonsterAction.class));
        AddLevelsAction addLevelsAction = models.get(AddLevelsAction.class);
        addLevelsButton.setAction(addLevelsAction);
        addLevelsField.setAction(addLevelsAction);
        RemoveLevelsAction removeLevelsAction = models.get(RemoveLevelsAction.class);
        removeLevelsButton.setAction(removeLevelsAction);
        ExpAddAction expAddAction = models.get(ExpAddAction.class);
        expaddButton.setAction(expAddAction);
        expmodField.setAction(expAddAction);
        expsubtractButton.setAction(models.get(ExpSubtractAction.class));
        addLevelsAction.install();

        resetBasicsPanel();
    }

    private class LabelAndFieldHandler {

        private final LabelHandler statTotalLabelHandler;
        private final LabelHandler statTotalHandler;
        private final LabelHandler modTotalLabelHandler;
        private final LabelHandler modTotalHandler;
        private final TextFieldHandler charNameHandler;
        private final TextFieldHandler playerNameHandler;
        private final TextFieldHandler tabNameHandler;
        private final FormattedFieldHandler ageHandler;
        private final FormattedFieldHandler expHandler;
        private final FormattedFieldHandler nextLevelHandler;

        LabelAndFieldHandler(final CharacterFacade character) {

            statTotalLabelHandler = new LabelHandler(statTotalLabel, character.getStatTotalLabelTextRef());
            statTotalHandler = new LabelHandler(statTotal, character.getStatTotalTextRef());
            modTotalLabelHandler = new LabelHandler(modTotalLabel, character.getModTotalLabelTextRef());
            modTotalHandler = new LabelHandler(modTotal, character.getModTotalTextRef());

            //Manages the character name text field
            charNameHandler = new TextFieldHandler(characterNameField, character.getNameRef()) {

                @Override
                protected void textChanged(String text) {
                    character.setName(text);
                }

            };

            //Manages the player name text field.
            playerNameHandler = new TextFieldHandler(playerNameField, character.getPlayersNameRef()) {

                @Override
                protected void textChanged(String text) {
                    character.setPlayersName(text);
                }

            };
            //Manages the tab name text field.
            tabNameHandler = new TextFieldHandler(tabLabelField, character.getTabNameRef()) {

                @Override
                protected void textChanged(String text) {
                    character.setTabName(text);
                }

            };

            /*
             * Handler for the Age field. This listens for and processes both
             * changes to the value from the character and modifications to the
             * field made by the user.
             */
            ageHandler = new FormattedFieldHandler(ageField, character.getAgeRef()) {
                @Override
                protected void valueChanged(int value) {
                    character.setAge(value);
                }

            };

            /*
             * Handler for the Current Experience field. This listens for and
             * processes both changes to the value from the character and
             * modifications to the field made by the user.
             */
            expHandler = new FormattedFieldHandler(expField, character.getXPRef()) {
                @Override
                protected void valueChanged(int value) {
                    character.setXP(value);
                }

            };

            /*
             * Handler for the Next Level field. This is a read-only field so
             * the handler only listens for changes to the value from the
             * character.
             */
            nextLevelHandler = new FormattedFieldHandler(nextlevelField, character.getXPForNextLevelRef()) {

                @Override
                protected void valueChanged(int value) {
                    //This will never be called
                }

            };
        }

        public void install() {
            charNameHandler.install();
            playerNameHandler.install();
            tabNameHandler.install();
            ageHandler.install();
            expHandler.install();
            nextLevelHandler.install();
            statTotalLabelHandler.install();
            statTotalHandler.install();
            modTotalLabelHandler.install();
            modTotalHandler.install();
        }

        public void uninstall() {
            charNameHandler.uninstall();
            playerNameHandler.uninstall();
            tabNameHandler.uninstall();
            ageHandler.uninstall();
            expHandler.uninstall();
            nextLevelHandler.uninstall();
            statTotalLabelHandler.uninstall();
            statTotalHandler.uninstall();
            modTotalLabelHandler.uninstall();
            modTotalHandler.uninstall();
        }
    }

    private class ComboBoxModelHandler {

        private final CharacterComboBoxModel<GenderFacade> genderModel;
        private final CharacterComboBoxModel<HandedFacade> handsModel;
        private final CharacterComboBoxModel<PCAlignment> alignmentModel;
        private final CharacterComboBoxModel<DeityFacade> deityModel;
        private final DeferredCharacterComboBoxModel<RaceFacade> raceModel;
        private final CharacterComboBoxModel<SimpleFacade> ageCatModel;
        private final FacadeComboBoxModel<ClassFacade> classModel;
        private final CharacterComboBoxModel<String> xpTableModel;
        private final CharacterComboBoxModel<String> characterTypeModel;

        ComboBoxModelHandler(final CharacterFacade character) {
            DataSetFacade dataset = character.getDataSet();

            //initialize character type model
            characterTypeModel = new CharacterComboBoxModel<String>(dataset.getCharacterTypes(), character.getCharacterTypeRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setCharacterType((String) anItem);
                }

            };

            //initialize gender model
            genderModel = new CharacterComboBoxModel<GenderFacade>(character.getAvailableGenders(), character.getGenderRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setGender((GenderFacade) anItem);
                }

            };

            //initialize handed model
            handsModel = new CharacterComboBoxModel<HandedFacade>(character.getAvailableHands(), character.getHandedRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setHanded((HandedFacade) anItem);
                }

            };

            //initialize alignment model
            alignmentModel = new CharacterComboBoxModel<PCAlignment>(dataset.getAlignments(), character.getAlignmentRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setAlignment((PCAlignment) anItem);
                }

            };

            //initialize deity model
            deityModel = new CharacterComboBoxModel<DeityFacade>(dataset.getDeities(), character.getDeityRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setDeity((DeityFacade) anItem);
                }

            };

//            //initialize race model
//            raceModel = new DeferredCharacterComboBoxModel<RaceFacade>(dataset.getRaces(), character.getRaceRef()) {
//                @Override
//                public void commitSelectedItem(Object anItem) {
//                    character.setRace((RaceFacade) anItem);
//                }
//            };

            //initialize age category model
            ageCatModel = new CharacterComboBoxModel<SimpleFacade>(character.getAgeCategories(), character.getAgeCategoryRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setAgeCategory((SimpleFacade) anItem);
                }

            };

            //initialize XP table model
            xpTableModel = new CharacterComboBoxModel<String>(dataset.getXPTableNames(), character.getXPTableNameRef()) {

                @Override
                public void setSelectedItem(Object anItem) {
                    character.setXPTable((String) anItem);
                }

            };

            classModel = new FacadeComboBoxModel<>(dataset.getClasses(), null);
        }

        public void install() {
            characterTypeComboBox.setModel(characterTypeModel);
            genderComboBox.setModel(genderModel);
            handsComboBox.setModel(handsModel);
            alignmentComboBox.setModel(alignmentModel);
            deityComboBox.setModel(deityModel);
            //raceComboBox.setModel(raceModel);
            //raceComboBox.addFocusListener(raceModel);
            ageComboBox.setModel(ageCatModel);
            classComboBox.setModel(classModel);
            xpTableComboBox.setModel(xpTableModel);
        }

        public void uninstall() {
            raceComboBox.removeFocusListener(raceModel);
        }
    }

    private class ComboBoxRendererHandler {

        private final CharacterFacade character;

        ComboBoxRendererHandler(CharacterFacade character) {
            this.character = character;
        }

        public void install() {
            infoBoxRenderer.setCharacter(character);
            classBoxRenderer.setCharacter(character);
        }

        public void uninstall() {
            infoBoxRenderer.setCharacter(null);
            classBoxRenderer.setCharacter(null);
        }
    }

    private static class CharacterComboBoxRenderer extends DefaultListCellRenderer {

        protected CharacterFacade character;

        public void setCharacter(CharacterFacade character) {
            this.character = character;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            setToolTipText(value == null ? null : value.toString());
            return this;
        }

    }

    private static class InfoBoxRenderer extends CharacterComboBoxRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof InfoFacade && !character.isQualifiedFor((InfoFacade) value)) {
                if (index == -1) {// this is a hack to prevent the combobox from overwriting the text color
                    setText("");
                    setIcon(new SimpleTextIcon(list, value.toString(), UIPropertyContext.getNotQualifiedColor()));
                } else {
                    setForeground(UIPropertyContext.getNotQualifiedColor());
                }
            }
            return this;
        }

    }

    private static class ClassBoxRenderer extends CharacterComboBoxRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof ClassFacade && !character.isQualifiedFor((ClassFacade) value)) {
                if (index == -1) {// this is a hack to prevent the combobox from overwriting the text color
                    setText("");
                    setIcon(new SimpleTextIcon(list, value.toString(), UIPropertyContext.getNotQualifiedColor()));
                } else {
                    setForeground(UIPropertyContext.getNotQualifiedColor());
                }
            }
            return this;
        }

    }

    private class HPHandler extends AbstractAction implements ReferenceListener<Integer> {

        private final CharacterFacade character;
        private final ReferenceFacade<Integer> ref;

        HPHandler(CharacterFacade character) {
            this.character = character;
            this.ref = character.getTotalHPRef();
            putValue(NAME, LanguageBundle.getString("in_edit"));
        }

        public void install() {
            hpButton.setAction(this);
            totalHPLabel.setText(ref.get().toString());
            ref.addReferenceListener(this);
        }

        public void uninstall() {
            ref.removeReferenceListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CharacterHPDialog.showHPDialog(SummaryInfoTab.this, character);
        }

        @Override
        public void referenceChanged(ReferenceEvent<Integer> e) {
            totalHPLabel.setText(ref.get().toString());
        }

    }

    private class RandomNameAction extends AbstractAction {

        private final CharacterFacade character;
        private final JFrame frame;

        RandomNameAction(CharacterFacade character, JFrame frame) {
            this.character = character;
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String gender = character.getGenderRef().get() != null ? character.getGenderRef().get().toString() : "";
            RandomNameDialog dialog = new RandomNameDialog(frame, gender);
            dialog.setVisible(true);
            String chosenName = dialog.getChosenName();
            if (chosenName != null && !chosenName.isEmpty() && !chosenName.equals(LanguageBundle.getString("in_rndNmDefault")))
            {
                character.setName(chosenName);
            }
            String chosenGender = dialog.getGender();
            character.setGender(chosenGender);
        }

    }

    /**
     * Handler for actions from the generate rolls button. Also defines the
     * appearance of the button.
     */
    private final class GenerateRollsAction extends AbstractAction implements ListListener<CharacterLevelFacade>, ReferenceListener<Integer> {

        private final CharacterFacade character;

        GenerateRollsAction(CharacterFacade character) {
            this.character = character;
            putValue(NAME, LanguageBundle.getString("in_sumGenerate_Rolls"));
            update();
        }

        /**
         * Attach the handler to the screen button. e.g. When the character is
         * made active.
         */
        public void install() {
            // Listen to the total levels
            character.getCharacterLevelsFacade().addListListener(this);

            // Listen to the roll method
            character.getRollMethodRef().addReferenceListener(this);
        }

        /**
         * Detach the handler from the on screen button. e.g. when the character
         * is no longer being displayed.
         */
        public void uninstall() {
            character.getCharacterLevelsFacade().removeListListener(this);
            character.getRollMethodRef().removeReferenceListener(this);
        }

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            character.rollStats();
        }

        /**
         * Update the state of the button.
         */
        public void update() {
            setEnabled(character.isStatRollEnabled());
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementAdded(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementAdded(ListEvent<CharacterLevelFacade> e) {
            update();
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementRemoved(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementRemoved(ListEvent<CharacterLevelFacade> e) {
            update();
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementsChanged(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementsChanged(ListEvent<CharacterLevelFacade> e) {
            update();
        }

        @Override
        public void referenceChanged(ReferenceEvent<Integer> e) {
            update();
        }

        @Override
        public void elementModified(ListEvent<CharacterLevelFacade> e) {
            update();
        }

    }

    /**
     * Handler for actions from the generate rolls button. Also defines the
     * appearance of the button.
     */
    private class RollMethodAction extends AbstractAction {

        private final JFrame parent;
        private final CharacterFacade character;

        RollMethodAction(CharacterFacade character, JFrame parent) {
            putValue(NAME, LanguageBundle.getString("in_sumRoll_Method"));
            putValue(SHORT_DESCRIPTION, LanguageBundle.getString("in_sumRoll_Method_Tip"));
            this.parent = parent;
            this.character = character;
        }

        /**
         * Attach the handler to the screen button. e.g. When the character is
         * made active.
         */
        public void install() {
        }

        /**
         * Detach the handler from the on screen button. e.g. when the character
         * is no longer being displayed.
         */
        public void uninstall() {
        }

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            CharacterStatsPanel charStatsPanel = new CharacterStatsPanel(null);
            SinglePrefDialog prefsDialog = new SinglePrefDialog(parent, charStatsPanel);
            charStatsPanel.setParent(prefsDialog);
            Utility.setComponentRelativeLocation(parent, prefsDialog);
            prefsDialog.setVisible(true);
            character.refreshRollMethod();
        }

    }

    private class CreateMonsterAction extends AbstractAction {

        private final CharacterFacade character;
        private final JFrame frame;

        CreateMonsterAction(CharacterFacade character, JFrame frame) {
            putValue(NAME, LanguageBundle.getString("in_sumCreateMonster"));
            putValue(SHORT_DESCRIPTION, LanguageBundle.getString("in_sumCreateMonster_Tip"));
            this.character = character;
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            KitSelectionDialog kitDialog
                    = new KitSelectionDialog(frame, character);
            Utility.setComponentRelativeLocation(frame, kitDialog);
            kitDialog.setVisible(true);
        }

    }

    private class AddLevelsAction extends AbstractAction {

        private final CharacterFacade character;

        AddLevelsAction(CharacterFacade character) {
            this.character = character;
            //putValue(SMALL_ICON, new SignIcon(Sign.Plus));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ClassFacade c = (ClassFacade) classComboBox.getSelectedItem();
            if (c != null) {
                Number levels = (Number) addLevelsField.getValue();
                if (levels.intValue() >= 0) {
                    ClassFacade[] classes = new ClassFacade[levels.intValue()];
                    Arrays.fill(classes, c);
                    character.addCharacterLevels(classes);
                }
            }
        }

        public void install() {
            CharacterLevelsFacade characterLevelsFacade
                    = character.getCharacterLevelsFacade();
            int maxLvl = characterLevelsFacade.getSize();
            if (maxLvl > 0) {
                ClassFacade classTaken
                        = characterLevelsFacade
                        .getClassTaken(characterLevelsFacade
                                .getElementAt(maxLvl - 1));
                classComboBox.setSelectedItem(classTaken);
            }
        }
    }

    private class RemoveLevelsAction extends AbstractAction {

        private final CharacterFacade character;

        RemoveLevelsAction(CharacterFacade character) {
            this.character = character;
            //putValue(SMALL_ICON, new SignIcon(Sign.Minus));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Number levels = (Number) addLevelsField.getValue();
            character.removeCharacterLevels(levels.intValue());
        }

    }

    /**
     * Handler for actions from the add experience button. Also defines the
     * appearance of the button.
     */
    private class ExpAddAction extends AbstractAction {

        private final CharacterFacade character;

        ExpAddAction(CharacterFacade character) {
            this.character = character;
            //putValue(SMALL_ICON, new SignIcon(Sign.Plus));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Object value = expmodField.getValue();
            if (value == null) {
                return;
            }
            int modVal = ((Number) value).intValue();
            character.adjustXP(modVal);
        }

    }

    /**
     * Handler for actions from the subtract experience button. Also defines the
     * appearance of the button.
     */
    private class ExpSubtractAction extends AbstractAction {

        private final CharacterFacade character;

        ExpSubtractAction(CharacterFacade character) {
            this.character = character;
            //putValue(SMALL_ICON, new SignIcon(Sign.Minus));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Object value = expmodField.getValue();
            if (value == null) {
                return;
            }
            int modVal = ((Number) value).intValue();
            character.adjustXP(modVal * -1);
        }

    }

     /**
     * The Class {@code TodoListHandler} manages the text displayed in the
     * things to be done panel for the character. The text will be updated each
     * time the character's todo list changes. The handler also knows how to
     * react to install and uninstall actions when the displayed character
     * changes.
     */
    @SuppressWarnings("TodoComment")
    private class TodoListHandler implements ListListener<TodoFacade>, HyperlinkListener {

        private final CharacterFacade character;
        private String lastDest = "";

        /**
         * Create a new instance for the character.
         *
         * @param character The character being managed.
         */
        TodoListHandler(CharacterFacade character) {
            this.character = character;
        }

        /**
         * Attach the handler to the on-screen field. e.g. When the character is
         * made active.
         */
        public void install() {
            character.getTodoList().addListListener(this);
            todoPane.addHyperlinkListener(this);
            lastDest = "";
            refreshTodoList();
        }

        /**
         * Detach the handler from the on-screen field. e.g. when the character
         * is no longer being displayed.
         */
        public void uninstall() {
            todoPane.removeHyperlinkListener(this);
            character.getTodoList().removeListListener(this);
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementAdded(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementAdded(ListEvent<TodoFacade> e) {
            refreshTodoList();
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementRemoved(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementRemoved(ListEvent<TodoFacade> e) {
            refreshTodoList();
        }

        /**
         * @see pcgen.core.facade.event.ListListener#elementsChanged(pcgen.core.facade.event.ListEvent)
         */
        @Override
        public void elementsChanged(ListEvent<TodoFacade> e) {
            refreshTodoList();
        }

        @Override
        public void elementModified(ListEvent<TodoFacade> e) {
            refreshTodoList();
        }

        /**
         * Recreate the "Things to be Done" list based on the character's todo
         * list.
         */
        private void refreshTodoList() {
            StringBuilder todoText = new StringBuilder("<html><body>");

            int i = 1;
            Collection<TodoFacade> sortedTodos = new TreeSet<>();
            character.getTodoList().iterator().forEachRemaining(sortedTodos::add);

            for (TodoFacade item : sortedTodos) {
                todoText.append(i++).append(". ");
                String fieldLoc = item.getTab().name() + "/" + item.getFieldName();
                if (StringUtils.isNotEmpty(item.getSubTabName())) {
                    fieldLoc += "/" + item.getSubTabName();
                }
                todoText.append("<a href=\"").append(fieldLoc).append("\">"); //$NON-NLS-2$
                if (item.getMessageKey().startsWith("in_"))
                {
                    todoText.append(LanguageBundle.getFormattedString(item.getMessageKey(), item.getFieldName()));
                } else {
                    todoText.append(item.getMessageKey());
                }
                todoText.append("</a><br>");
            }
            todoText.append("</body></html>");
            todoPane.setText(todoText.toString());
        }

        @Override
        public void hyperlinkUpdate(HyperlinkEvent e) {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                // We get two messages on a click, so ignore duplicates
                if (lastDest.equals(e.getDescription())) {
                    lastDest = "";
                    return;
                }
                lastDest = e.getDescription();
                firePropertyChange(TodoFacade.SWITCH_TABS, "", e.getDescription());
            }
        }

    }

    private class SummaryTabFocusTraversalPolicy extends LayoutFocusTraversalPolicy {

        @Override
        public Component getComponentAfter(Container aContainer, Component aComponent) {
            if (aComponent == deityComboBox) {
                int column = statsTable.getColumn(StatTableModel.EDITABLE_COLUMN_ID).getModelIndex();
                statsTable.editCellAt(0, column);
                JSpinner spinner = (JSpinner) statsTable.getEditorComponent();
                return ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
            }
            return super.getComponentAfter(aContainer, aComponent);
        }

        @Override
        public Component getComponentBefore(Container aContainer, Component aComponent) {
//			if (aComponent == generateRollsButton)
//			{
//				int column = statsTable.getColumn("EDITABLE").getModelIndex();
//				statsTable.editCellAt(statsTable.getRowCount()-1, column);
//				JSpinner spinner = (JSpinner) statsTable.getEditorComponent();
//				return ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
//			}
            return super.getComponentBefore(aContainer, aComponent);
        }

    }

}
