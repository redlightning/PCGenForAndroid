package pcgen.gui2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import pcgen.gui2.tabs.CharacterSheetInfoTab;

public class CharacterPagerAdapter extends FragmentStatePagerAdapter {
    public CharacterPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new CharacterSheetInfoTab();
        Bundle args = new Bundle();
        args.putInt("character", i + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }
}
