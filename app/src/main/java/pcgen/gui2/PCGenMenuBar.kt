/*
 * Copyright 2008 Connor Petty <cpmeister@users.sourceforge.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.gui2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import net.redlightning.pcgen.MainActivity

import net.redlightning.pcgen.R
import pcgen.cdom.content.Sponsor
import pcgen.core.Globals

import pcgen.facade.core.CharacterFacade
import pcgen.gui2.coreview.CoreViewFrame
import pcgen.gui2.dialog.*
import pcgen.gui2.solverview.SolverViewFrame
import pcgen.gui2.tools.CharacterSelectionListener
import pcgen.system.CharacterManager
import pcgen.system.ConfigurationSettings
import timber.log.Timber

/**
 * The menu bar that is displayed in PCGen's main activity.
 */
class PCGenMenuBar(pcGenFrame: MainActivity) : CharacterSelectionListener {
    private val frame: MainActivity = pcGenFrame
    private var character: CharacterFacade? = null

    fun handleMenuItemSelected(context: Context, menuItem: MenuItem): Boolean {
        return when (menuItem.itemId)
        {
            R.id.main_mnu_new -> {
                frame.createNewCharacter()
                true
            }
            R.id.main_mnu_open -> {
                frame.showOpenCharacterChooser()
                true
            }
            R.id.main_mnu_close -> {
                frame.closeCharacter(frame.selectedCharacterRef!!)
                true
            }
            R.id.main_mnu_close_all -> {
                frame.closeAllCharacters()
                true
            }
            R.id.main_mnu_save -> {
                val pc = frame.selectedCharacterRef!!
                frame.saveCharacter(pc)
                true
            }
            R.id.main_mnu_save_as -> {
                frame.showSaveCharacterChooser(frame.selectedCharacterRef!!)
                true
            }
            R.id.main_mnu_save_all -> {
                frame.saveAllCharacters()
                true
            }
            R.id.main_mnu_revert -> {
                frame.revertCharacter(frame.selectedCharacterRef!!)
                true
            }
            R.id.main_mnu_open_party -> {
                frame.showOpenPartyChooser()
                true
            }
            R.id.main_mnu_close_party -> {
                frame.closeAllCharacters()
                true
            }
            R.id.main_mnu_save_party -> {
                if (frame.saveAllCharacters() && !CharacterManager.saveCurrentParty()) {
                    frame.showSavePartyChooser()
                }
                true
            }
            R.id.main_mnu_save_party_as -> {
                frame.showSavePartyChooser()
                true
            }
            R.id.main_mnu_print -> {
                PrintPreviewDialog.showPrintPreviewDialog(frame)
                true
            }
            R.id.main_mnu_export -> {
                ExportDialog.showExportDialog(frame)
                true
            }
            R.id.main_mnu_exit -> {
                if (!frame.closeAllCharacters()) {
			        false;
		        } else {
                    frame.finish()
                    true
                }
            }
            R.id.main_mnu_add_kit -> {
                KitSelectionDialog(context, frame.selectedCharacterRef!!).show()
                true
            }
            R.id.main_mnu_equip_set -> { true }
            R.id.main_mnu_temp_bomus -> { true }
            R.id.main_mnu_select_sources -> {
                frame.showSourceSelectionDialog()
                true
            }
            R.id.main_mnu_reload_sources -> {
                val sources = frame.currentSourceSelection!!.get()
                if (sources != null) {
                    frame.unloadSources()
                    frame.loadSourceSelection(sources)
                }
                true
            }
            R.id.main_mnu_unload_sources -> {
                frame.unloadSources()
                true
            }
            R.id.main_mnu_install_data -> {
                Timber.e("DATA INSTALLER TODO")
                val installer: DataInstaller = DataInstaller(context)
                //TODO Show data installer fragment
                true
            }
            R.id.main_mnu_preferences -> {
                val dialog = PreferencesActivity()
                //TODO Show PreferencesActivity
                true
            }
//            R.id.main_mnu_gmgen -> {
//                PCGenUIManager.displayGmGen()
//                true
//            }
            R.id.main_mnu_calculator -> {
                CalculatorDialog(frame).show()
                true
            }
            R.id.main_mnu_view_character -> {
                val cf = frame.selectedCharacterRef!!
                CoreViewFrame(frame.characterTabs!!.view, cf).show()
                true
            }
            R.id.main_mnu_solver -> {
                //TODO Show SolverViewFrame
                SolverViewFrame(context)
                true
            }
            R.id.main_mnu_documentation -> {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse("file://" + ConfigurationSettings.getDocsDir() + "index.html")
                startActivity(context, openURL, null)
                true
            }
            R.id.main_mnu_ogl -> {
                frame.showOGLDialog()
                true
            }
            R.id.main_mnu_sponsors -> {
                val sponsors = Globals.getGlobalContext().referenceContext.getConstructedCDOMObjects(Sponsor::class.java)
                if (sponsors.size > 1) {
                    frame.showSponsorsDialog()
                }
                Toast.makeText(context, "There are no sponsors", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.main_mnu_about -> {
                frame.showAboutDialog()
                true
            }
            else -> false
        }
    }

    override fun setCharacter(character: CharacterFacade) {
        this.character = character
        //equipmentMenu.setListModel(character.equipmentSets)
        //tempMenu.setListModel(character.availableTempBonuses)
    }
}
