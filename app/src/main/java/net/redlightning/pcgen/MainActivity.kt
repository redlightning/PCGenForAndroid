package net.redlightning.pcgen

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.text.Html
import android.view.*
import android.webkit.WebView
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.switchmaterial.SwitchMaterial
import org.apache.commons.lang3.StringUtils
import pcgen.cdom.base.Constants
import pcgen.cdom.content.Sponsor
import pcgen.cdom.formula.PluginFunctionLibrary
import pcgen.core.CustomData
import pcgen.core.Globals
import pcgen.core.prereq.PrerequisiteTestFactory
import pcgen.core.utils.ShowMessageDelegate
import pcgen.facade.core.*
import pcgen.facade.util.DefaultReferenceFacade
import pcgen.facade.util.event.ReferenceEvent
import pcgen.facade.util.event.ReferenceListener
import pcgen.gui2.CharacterTabs
import pcgen.gui2.PCGenMenuBar
import pcgen.gui2.PCGenStatusBar
import pcgen.gui2.UIPropertyContext
import pcgen.gui2.dialog.*
import pcgen.gui2.dialog.about.CreditsFragment
import pcgen.gui2.sources.SourceSelectionDialog
import pcgen.gui2.util.ShowMessageGuiObserver
import pcgen.gui2.util.SwingWorker
import pcgen.io.ExportHandler
import pcgen.io.PCGFile
import pcgen.persistence.CampaignFileLoader
import pcgen.persistence.GameModeFileLoader
import pcgen.persistence.PersistenceLayerException
import pcgen.persistence.SourceFileLoader
import pcgen.persistence.lst.TokenStore
import pcgen.persistence.lst.output.prereq.PrerequisiteWriterFactory
import pcgen.persistence.lst.prereq.PreParserFactory
import pcgen.pluginmgr.PluginManager
import pcgen.rules.persistence.TokenLibrary
import pcgen.system.*
import pcgen.util.PJEP
import pcgen.util.chooser.ChooserFactory
import timber.log.Timber
import java.io.*
import java.lang.reflect.InvocationTargetException
import java.util.*
import java.util.logging.LogRecord

abstract class MainActivity : UIDelegate, AppCompatActivity() {
    private var selectedCharacterRef: CharacterFacade? = null
    var characterTabs: CharacterTabs? = null
    private var statusBar: PCGenStatusBar? = null
    var currentSourceSelection: DefaultReferenceFacade<SourceSelectionFacade>? = null
    @kotlin.jvm.JvmField
    var currentCharacterRef: DefaultReferenceFacade<CharacterFacade>? = null
    private var currentDataSetRef: DefaultReferenceFacade<DataSetFacade>? = null
    private var filenameListener: FilenameListener? = null
    private var sourceSelectionDialog: Dialog? = null
    private var sourceLoader: SourceLoadWorker? = null
    private var section15: String? = null
    private var lastCharacterPath: String? = null
    private val configFactory: PropertyContextFactory? = null

    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
        fun getAppContext(): Context? {
            return context
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = applicationContext
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        //Start of PC Gen
        Timber.i("Starting PCGen v%s", getString(R.string.pcgen_code_verson))

        Thread.setDefaultUncaughtExceptionHandler(PCGenUncaughtExceptionHandler())

        val executor = PCGenTaskExecutor()
        executor.addPCGenTask(createLoadPluginTask())
        executor.addPCGenTask(GameModeFileLoader())
        executor.addPCGenTask(CampaignFileLoader())
        FacadeFactory.initialize()

        loadProperties()

//        val pluginMgr = PluginManager.getInstance()
//        val handler = GMGenMessageHandler(this, pluginMgr.postbox)
//        pluginMgr.addMember(handler)

        Globals.setRootFrame(this)
        this.currentSourceSelection = DefaultReferenceFacade()
        this.currentCharacterRef = DefaultReferenceFacade()
        this.currentDataSetRef = DefaultReferenceFacade()
        this.characterTabs = CharacterTabs(this)
        this.statusBar = PCGenStatusBar(this)
        this.filenameListener = FilenameListener()
        val messageObserver = ShowMessageGuiObserver(this)
        ShowMessageDelegate.getInstance().addObserver(messageObserver)
        ChooserFactory.setDelegate(this)

        statusBar = findViewById(R.id.statusbar)
        characterTabs = findViewById(R.id.character_pager)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = MenuInflater(context)
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //Hand off to menu item handling class
        val menuBar = PCGenMenuBar(this)
        return menuBar.handleMenuItemSelected(this, item)
    }

    fun loadProperties() {
        PropertyContextFactory.setDefaultFactory(this)
        //Existing PropertyContexts are registered here
        val defaultFactory = PropertyContextFactory.getDefaultFactory()
        defaultFactory.registerPropertyContext(PCGenSettings.getInstance())
        defaultFactory.registerPropertyContext(UIPropertyContext.getInstance())
        //defaultFactory.registerPropertyContext(LegacySettings.getInstance());
        defaultFactory.loadPropertyContexts()
    }

    /**
     * Create a task to load all system plugins.
     *
     * @return The task to load plugins.
     */
    fun createLoadPluginTask(): PCGenTask {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val pluginsDir = sharedPref.getString(PCGenSettings.PLUGINS_DIR, Environment.getExternalStorageDirectory().absolutePath + "/plugins")
        val loader = PluginClassLoader(File(pluginsDir))
        loader.addPluginLoader(TokenLibrary.getInstance())
        loader.addPluginLoader(TokenStore.inst())
        try {
            loader.addPluginLoader(PreParserFactory.getInstance())
        } catch (ex: PersistenceLayerException) {
            Timber.e(ex, "createLoadPluginTask failed")
        }

        loader.addPluginLoader(PrerequisiteTestFactory.getInstance())
        loader.addPluginLoader(PrerequisiteWriterFactory.getInstance())
        loader.addPluginLoader(PJEP.getJepPluginLoader())
        loader.addPluginLoader(ExportHandler.getPluginLoader())
        loader.addPluginLoader(PluginManager.getInstance())
        loader.addPluginLoader(PluginFunctionLibrary.getInstance())
        return loader
    }

    override fun onStop() {
        // Need to (possibly) write customEquipment.lst
        configFactory!!.savePropertyContexts()
        BatchExporter.removeTemporaryFiles()
        PropertyContextFactory.getDefaultFactory().savePropertyContexts()

        if (PCGenSettings.OPTIONS_CONTEXT.getBoolean(PCGenSettings.OPTION_SAVE_CUSTOM_EQUIPMENT)) {
            CustomData.writeCustomItems()
        }
        super.onStop()
    }

    /**
     * The Class `PCGenUncaughtExceptionHandler` reports any
     * exceptions that are not otherwise handled by the program.
     */
    private class PCGenUncaughtExceptionHandler : Thread.UncaughtExceptionHandler {
        override fun uncaughtException(t: Thread, e: Throwable) {
            Timber.e(e, "Uncaught error - ignoring")
        }
    }

    fun setSelectedCharacter(character: CharacterFacade?) {
        if (currentCharacterRef?.get() != null) {
            currentCharacterRef!!.get().fileRef.removeReferenceListener(filenameListener)
        }
        currentCharacterRef!!.set(character)
        updateTitle()
        if (character != null && character.fileRef != null) {
            character.fileRef.addReferenceListener(filenameListener)
        }
    }

    /**
     * @return A reference to the currently loaded sources.
     */
    fun getCurrentSourceSelectionRef(): DefaultReferenceFacade<SourceSelectionFacade>? {
        return currentSourceSelection
    }

    /**
     *
     * @return a reference to the selected character
     */
    fun getSelectedCharacterRef(): CharacterFacade? {
        return selectedCharacterRef
    }

    /**
     *
     * @return a reference to the currently loaded data set
     */
    fun getLoadedDataSetRef(): DefaultReferenceFacade<DataSetFacade>? {
        return currentDataSetRef
    }

    /**
     * @return the status bar for the main PCGen frame
     */
    fun getStatusBar(): PCGenStatusBar? {
        return statusBar
    }

    /**
     * Unload any currently loaded sources.
     */
    fun unloadSources() {
        if (closeAllCharacters()) {
            currentSourceSelection!!.set(null)
            currentDataSetRef!!.set(null)
            Globals.emptyLists()
            updateTitle()
        }
    }

    /**
     * Loads a selection of sources into PCGen asynchronously and
     * tracks the load progress on the status bar. While sources
     * are being loaded any calls to this method are ignored until
     * sources are finished loading.
     * @param sources a SourceSelectionFacade specifying the sources to load
     * @return true if the sources are loaded or are loading
     */
    fun loadSourceSelection(sources: SourceSelectionFacade?): Boolean {
        if (sources == null) {
            return false
        }
        if (sourceLoader != null && sourceLoader!!.isAlive) {
            return checkSourceEquality(sources, sourceLoader!!.sources)
        }
        if (checkSourceEquality(sources, currentSourceSelection!!.get())) {
            return true
        }
        //make sure all characters are closed before loading new sources.
        if (closeAllCharacters()) {
            sourceLoader = SourceLoadWorker(sources, this)
            sourceLoader!!.start()
            return true
        }
        return false
    }

    private fun checkSourceEquality(source1: SourceSelectionFacade?, source2: SourceSelectionFacade?): Boolean {
        if (source1 === source2) {
            return true
        }
        if ((source1 == null) xor (source2 == null)) {
            return false
        }
        //we use reference equality since GameModeFacades come from a fixed database
        if (source1!!.gameMode.get() !== source2!!.gameMode.get()) {
            return false
        }
        val campaigns1 = source1!!.campaigns
        val campaigns2 = source2!!.campaigns
        if (campaigns1.size != campaigns2.size) {
            return false
        }
        for (campaignFacade in campaigns1) {
            if (!campaigns2.containsElement(campaignFacade)) {
                return false
            }
        }
        return true
    }

    private fun checkGameModeEquality(source1: SourceSelectionFacade?, source2: SourceSelectionFacade?): Boolean {
        if (source1 === source2) {
            return true
        }
        return if ((source1 == null) xor (source2 == null)) {
            false
        } else source1!!.gameMode.get() === source2!!.gameMode.get()
        //we use reference equality since GameModeFacades come from a fixed database
    }

    fun saveCharacter(character: CharacterFacade): Boolean {
        if (!CharacterManager.characterFilenameValid(character)) {
            return showSaveCharacterChooser(character)
        }
        // We must have a file name before we prepare.
        prepareForSave(character, false)
        return if (!reallySaveCharacter(character)) {
            showSaveCharacterChooser(character)
        } else true
    }

    /**
     * Wraps the CharacterManager with GUI progress updates
     * @param character current character
     * @return value from CharacterManager.saveCharacter()
     */
    fun reallySaveCharacter(character: CharacterFacade): Boolean {
        var result = false

        // KAW TODO externalize and NLS the msg
        val msg = "Saving character..."
        statusBar!!.startShowingProgress(msg, true)
        try {
            result = CharacterManager.saveCharacter(character)
        } catch (e: Exception) {
            Timber.e(e, e.localizedMessage)
        } finally {
            statusBar!!.endShowingProgress()
        }
        return result
    }

    /**
     * Prepare the character for a save. This is primarily concerned with
     * ensuring all companions (or masters) have file names before the save is
     * done.
     * @param character The character being saved.
     */
    private fun prepareForSave(character: CharacterFacade, savingAll: Boolean) {
        val tobeSaved = ArrayList<CompanionFacade>()
        for (comp in character.companionSupport.companions) {
            if (StringUtils.isEmpty(comp.fileRef.get().name) && CharacterManager.getCharacterMatching(comp) != null) {
                tobeSaved.add(comp)
            }
        }
        if (tobeSaved.isNotEmpty()) {
            if (savingAll || showMessageConfirm(Constants.APPLICATION_NAME,
                            LanguageBundle.getString(R.string.in_unsavedCompanions))) {
                for (companionFacade in tobeSaved) {
                    val compChar = CharacterManager.getCharacterMatching(companionFacade)
                    showSaveCharacterChooser(compChar)
                }
            }
        }
        val master = character.master
        if (master != null && (master.fileRef.get() == null || StringUtils
                        .isEmpty(master.fileRef.get().name))) {
            if (savingAll || showMessageConfirm(Constants.APPLICATION_NAME,
                            LanguageBundle.getString(R.string.in_unsavedMaster))) {
                val masterChar = CharacterManager.getCharacterMatching(master)
                showSaveCharacterChooser(masterChar)
            }
        }
    }


    fun closeCharacter(character: CharacterFacade) {
        if (character.isDirty) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Save?")
            builder.setMessage("Save changes to character?")
            builder.setPositiveButton("Yes") { _: DialogInterface, _: Int -> saveCharacter(character) }
            builder.setNegativeButton("No") { dialogInterface: DialogInterface, _: Int -> dialogInterface.dismiss() }
            builder.create().show()
        }
        CharacterManager.removeCharacter(character)
    }

    fun closeAllCharacters(): Boolean {
        val CLOSE_OPT_CHOOSE = 2
        val characters = CharacterManager.getCharacters()
        if (characters.isEmpty) {
            return true
        }
        val saveAllChoice = CLOSE_OPT_CHOOSE
        val characterList = ArrayList<CharacterFacade>()
        val unsavedPCs = ArrayList<CharacterFacade>()
        for (characterFacade in characters) {
            if (characterFacade.isDirty) {
                unsavedPCs.add(characterFacade)
            } else {
                characterList.add(characterFacade)
            }
        }
        if (unsavedPCs.size > 1) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Save?")
            builder.setMessage("Save all characters?")
            builder.setPositiveButton("All") { dialog: DialogInterface, _: Int ->
                saveAllCharacters()
                dialog.dismiss()
            }
            builder.setNegativeButton("None") { dialog: DialogInterface, _: Int ->
                CharacterManager.removeAllCharacters()
                dialog.dismiss()
            }
            builder.setNeutralButton("Cancel") { dialogInterface: DialogInterface, _: Int -> dialogInterface.dismiss() }
            builder.create().show()
        }

        return characters.isEmpty
    }

    internal fun showSavePartyChooser(): Boolean {
        val context = PCGenSettings.getInstance()
        val parentPath = context.getProperty(PCGenSettings.PCP_SAVE_PATH)
        var returnVal = false
        com.obsez.android.lib.filechooser.ChooserDialog(this.applicationContext)
                .withStartFile(parentPath)
                .withFilter(false, "pcp")
                .withChosenListener { _: String?, pathFile: File? ->
                    returnVal = saveParty(pathFile)
                }
                .withOnCancelListener { obj: DialogInterface -> obj.cancel() }
                .build()
                .show()
        return returnVal
    }

    private fun saveParty(partyFile: File?): Boolean {
        val party = CharacterManager.getCharacters()
        var file: File? = partyFile
        if (!file!!.name.endsWith(Constants.EXTENSION_PARTY_FILE)) {
            file = File(file.parent, file.name + Constants.EXTENSION_PARTY_FILE)
        }
        if (file.isDirectory) {
            showErrorMessage(Constants.APPLICATION_NAME,
                    LanguageBundle.getString(R.string.in_savePcDirOverwrite))
            return showSavePartyChooser()
        }
        if (file.exists()) {
            val overwrite = showWarningConfirm(LanguageBundle.getFormattedString(
                    R.string.in_savePcConfirmOverTitle,
                    file.name),
                    LanguageBundle.getFormattedString(
                            R.string.in_savePcConfirmOverMsg,
                            file.name))
            if (!overwrite) {
                return showSavePartyChooser()
            }
        }
        party.setFile(file)
        //context.setProperty(PCGenSettings.PCP_SAVE_PATH, file.parent)
        if (!saveAllCharacters()) {
            showErrorMessage(LanguageBundle.getString(R.string.in_savePartyFailTitle),
                    LanguageBundle.getString(R.string.in_savePartyFailMsg))
            return false
        }
        return if (!CharacterManager.saveCurrentParty()) {
            showSavePartyChooser()
        } else true
    }

    internal fun saveAllCharacters(): Boolean {
        var ok = true
        for (character in CharacterManager.getCharacters()) {
            val file = character.fileRef.get()
            if (file == null || StringUtils.isEmpty(file!!.name)) {
                ok = ok and showSaveCharacterChooser(character)
            } else {
                prepareForSave(character, true)
                ok = ok and reallySaveCharacter(character)
            }
        }
        return ok
    }

    /**
     * This brings up a file chooser allows the user to select
     * the location that a character should be saved to.
     * @param character the character to be saved
     */
    internal fun showSaveCharacterChooser(character: CharacterFacade?): Boolean {
        var returnVal = false
        val context = PCGenSettings.getInstance()
        var parentPath: String? = lastCharacterPath
        if (parentPath == null) {
            parentPath = context.getProperty(PCGenSettings.PCG_SAVE_PATH)
        }

        com.obsez.android.lib.filechooser.ChooserDialog(this.applicationContext)
                .withStartFile(parentPath)
                .withFilter(false, "pcg")
                .withChosenListener { _: String?, pathFile: File? ->
                    returnVal = saveCharacter(character!!, pathFile)
                }
                .withOnCancelListener { obj: DialogInterface -> obj.cancel() }
                .build()
                .show()

        return returnVal
    }

    private fun saveCharacter(character: CharacterFacade, character_file: File?): Boolean {
        var file = character_file
        var returnVal = false
        if (!file!!.name.endsWith(Constants.EXTENSION_CHARACTER_FILE)) {
            file = File(file.parent, file.name + Constants.EXTENSION_CHARACTER_FILE)
        }

        val delegate = character.uiDelegate
        if (file.isDirectory) {
            delegate.showErrorMessage(Constants.APPLICATION_NAME,
                    LanguageBundle.getString(R.string.in_savePcDirOverwrite))
            returnVal = showSaveCharacterChooser(character)
        }

        try {
            character.setFile(file)
            prepareForSave(character, false)
            if (!reallySaveCharacter(character)) {
                showSaveCharacterChooser(character)
            }

            //lastCharacterPath = chooser.getCurrentDirectory().toString()
            returnVal = true
        } catch (e: Exception) {
            Timber.e("Error saving character to new file %s", file)
            delegate.showErrorMessage(
                    Constants.APPLICATION_NAME,
                    LanguageBundle.getFormattedString(
                            R.string.in_saveFailMsg, file.name))
        }
        return returnVal
    }

    /**
     * Revert the character to the previous save. If no previous save, open a
     * new character tab.
     * @param character The character being saved.
     */
    fun revertCharacter(character: CharacterFacade) {
        if (character.isDirty) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Save?")
            builder.setMessage("Save changes to character?")
            builder.setPositiveButton("Yes") { dialogInterface: DialogInterface, _: Int ->
                CharacterManager.removeCharacter(character)

                if ((character.fileRef.get() != null && character.fileRef.get().exists())) {
                    openCharacter(character.fileRef.get(), currentDataSetRef!!.get())
                } else {
                    createNewCharacter()
                }
                dialogInterface.dismiss()
            }
            builder.setNegativeButton("No") { dialogInterface: DialogInterface, _: Int -> dialogInterface.dismiss() }
            builder.create().show()
        }
    }

    internal fun showOpenCharacterChooser() {
        val context = PCGenSettings.getInstance()
        var path: String? = lastCharacterPath
        if (path == null) {
            path = context.getProperty(PCGenSettings.PCG_SAVE_PATH)
        }
        com.obsez.android.lib.filechooser.ChooserDialog(this.applicationContext)
                .withStartFile(path)
                .withFilter(false, "pcg")
                .withChosenListener { path: String?, pathFile: File? ->
                    if (pathFile != null) {
                        loadCharacterFromFile(pathFile)
                    }
                    lastCharacterPath = path
                }
                .withOnCancelListener { obj: DialogInterface -> obj.cancel() }
                .build()
                .show()
    }

    internal fun showOpenPartyChooser() {
        val context = PCGenSettings.getInstance()
        com.obsez.android.lib.filechooser.ChooserDialog(this.applicationContext)
                .withStartFile(context.getProperty(PCGenSettings.PCP_SAVE_PATH))
                .withFilter(false, "pcp")
                .withChosenListener { _: String?, pathFile: File? ->
                    if (pathFile != null) {
                        loadPartyFromFile(pathFile)
                    }
                }
                .withOnCancelListener { obj: DialogInterface -> obj.cancel() }
                .build()
                .show()
    }

    fun createNewCharacter() {
        createNewCharacter(null)
    }

    /**
     * creates a new character and sets its file if possible
     * then sets the character as the currently selected character
     * @param file the File for this character
     */
    protected fun createNewCharacter(file: File?) {
        val data = getLoadedDataSetRef()!!.get()
        val character = CharacterManager.createNewCharacter(this, data)
        //This is called before the we set it as the selected character so
        //the InfoTabbedPane can catch any character specific properties when
        //it is first displayed
        if (file != null) {
            character!!.setFile(file)
        }
        //Because CharacterManager adds the new character to the character
        //list before it returns, it is not necessary to update the character
        //tabs since they will catch that event before the call to
        //setSelectedCharacter is called
        setSelectedCharacter(character)
    }

    /**
     * Loads a character from a file. Any sources that are required for
     * this character are loaded first, then the character is loaded
     * from the file and a tab is opened for it.
     * @param pcgFile a file specifying the character to be loaded
     */
    fun loadCharacterFromFile(pcgFile: File) {
        if (!PCGFile.isPCGenCharacterFile(pcgFile)) {
            Toast.makeText(this,
                    LanguageBundle.getFormattedString(R.string.in_loadPcInvalid, pcgFile), Toast.LENGTH_LONG).show()
            return
        }
        if (!pcgFile.canRead()) {
            Toast.makeText(this,
                    LanguageBundle.getFormattedString(R.string.in_loadPcNoRead, pcgFile), Toast.LENGTH_LONG).show()
            return
        }

        val sources = CharacterManager.getRequiredSourcesForCharacter(pcgFile, this)
        if (sources == null) {
            Toast.makeText(this, LanguageBundle.getFormattedString(R.string.in_loadPcNoSources, pcgFile), Toast.LENGTH_LONG).show()
        } else if (!sources.campaigns.isEmpty) {
            // Check if the user has asked that sources not be loaded with the character
            val dontLoadSources = currentSourceSelection!!.get() != null && !PCGenSettings.OPTIONS_CONTEXT
                    .initBoolean(PCGenSettings.OPTION_AUTOLOAD_SOURCES_WITH_PC, true)
            val sourcesSame = checkSourceEquality(sources, currentSourceSelection!!.get())
            val gameModesSame = checkGameModeEquality(sources, currentSourceSelection!!.get())
            if (!dontLoadSources && !sourcesSame && gameModesSame) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.in_loadPcDiffSourcesLoaded)
                builder.setNegativeButton(R.string.in_cancel) { dialog, _ -> dialog.dismiss() }
                builder.setPositiveButton(R.string.in_loadPcDiffSourcesCharacter) { _, _ -> openCharacter(pcgFile, currentDataSetRef!!.get()) }
                builder.setMessage(
                        LanguageBundle.getFormattedString(R.string.in_loadPcDiffSources,
                                getFormattedCampaigns(currentSourceSelection!!.get()), getFormattedCampaigns(sources)),
                )
            }

            if (dontLoadSources) {
                if (!checkSourceEquality(sources, currentSourceSelection!!.get())) {
                    Timber.w("Loading character with different sources. Character: %s current: %s", sources, currentSourceSelection!!.get())
                }
                openCharacter(pcgFile, currentDataSetRef!!.get())
            } else if (loadSourceSelection(sources)) {
                loadSourcesThenCharacter(pcgFile)
            } else {
                Toast.makeText(this,
                        LanguageBundle.getString(R.string.in_loadPcIncompatSource), Toast.LENGTH_LONG).show()
            }
        } else if (currentDataSetRef!!.get() != null) {
            if (showWarningConfirm(Constants.APPLICATION_NAME, LanguageBundle.getFormattedString(R.string.in_loadPcSourcesLoadQuery, pcgFile))) {
                openCharacter(pcgFile, currentDataSetRef!!.get())
            }
        } else {
            Toast.makeText(this, LanguageBundle.getFormattedString(R.string.in_loadPcNoSources, pcgFile), Toast.LENGTH_LONG).show()
        }
    }

    /**
     * @param pcgFile character's pcgFile
     * @param reference set reference
     */
    protected fun openCharacter(pcgFile: File, reference: DataSetFacade) {
        val msg = LanguageBundle.getFormattedString(R.string.in_loadPcLoadingFile, pcgFile.name)
        statusBar!!.startShowingProgress(msg, false)
        statusBar!!.progressBar.max = 2
        statusBar!!.progressBar.min = 0
        statusBar!!.progressBar.progress = 0

        this.runOnUiThread {
            try {
                CharacterManager.openCharacter(pcgFile, this, reference)
                statusBar!!.progressBar.max = 2
                statusBar!!.progressBar.min = 0
                statusBar!!.progressBar.progress = 1
            } catch (e: Exception) {
                Timber.e(e, "Error loading character: " + pcgFile.name)
            } finally {
                statusBar!!.endShowingProgress()
            }
        }
    }

    private fun getFormattedCampaigns(sources: SourceSelectionFacade): String {
        val campList = StringBuilder(100)
        campList.append("<UL>")
        var count = 1
        val maxListLen = 6
        for (facade in sources.campaigns) {
            campList.append("<li>")
            if (count >= maxListLen && sources.campaigns.size > maxListLen) {
                val numExtra = sources.campaigns.size - maxListLen + 1
                campList.append(LanguageBundle.getFormattedString(R.string.in_loadPcDiffSourcesExcessSources, (numExtra).toString()))
                break
            }
            campList.append(facade.toString())
            campList.append("</li>")
            count++
        }
        campList.append("</UL>")
        return campList.toString()
    }

    /**
     * Asynchronously load the sources required for a character and then load the character.
     * @param pcgFile The character to be loaded.
     */
    private fun loadSourcesThenCharacter(pcgFile: File) {
        try {
            sourceLoader!!.join()
            this.runOnUiThread {
                val msg = LanguageBundle.getFormattedString(R.string.in_loadPcLoadingFile, pcgFile.name)
                statusBar!!.startShowingProgress(msg, false)
                statusBar!!.progressBar.max = 2
                statusBar!!.progressBar.min = 0
                statusBar!!.progressBar.progress = 0
            }
            this.runOnUiThread {
                try {
                    CharacterManager.openCharacter(pcgFile, this@MainActivity, currentDataSetRef!!.get())
                    statusBar!!.progressBar.max = 2
                    statusBar!!.progressBar.min = 0
                    statusBar!!.progressBar.progress = 1
                } catch (e: Exception) {
                    Timber.e("Error loading character: %s", pcgFile.name)
                } finally {
                    statusBar!!.endShowingProgress()
                }
            }
        } catch (ex: InterruptedException) {
            //Do nothing
        } catch (e1: InvocationTargetException) {
            Timber.e(e1, "Error showing progress bar.")
        }
    }

    fun loadPartyFromFile(pcpFile: File) {
        if (!PCGFile.isPCGenPartyFile(pcpFile)) {
            Toast.makeText(this, LanguageBundle
                    .getFormattedString(R.string.in_loadPartyInvalid, pcpFile),
                    Toast.LENGTH_LONG).show()
            return
        }
        if (!pcpFile.canRead()) {
            Toast.makeText(this, LanguageBundle
                    .getFormattedString(R.string.in_loadPartyNoRead, pcpFile),
                    Toast.LENGTH_LONG).show()
            return
        }
        val sources = CharacterManager.getRequiredSourcesForParty(pcpFile, this)
        if (sources == null) {
            Toast.makeText(this, LanguageBundle
                    .getFormattedString(R.string.in_loadPartyNoSources, pcpFile),
                    Toast.LENGTH_LONG).show()
        } else if (!sources!!.campaigns.isEmpty) {
            // Check if the user has asked that sources not be loaded with the character
            val dontLoadSources = (currentSourceSelection!!.get() != null && !PCGenSettings.OPTIONS_CONTEXT
                    .initBoolean(
                            PCGenSettings.OPTION_AUTOLOAD_SOURCES_WITH_PC,
                            true))
            if (dontLoadSources) {
                if (!checkSourceEquality(sources, currentSourceSelection!!.get())) {
                    Timber.w("Loading party with different sources. Party: %s current: %s", sources, currentSourceSelection!!.get())
                }
                CharacterManager.openParty(pcpFile, this@MainActivity,
                        currentDataSetRef!!.get())
            } else if (loadSourceSelection(sources)) {

                try {
                    sourceLoader!!.join()
                    this.runOnUiThread(Runnable { CharacterManager.openParty(pcpFile, this@MainActivity, currentDataSetRef!!.get()) })
                } catch (ex: InterruptedException) {
                    //Do nothing
                }

            } else {
                Toast.makeText(this, R.string.in_loadPcIncompatSource, Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Set the frame's title based on the current character and source/game mode.
     */
    private fun updateTitle() {
        val title = StringBuilder(100)
        var characterFile: File?
        var characterFileName: String? = null
        var sourceName: String? = null
        if (currentCharacterRef != null && currentCharacterRef!!.get() != null) {
            characterFile = currentCharacterRef!!.get().fileRef.get()
            if (characterFile == null || StringUtils.isEmpty(characterFile.name)) {
                characterFileName = LanguageBundle.getString(R.string.in_unsaved_char)
            } else {
                characterFileName = characterFile.name
            }
        }
        if (currentSourceSelection!!.get() != null) {
            sourceName = currentSourceSelection?.get().toString()
        }

        if (characterFileName != null && characterFileName.isNotEmpty()) {
            title.append(characterFileName)
            title.append(" - ")
        }
        if (sourceName != null && sourceName.isNotEmpty()) {
            title.append(sourceName)
            title.append(" - ")
        }
        title.append("PCGen v")
        title.append(PCGenPropBundle.getVersionNumber())
        setTitle(title.toString())
    }

    /**
     * display the source selection dialog to the user
     */
    fun showSourceSelectionDialog() {
        if (sourceSelectionDialog == null) {
            sourceSelectionDialog = SourceSelectionDialog(this)
        }
        sourceSelectionDialog!!.show()
    }

//    //TODO: This should be in a utility class.
//    /**
//     * Builds a JPanel containing the supplied message, split at each new
//     * line and an optional checkbox, suitable for use in a showMessageDialog
//     * call. This is generally useful for showing messges which can turned
//     * off either in preferences or when they are displayed.
//     *
//     * @param message The message to be displayed.
//     * @param checkbox The optional checkbox to be added - may be null.
//     * @return JPanel A panel containing the message and the checkbox.
//     */
//    fun buildMessageLabelPanel(message: String, checkbox: JCheckBox?): JPanel {
//        val panel = JPanel()
//        val label: JLabel
//        val part: String
//
//        panel.setLayout(GridBagLayout())
//
//        val cons = GridBagConstraints()
//        cons.gridy = 0
//        cons.gridx = cons.gridy
//        cons.gridwidth = GridBagConstraints.REMAINDER
//        cons.gridheight = 1
//        cons.anchor = GridBagConstraints.WEST
//        cons.insets = Insets(0, 0, 3, 0)
//        cons.weightx = 1
//        cons.weighty = 0
//        cons.fill = GridBagConstraints.NONE
//
//        var start = 0
//        var sepPos = -1
//
//        do {
//            sepPos = message.indexOf("\n", start)
//
//            if (sepPos >= 0) {
//                part = message.substring(start, sepPos)
//                start = sepPos + 1
//            } else {
//                part = message.substring(start)
//                start = -1
//            }
//
//            label = JLabel(part, SwingConstants.LEADING)
//
//            panel.add(label, cons)
//            cons.gridy++
//        } while (start >= 0)
//
//        if (checkbox != null) {
//            label = JLabel("", SwingConstants.LEADING)
//            panel.add(label, cons)
//            cons.gridy++
//            panel.add(checkbox, cons)
//            cons.gridy++
//        }
//
//        return panel
//    }

    override fun maybeShowWarningConfirm(title: String, message: String, checkBoxText: String, context: PropertyContext, contextProp: String): Boolean? {
        if (!context.getBoolean(contextProp, true)) {
            return null
        }
        var returnVal = false
        val inflater = LayoutInflater.from(this)
        val layout = inflater.inflate(R.layout.dialog_warning_confirm, null)

        val checkBox = layout.findViewById<CheckBox>(R.id.warning_checkbox)
        checkBox.text = checkBoxText
        checkBox.setOnCheckedChangeListener { _, _ -> context.setBoolean(contextProp, checkBox.isSelected) }

        val messageView = layout.findViewById<TextView>(R.id.warning_message)
        messageView.text = message
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
                .setView(layout)
                .setPositiveButton(R.string.in_yes) { dialog, _ ->
                    returnVal = true
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.in_no) { dialog, _ ->
                    returnVal = false
                    dialog.dismiss()
                }
                .show()

        return returnVal
    }

    override fun showWarningConfirm(title: String, message: String): Boolean {
        var returnVal = false;
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.in_yes) { dialog, _ ->
                    returnVal = true
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.in_no) { dialog, _ ->
                    returnVal = false
                    dialog.dismiss()
                }
                .show()

        return returnVal
    }

//	/**
//     * Create a component to display the message within the bounds of the
//     * screen. If the message is too big for the screen a suitably sized
//     * scroll pane will be returned.
//     * @param message The text of the message.
//     * @return The component containing the text.
//     */
//	private fun getComponentForMessage(message: String):JComponent {
//        val jLabel = JLabel(message)
//        val scroller = JScrollPane(jLabel)
//        val size = jLabel.getPreferredSize()
//        val decorationHeight = 80
//        val decorationWidth = 70
//        val screenBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds()
//        var scrollerNeeded = false
//        if (size.height > screenBounds.height - decorationHeight)
//        {
//            size.height = screenBounds.height - decorationHeight
//            scrollerNeeded = true
//        }
//        if (size.width > screenBounds.width - decorationWidth)
//        {
//            size.width = screenBounds.width - decorationWidth
//            scrollerNeeded = true
//        }
//        else if (scrollerNeeded)
//        {
//            scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER)
//        }
//        scroller.setPreferredSize(size)
//        return if (scrollerNeeded) scroller else jLabel
//    }

    private fun showMessageConfirm(title: String, message: String): Boolean {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)
        var ret = false
        builder.setPositiveButton(R.string.in_yes) { dialog, _ ->
            ret = true
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.in_close) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
        return ret
    }

    override fun showErrorMessage(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    override fun showInfoMessage(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setIcon(android.R.drawable.ic_dialog_info)
        builder.show()
    }

    override fun showWarningMessage(title: String, message: String) {
        showErrorMessage(title, message)
    }

    override fun showInputDialog(title: String, message: String, initialValue: String): String? {
        val inflater = this.layoutInflater
        val layout = inflater.inflate(R.layout.dialog_game_license, null)
        val messageView = layout.findViewById<TextView>(R.id.input_message)
        messageView.text = message
        val inputView = layout.findViewById<EditText>(R.id.input_value)
        inputView.setText(initialValue)
        var ret: String? = null

        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setNegativeButton(R.string.in_cancel) { dialog, _ -> dialog.dismiss() }
        builder.setPositiveButton(R.string.in_ok) { dialog, _ ->
            ret = inputView.text.toString()
            dialog.dismiss()
        }
        builder.setCancelable(true)
        builder.setView(layout)
        builder.show()

        return if (ret == null) null else (ret).toString()
    }

    override fun showLevelUpInfo(character: CharacterFacade, oldLevel: Int) {
        PostLevelUpDialog.showPostLevelUpDialog(this, character, oldLevel)
    }

    override fun showGeneralChooser(chooserFacade: ChooserFacade): Boolean {
        // Check for an override of the chooser to be used
        val choiceHandler = ChooserFactory.getChoiceHandler()
        if (choiceHandler != null) {
            return choiceHandler.makeChoice(chooserFacade)
        }

        return if ((chooserFacade.isPreferRadioSelection && chooserFacade.availableList.size <= 20 && chooserFacade.remainingSelections.get() == 1)) {
            val dialog = RadioChooserDialog(this, chooserFacade)
            dialog.show()
            dialog.isCommitted
        } else {
            val dialog = ChooserDialog(this, chooserFacade)
            dialog.show()
            dialog.isCommitted
        }
    }

    /*
     * This thread does all work of loading sources that the user
     * has selected. After the sources are loaded the thread then
     * displays the licenses for the data.
     */
    private inner class SourceLoadWorker(val sources: SourceSelectionFacade, delegate: UIDelegate) : Thread() {
        private val loader: SourceFileLoader = SourceFileLoader(sources, delegate)
        private val worker: SwingWorker<List<LogRecord>>

        init {
            worker = statusBar!!.createWorker(LanguageBundle.getString(R.string.in_taskLoadSources), loader)
        }

        override fun run() {
            worker.start()
            //wait until the worker finish and post any errors that occurred
            statusBar!!.setSourceLoadErrors(worker.get())
            //now that the SourceFileLoader has finished
            //handle licenses and whatnot
            val sec15 = StringBuilder(" ")
            sec15.append(readTextFromFile((ConfigurationSettings.getSystemsDir() + File.separator + "opengaminglicense.10a.txt")))
            sec15.append(loader.ogl)
            section15 = sec15.toString()
            try {
                showLicenses()
            } catch (e: Throwable) {
                Timber.e(e, "Failed to show licences.")
            }

            val data = loader.dataSetFacade
            if (data != null) {
                currentSourceSelection?.set(sources)

                val sourceString = StringBuilder(100)
                val campaigns = sources.campaigns
                for (i in 0 until campaigns.size) {
                    if (i > 0) {
                        sourceString.append('|')
                    }
                    sourceString.append(campaigns.getElementAt(i))
                }
                PCGenSettings.getInstance().setProperty(
                        PCGenSettings.LAST_LOADED_GAME, sources.gameMode.toString())
                PCGenSettings.getInstance().setProperty(
                        PCGenSettings.LAST_LOADED_SOURCES, sourceString.toString())
            } else {
                currentSourceSelection?.set(null)
            }
            currentDataSetRef?.set(data)
            updateTitle()
        }

        private fun showLicenses() {
            val context = PCGenSettings.OPTIONS_CONTEXT
            if (context.initBoolean(PCGenSettings.OPTION_SHOW_LICENSE, true)) {
                if (loader.hasOGLCampaign()) {
                    showOGLDialog()
                }
                if (loader.hasLicensedCampaign()) {
                    val licenses = loader.licenses
                    if (licenses.trim { it <= ' ' }.isNotEmpty()) {
                        showLicenseDialog(LanguageBundle.getString(R.string.in_specialLicenses), licenses)
                    }
                    for (license in loader.otherLicenses) {
                        showLicenseDialog(LanguageBundle.getString(R.string.in_specialLicenses), license)
                    }
                }
            }
            if ((loader.hasMatureCampaign() && context.initBoolean(PCGenSettings.OPTION_SHOW_MATURE_ON_LOAD, true))) {
                showMatureDialog(loader.matureInfo)
            }
            if (context.initBoolean(PCGenSettings.OPTION_SHOW_SPONSORS_ON_LOAD, true)) {
                showSponsorsDialog()
            }
        }
    }

    fun showOGLDialog() {
        showLicenseDialog(LanguageBundle.getString(R.string.in_oglTitle), section15)
    }

    private fun showLicenseDialog(title: String, html: String?) {
        var htmlString = html
        if (htmlString == null) {
            htmlString = LanguageBundle.getString(R.string.in_licNoInfo)
        }

        val pcgenSettings = PCGenSettings.OPTIONS_CONTEXT
        val inflater = this.layoutInflater
        val layout = inflater.inflate(R.layout.dialog_game_license, null)
        val licenseView = layout.findViewById<WebView>(R.id.licence_view)
        licenseView.loadData(Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY).toString(),
                "text/html; charset=utf-8", "utf-8")

        val showOnLoadSwitch = layout.findViewById<SwitchMaterial>(R.id.show_licence_on_load)
        showOnLoadSwitch.isSelected = pcgenSettings.getBoolean(PCGenSettings.OPTION_SHOW_LICENSE)
        showOnLoadSwitch.setOnCheckedChangeListener { _, isChecked ->
            pcgenSettings.setBoolean(PCGenSettings.OPTION_SHOW_LICENSE, isChecked)
        }

        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setNegativeButton(R.string.in_close) { dialog, _ -> dialog.dismiss() }
        builder.setCancelable(true)
        builder.setView(layout)
        builder.show()
    }

    private fun showMatureDialog(text: String) {
        Timber.i("Warning: The following datasets contains mature themes. User discretion is advised.")
        Timber.i(text)

        val pcgenSettings = PCGenSettings.OPTIONS_CONTEXT
        val inflater = this.layoutInflater
        val layout = inflater.inflate(R.layout.dialog_mature_content, null)

        val showOnLoadSwitch = layout.findViewById<SwitchMaterial>(R.id.show_licence_on_load)
        showOnLoadSwitch.isSelected = pcgenSettings.getBoolean(PCGenSettings.OPTION_SHOW_MATURE_ON_LOAD)
        showOnLoadSwitch.setOnCheckedChangeListener { _, isChecked ->
            pcgenSettings.setBoolean(PCGenSettings.OPTION_SHOW_MATURE_ON_LOAD, isChecked)
        }

        val licenseView = layout.findViewById<WebView>(R.id.licence_view)
        licenseView.loadData(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString(),
                "text/html; charset=utf-8", "utf-8")

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.in_matureTitle)
        builder.setNegativeButton(R.string.in_close) { dialog, _ -> dialog.dismiss() }
        builder.setCancelable(true)
        builder.setView(layout)
        builder.show()
    }

    fun showSponsorsDialog() {
        val sponsors = Globals.getGlobalContext().referenceContext.getConstructedCDOMObjects(Sponsor::class.java)

        if (sponsors.size <= 1) {
            return
        }

        val pcgenSettings = PCGenSettings.OPTIONS_CONTEXT
        val inflater = this.layoutInflater
        val layout = inflater.inflate(R.layout.dialog_game_license, null)

        val showOnLoadSwitch = layout.findViewById<SwitchMaterial>(R.id.show_licence_on_load)
        showOnLoadSwitch.isSelected = pcgenSettings.getBoolean(PCGenSettings.OPTION_SHOW_MATURE_ON_LOAD)
        showOnLoadSwitch.setOnCheckedChangeListener { _, isChecked ->
            pcgenSettings.setBoolean(PCGenSettings.OPTION_SHOW_SPONSORS_ON_LOAD, isChecked)
        }

        val sb = StringBuilder(500)

        sb.append("<html>")
        for (sponsor in sponsors) {
            if ("PCGEN" != sponsor.keyName) {
                continue
            }
            sb.append("<img src='").append(sponsor.bannerImage).append("'><br>")
        }

        var s = ""
        if (sponsors.size > 2) {
            s = "s"
        }
        sb.append("<H2><CENTER>")
                .append(LanguageBundle.getString(R.string.in_sponsorThanks))
                .append(s)
                .append(":</CENTER></h2>")
        var size = 172
        for (sponsor in sponsors) {
            if ("PCGEN" == sponsor.keyName) {
                continue
            }
            size += 70
            sb.append("<img src='").append(sponsor.bannerImage).append("'><br>")
        }
        sb.append("</html>")

        val licenseView = layout.findViewById<WebView>(R.id.licence_view)
        licenseView.loadData(Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY).toString(),
                "text/html; charset=utf-8", "utf-8")

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.in_sponsorTitle)
        builder.setNegativeButton(R.string.in_close) { dialog, _ -> dialog.dismiss() }
        builder.setCancelable(true)
        builder.setView(layout)
        builder.show()
    }

    fun showAboutDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.in_mnuHelpAbout)
        val creditsFragment = CreditsFragment()
        val inflater: LayoutInflater = LayoutInflater.from(this)
        builder.setView(creditsFragment.getLayout(this, inflater))
        builder.show()
    }

    override fun showCustomEquipDialog(character: CharacterFacade, equipBuilder: EquipmentBuilderFacade): UIDelegate.CustomEquipResult {
        val eqDialog = EquipCustomizerDialog(this, character, equipBuilder)
        eqDialog.show()

        return when {
            eqDialog.isCancelled -> UIDelegate.CustomEquipResult.CANCELLED
            eqDialog.isPurchase -> UIDelegate.CustomEquipResult.PURCHASE
            else -> UIDelegate.CustomEquipResult.OK
        }
    }

    override fun showCustomSpellDialog(spellBuilderFI: SpellBuilderFacade): Boolean {
        val spellDialog = SpellChoiceDialog(this, spellBuilderFI)
        spellDialog.show()
        return !spellDialog.isShowing
    }

    private fun readTextFromFile(fileName: String): String {
        val aString: String
        val aFile = File(fileName)

        if (!aFile.exists()) {
            Timber.e("Could not find license at $fileName")
            return LanguageBundle.getString(R.string.in_licNoInfo)
        }

        aString = try {
            val theReader = BufferedReader(InputStreamReader(FileInputStream(aFile), "UTF-8"))
            val length = aFile.length().toInt()
            val inputLine = CharArray(length)
            theReader.read(inputLine, 0, length)
            theReader.close()
            String(inputLine)
        } catch (e: IOException) {
            Timber.e(e, "Could not read license at $fileName")
            "No license information found"
        }

        return aString
    }

    /**
     * The Class `FilenameListener` is used to update the frame title each time the
     * current character's file name is changed.
     */
    private inner class FilenameListener : ReferenceListener<File> {
        override fun referenceChanged(e: ReferenceEvent<File>) {
            this@MainActivity.updateTitle()
        }
    }
}
