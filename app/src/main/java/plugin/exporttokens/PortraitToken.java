/*
 * PortraitToken.java
 * Copyright 2003 (C) Devon Jones <soulcatcher@evilsoft.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package plugin.exporttokens;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import pcgen.cdom.base.Constants;
import pcgen.core.display.CharacterDisplay;
import pcgen.io.ExportHandler;
import pcgen.io.exporttoken.AbstractExportToken;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

import net.redlightning.pcgen.MainActivity;

/**
 * The Class {@code PortraitToken} supports the PORTRAIT
 * token and its and PORTRAIT.THUMB variant.
 */
public class PortraitToken extends AbstractExportToken {
    private static final String TAG = PortraitToken.class.getCanonicalName();

    @Override
    public String getTokenName() {
        return "PORTRAIT";
    }

    //TODO: Move this to a token that has all of the descriptive stuff about a character
    @Override
    public String getToken(String tokenSource, CharacterDisplay display,
                           ExportHandler eh) {
        if ("PORTRAIT.THUMB".equals(tokenSource)) {
            return getThumbnailToken(display);
        }

        return display.getPortraitPath();
    }

    private String getThumbnailToken(CharacterDisplay display) {
        // Generate thumbnail
        Bitmap thumb = generateThumb(display);
        if (thumb == null) {
            return null;
        }

        // Save to a temporary file
        String pcgFilename = display.getFileName();
        String baseName;
        if (StringUtils.isNotBlank(pcgFilename)) {
            baseName = new File(pcgFilename).getName();
            if (baseName.indexOf(Constants.EXTENSION_CHARACTER_FILE) > 0) {
                baseName = baseName.substring(0, baseName.indexOf(Constants.EXTENSION_CHARACTER_FILE));
            }
        } else {
            baseName = display.getName();
        }

        File thumbFile;
        Context context = MainActivity.Companion.getAppContext();
        if (context != null) {
            try {
                File filesDir = MainActivity.Companion.getAppContext().getFilesDir();
                thumbFile = new File(filesDir, "pcgentmb_" + baseName + ".png");
            } catch (NullPointerException npe) {
                Log.e(TAG, "PortraitToken.getThumbnailToken failed", npe);
                return null;
            }

            try (FileOutputStream out = new FileOutputStream(thumbFile)) {
                thumb.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (IOException ioe) {
                Log.e(TAG, "PortraitToken.getThumbnailToken failed", ioe);
                return null;
            }
            return thumbFile.getAbsolutePath();
        }
        return null;
    }

    /**
     * Generate a thumnbnail image based on the character's portrait and
     * the thumbnail rectangle.
     *
     * @param display The character display being output.
     * @return The thumbnail image, or null if not defined.
     */
    private Bitmap generateThumb(CharacterDisplay display) {
        Rect cropRect = display.getPortraitThumbnailRect();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap portrait = BitmapFactory.decodeFile(display.getPortraitPath(), options);

        if (portrait == null || cropRect == null) {
            return null;
        }

        Bitmap thumb = Bitmap.createBitmap(portrait, cropRect.left, cropRect.top,
                cropRect.right - cropRect.left,
                cropRect.bottom - cropRect.top);
        thumb = Bitmap.createScaledBitmap(thumb, Constants.THUMBNAIL_SIZE,
                Constants.THUMBNAIL_SIZE, true);
        return thumb;
    }
}
