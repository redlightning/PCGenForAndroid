package plugin.lsttokens.gamemode;

import java.net.URI;

import pcgen.cdom.inst.EqSizePenalty;
import pcgen.core.GameMode;
import pcgen.persistence.lst.GameModeLstToken;
import pcgen.persistence.lst.SimpleLoader;
import android.util.Log;

/**
 * Class deals with EQSIZEPENALTY Token
 */
public class EqsizepenaltyToken implements GameModeLstToken
{
    private static final String TAG = EqsizepenaltyToken.class.getCanonicalName();

    @Override
	public String getTokenName()
	{
		return "EQSIZEPENALTY";
	}

    @Override
	public boolean parse(GameMode gameMode, String value, URI source)
	{
		try
		{
			SimpleLoader<EqSizePenalty> penaltyDiceLoader = new SimpleLoader<>(
					EqSizePenalty.class);
			penaltyDiceLoader.parseLine(gameMode.getModeContext(), value,
					source);
			return true;
		}
		catch (Exception e)
		{
			 Log.e(TAG, e.getMessage());
			return false;
		}
	}
}
