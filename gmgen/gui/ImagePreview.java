package gmgen.gui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import net.redlightning.pcgen.R;

import org.apache.commons.lang3.StringUtils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;

import pcgen.core.PlayerCharacter;
import pcgen.io.PCGFile;
import pcgen.io.PCGIOHandler;
import pcgen.system.LanguageBundle;

/**
 * A panel for previewing graphics in a file chooser.  This includes previewing
 * a character portrait referenced in a PCG file.
 *
 * TODO Support PCG portraits
 */
public final class ImagePreview
        extends View
		implements PropertyChangeListener
{
	private static final int SIZE = 200;

	private static final String in_notAnImage
			= LanguageBundle.getString(R.string.in_ImagePreview_notAnImage);
	private static final String in_noCharacterPortrait
			= LanguageBundle.getString(R.string.in_ImagePreview_notAnImage);

    private final FileChooserActivity jfc;
	private PlayerCharacter aPC;
	private Bitmap image;

	/**
	 * Constructor
	 * @param jfc
	 */
	private ImagePreview(final FileChooserActivity jfc)
	{
		this.jfc = jfc;
	}

	/**
	 * Decorates a Swing file chooser with an image previewer.
	 *
	 * @param jfc the file chooser
	 *
	 * @return the file chooser
	 */
	public static FileChooserActivity decorateWithImagePreview(final FileChooserActivity jfc)
	{
		new ImagePreview(jfc); // hooks everything up
		return jfc;
	}

    @Override
	public void propertyChange(final PropertyChangeEvent evt)
	{
		try
		{
			updateImage(jfc.getSelectedFile());
		}
		catch (final IOException e)
		{
			e.printStackTrace(); // TODO: ack, suckage!
		}
	}

	/**
	 * Update the image
	 * @param file File object holding the PC Gen character
	 * @throws IOException if the image file cannot be opened
	 */
    private void updateImage(final File file) throws IOException {
		if (file == null || !file.exists())
		{
			image = null;
			return;
		}

		if (PCGFile.isPCGenCharacterFile(file))
		{
			aPC = new PlayerCharacter(Collections.emptyList());

			new PCGIOHandler().readForPreview(aPC, file.getAbsolutePath());

			final String portraitPath = aPC.getDisplay().getPortraitPath();

			image = StringUtils.isEmpty(portraitPath)
					? null
					: BitmapFactory.decodeFile(portraitPath);
		}
		else
		{
			aPC = null;
			image = BitmapFactory.decodeFile(file.getCanonicalPath());
		}

		//repaint();
	}

    protected void paintComponent(final Canvas canvas)
	{
	    Paint opaque = new Paint(Color.WHITE);
        Paint black = new Paint(Color.BLACK);
	    Paint background = new Paint(Color.WHITE);
	    canvas.drawRect(new Rect(0,0,getWidth(), getHeight()), background);

		final int textX = ImagePreview.getFontHeightHint(canvas);
		final int textY = ImagePreview.SIZE - ImagePreview.getFontHeightHint(canvas);

		if (image != null)
		{
			final int width = image.getWidth();
			final int height = image.getHeight();
			final int side = Math.max(width, height);
			final double scale = SIZE / (double) side;

			Rect src = new Rect(0,0,width, height);
			Rect dest = new Rect(0,0, (int) (scale * width), (int) (scale * height));
            canvas.drawBitmap(image, src, dest, opaque);
			// Annotate with original dimensions.  Overlay black on white so
			// the values are visible against most possible image backgrounds.
			final String dim = width + " x " + height;
			canvas.drawText(dim, textX, textY, black);
            canvas.drawText(dim, textX - 1, textX - 1, black);
		}
		else
		{
			// TODO: I18N
            canvas.drawText(aPC == null ? ImagePreview.in_notAnImage : ImagePreview.in_noCharacterPortrait, textX, textY, black);
		}
	}

	private static int getFontHeightHint(final Canvas canvas) {
		return canvas.getHeight();
	}
}
