/*
 * Copyright 2003 (C) Devon Jones
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
 package plugin.doomsdaybook;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import net.redlightning.pcgen.R;

import java.io.File;

import gmgen.GMGenSystemView;
import gmgen.pluginmgr.messages.AddMenuItemToGMGenToolsMenuMessage;
import gmgen.pluginmgr.messages.RequestAddTabToGMGenMessage;
import pcgen.core.SettingsHandler;
import pcgen.gui2.doomsdaybook.NameGenPanel;
import pcgen.pluginmgr.InteractivePlugin;
import pcgen.pluginmgr.PCGenMessage;
import pcgen.pluginmgr.PCGenMessageHandler;
import pcgen.pluginmgr.messages.FocusOrStateChangeOccurredMessage;
import pcgen.system.LanguageBundle;


public class RandomNamePlugin implements InteractivePlugin
{
	/** Log name */
	public static final String LOG_NAME = "Random_Name_Generator";
    private final Context context;

    /** The plugin menu item in the tools menu. */
	private MenuItem nameToolsItem;

	/** The user interface that this class will be using. */
	private NameGenPanel theView;

	/** The English name of the plugin. */
	private static final String NAME = "Random Names";

	private PCGenMessageHandler messageHandler;

	public RandomNamePlugin(Context context) {
	    this.context = context;
    }

	/**
	 * Starts the plugin, registering itself with the {@code TabAddMessage}.
	 */
    @Override
	public void start(Menu menu, PCGenMessageHandler mh)
	{
    	messageHandler = mh;
		theView = new NameGenPanel(context, getDataDirectory());
		messageHandler.handleMessage(new RequestAddTabToGMGenMessage(this, RandomNamePlugin.getLocalizedName(), getView()));
		initMenus(menu);
	}

	@Override
	public void stop()
	{
		messageHandler = null;
	}

    @Override
	public int getPriority()
	{
		return SettingsHandler.getGMGenOption(RandomNamePlugin.LOG_NAME + ".LoadOrder", 80);
	}

	/**
	 * Accessor for name
	 * @return name
	 */
    @Override
	public String getPluginName()
	{
		return RandomNamePlugin.NAME;
	}
	
	private static String getLocalizedName()
	{
		return LanguageBundle.getString(R.string.in_plugin_randomname_name);
	}

	/**
	 * Gets the view that this class is using.
	 * @return the view.
	 */
	public View getView()
	{
		return theView;
	}

	/**
	 * listens to messages from the GMGen system, and handles them as needed
	 * @param message the source of the event from the system
	 */
    @Override
	public void handleMessage(PCGenMessage message)
	{
		if (message instanceof FocusOrStateChangeOccurredMessage)
		{
			if (isActive())
			{
				nameToolsItem.setEnabled(false);
			}
			else
			{
				nameToolsItem.setEnabled(true);
			}
		}
	}

	/**
	 * Returns true if this plugin is active
	 * @return true if this plugin is active
	 */
	public boolean isActive()
	{
	    return getView().getVisibility() == View.VISIBLE;
	}

	/**
	 * Initialise the menus
	 */
	private void initMenus(Menu menu)
	{
        nameToolsItem = menu.add(R.string.in_plugin_randomname_name);
        nameToolsItem.setOnMenuItemClickListener((item) -> {
            getView().setVisibility(View.VISIBLE);
            return true;
        });
		messageHandler.handleMessage(new AddMenuItemToGMGenToolsMenuMessage(this, nameToolsItem));
	}

	@Override
	public File getDataDirectory()
	{
		return new File(SettingsHandler.getGmgenPluginDir(), RandomNamePlugin.NAME);
	}
}
