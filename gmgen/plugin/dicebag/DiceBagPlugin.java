/*
 * Copyright 2003 (C) Ross M. Lodge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package plugin.dicebag;

import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import net.redlightning.pcgen.R;

import java.io.File;

import pcgen.core.SettingsHandler;
import pcgen.pluginmgr.InteractivePlugin;
import pcgen.pluginmgr.PCGenMessage;
import pcgen.pluginmgr.PCGenMessageHandler;
import pcgen.system.LanguageBundle;

import gmgen.pluginmgr.messages.AddMenuItemToGMGenToolsMenuMessage;
import gmgen.pluginmgr.messages.RequestAddTabToGMGenMessage;
import plugin.dicebag.gui.DiceBagPluginView;

/**
 *
 * <p>
 * The base plugin class for the DiceBag plugin. This class handles mediation
 * between the GUI components of in {@code dicebag.gui} and the plugin
 * framework. This class should <b>not </b> pass framework events directly on to
 * the {@code dicebag.gui} classes, nor should those classes call the
 * framework directly.
 * </p>
 */
public class DiceBagPlugin implements InteractivePlugin
{
    private static final String TAG = DiceBagPlugin.class.getCanonicalName();

    private Context context;

	/** Name for logger.*/
	public static final String LOG_NAME = "DiceBag";

    private static final String NAME = "DiceBag";

    /** Key of dice bag tab. */
	private static final String IN_NAME = "in_plugin_dicebag_name";

	private PCGenMessageHandler messageHandler;
	private DiceBagPluginView diceBagPluginView;

	/**
	 * <p>
	 * Default (and only) constructor. Initializes the plugin.
	 * </p>
	 */
	public DiceBagPlugin(Context context)
	{
	    this.context = context;
	}

    /**
	 * <p>
	 * Adds view panel via TabAddMessage and initializes the menu items.
	 * </p>
	 */
    @Override
	public void start(Menu menu, PCGenMessageHandler mh)
	{
	    diceBagPluginView = new DiceBagPluginView(context);
    	messageHandler = mh;
		messageHandler.handleMessage(new RequestAddTabToGMGenMessage(this, LanguageBundle.getString(R.string.in_plugin_dicebag_name), diceBagPluginView));
        initMenus(menu);
	}

	@Override
	public void stop()
	{
		messageHandler = null;
	}

    @Override
	public int getPriority()
	{
		return SettingsHandler.getGMGenOption(DiceBagPlugin.LOG_NAME + ".LoadOrder", 20);
	}

    @Override
	public String getPluginName()
	{
		return DiceBagPlugin.NAME;
	}



    /**
	 * <p>
	 * Listens to messages on the GMGen Bus. Handles the following:
	 * </p>
	 * <ul>
	 * <li>StateChangedMessage</li>
	 * <li>WindowClosedMessage</li>
	 * <li>FileOpenMessage</li>
	 * <li>SaveMessage</li>
	 * <li>LoadMessage</li>
	 * </ul>
	 * <p>
	 * Delegates all these messages to {@code theController}.
	 * </p>
	 * @param message
	 *
	 * @see pcgen.pluginmgr.PCGenMessageHandler#handleMessage
	 */
    @Override
	public void handleMessage(PCGenMessage message)
	{
        //TODO handle messages
        Log.i(TAG, message.toString());
	}

	/**
	 * isActive
	 * @return TRUE if active
	 */
	public boolean isActive()
	{
	    return diceBagPluginView.getVisibility() == View.VISIBLE;
	}

    /**
     * Initialise the menus
     */
    private void initMenus(Menu menu)
    {
        /** Menu item for tools menu. Selects this tab. */
        MenuItem notesToolsItem = menu.add(R.string.in_plugin_dicebag_name);
        notesToolsItem.setOnMenuItemClickListener((item) -> {
            diceBagPluginView.setVisibility(View.VISIBLE);
            return true;
        });
        messageHandler.handleMessage(new AddMenuItemToGMGenToolsMenuMessage(this, notesToolsItem));
    }

	/**
	 *  Gets the name of the data directory for Plugin object
	 *
	 *@return    The data directory name
	 */
	@Override
	public File getDataDirectory()
	{
		return new File(SettingsHandler.getGmgenPluginDir(), getPluginName());
	}
}
