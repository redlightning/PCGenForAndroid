/*
 * Copyright 2003 (C) Ross M. Lodge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package plugin.dicebag.gui;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import java.util.Random;

/**
 *
 * <p>The view class for the DiceBag plugin.</p>
 */
public class DiceBagPluginView extends View
{
    private static final Integer ORANGE = Color.rgb(255, 140, 0);
    private static final String[] DICE_SIDES = new String[] { "2", "3", "4", "6", "8", "10", "12", "20", "100" };
    private NumberPicker mNumberOfSides;
    private NumberPicker mNumberOfDice;
    private TextView mResultView;


    public DiceBagPluginView(Context context) {
        super(context);
        // Required empty public constructor

        final View layout = LayoutInflater.from(context).inflate(R.layout.plugin_dicebag, null, false);

        mResultView = layout.findViewById(R.id.roll_result);

        mNumberOfDice = (DiceBagGridLayout) layout.findViewById(R.id.die_count);
        mNumberOfDice.setMinValue(1);
        mNumberOfDice.setMaxValue(20);

        mNumberOfSides = (DiceBagGridLayout) layout.findViewById(R.id.die_sides);
        mNumberOfSides.setDisplayedValues(DICE_SIDES);
        mNumberOfSides.setMaxValue(DICE_SIDES.length - 1);

        final LinearLayout roller = layout.findViewById(R.id.roller);
        roller.setOnClickListener((view) -> roll());

    }

    /**
     * Calculate the random result of XdY dice rolls.
     */
    private void roll() {
        final int selected = mNumberOfSides.getValue();
        final String sideName = DICE_SIDES[selected];
        final int sides = Integer.valueOf(sideName);
        final Random random = new Random();

        int roll = 0;
        for (int i = 1; i <= mNumberOfDice.getValue(); i++) {
            roll += random.nextInt(sides) + 1;
        }

        // Display the result
        mResultView.setText(String.valueOf(roll));
        final ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), Color.WHITE, ORANGE);
        colorAnimation.addUpdateListener((animator) -> mResultView.setTextColor((Integer) animator.getAnimatedValue()));
        colorAnimation.setDuration(1000);
        colorAnimation.start();
    }
}
