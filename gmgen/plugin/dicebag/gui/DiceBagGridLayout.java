/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package plugin.dicebag.gui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.lang.reflect.Field;

/**
 * Customized version of NumberPicker allowing setting of the font size, etc
 *
 * @author Michael Isaacson
 * @version 18.10.17
 * @link https://stackoverflow.com/questions/6958460/android-can-i-increase-the-textsize-for-the-numberpicker-widget/12084420#12084420
 */
public class DiceBagGridLayout extends android.widget.NumberPicker {
    private static final String TAG = DiceBagGridLayout.class.getCanonicalName();

    public DiceBagGridLayout(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        setDividerColor(this);
    }

    @Override
    public void addView(final View child) {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(final View child, final int index, final android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        updateView(child);
    }

    @Override
    public void addView(final View child, final android.view.ViewGroup.LayoutParams params) {
        super.addView(child, params);
        updateView(child);
    }

    /**
     *
     * @param view the NumberPicker's EditText view
     */
    private void updateView(final View view) {
        if (view instanceof EditText) {
            ((EditText) view).setTextSize(28);
            ((EditText) view).setTextColor(Color.WHITE);
        }
    }

    /**
     * Set the NumberPicker's dividing line color
     *
     * @link https://stackoverflow.com/questions/24233556/changing-numberpicker-divider-color
     */
    private void setDividerColor(@NonNull NumberPicker picker) {
        try {
            @SuppressWarnings("JavaReflectionMemberAccess")
            final Field fDividerDrawable = NumberPicker.class.getDeclaredField("mSelectionDivider");
            fDividerDrawable.setAccessible(true);
            ColorDrawable colorDrawable = new ColorDrawable(Color.TRANSPARENT);
            fDividerDrawable.set(picker, colorDrawable);
            postInvalidate(); // Drawable is dirty
        } catch (final Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }
}